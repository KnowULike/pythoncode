# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(01/12/2020)---------------------------------------------------------------------------------------------

Calculator for failure fix in test_mocule10.py.
"""


# -------------------------------------------------------------------------------------------------------------------
class Calculator:
    @staticmethod
    def add(x, y):
        number_types = (int, float, complex)
        if isinstance(x, number_types) and isinstance(y, number_types):
            return x + y
        else:
            raise ValueError
