# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(01/12/2020)---------------------------------------------------------------------------------------------

Test-Driven-Development Example.

1st - run test and identify that it's missing import
 fix.:      "Make import"

2nd - run test again and identify that the add() method returns the wrong value.
 fix.:      "change 'pass' in calculator.py file under development."

Continue to add features and correct failures..
"""


# -------------------------------------------------------------------------------------------------------------------
import pytest
from Testing.Pytest.Dev.calculator import Calculator


class TestClass01:

    def test_case01(self):
        calc = Calculator()
        result = calc.add(2, 2)
        assert 4 == result

    def test_case02(self):
        with pytest.raises(ValueError):
            result = Calculator().add(2, 'two')

    def test_case03(self):
        with pytest.raises(ValueError):
            result = Calculator().add('two', 2)

    def test_case04(self):
        with pytest.raises(ValueError):
            result = Calculator().add('two', 'two')
