# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(01/12/2020)---------------------------------------------------------------------------------------------

 How to access fixture information on the requested object.

Scope of pytest Fixtures
 The following is the list of scopes for pytest fixtures:

	 "function" : Runs once per test
	 "class"    : Runs once per class
	 "module"   : Runs once per module
	 "session"  : Runs once per session, it's a good ideia to use session for packages.

To use, define like:
    @pytest.fixture(scope="class")

"""


# -------------------------------------------------------------------------------------------------------------------
import pytest

@pytest.fixture()
def fixture01(request):
    print("\nIn fixture...")
    print("Fixture Scope: " + str(request.scope))
    print("Function Name: " + str(request.function.__name__))
    print("Class Name: " + str(request.cls))
    print("Module Name: " + str(request.module.__name__))
    print("File Path: " + str(request.fspath))

@pytest.mark.usefixtures('fixture01')
def test_case01():
    print("\nI'm the test_case01")