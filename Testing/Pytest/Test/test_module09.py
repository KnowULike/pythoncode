# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(01/12/2020)---------------------------------------------------------------------------------------------

Use pytest.raises() to check if any test raises an exception.
It's useful for automating negative test scenarios.
"""

# -------------------------------------------------------------------------------------------------------------------
import pytest


def test_case01():
    with pytest.raises(Exception):
        x = 1 / 0


def test_case02():
    with pytest.raises(Exception):
        x = 1 / 1
