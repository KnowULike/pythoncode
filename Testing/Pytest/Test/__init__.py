# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(01/12/2020)---------------------------------------------------------------------------------------------

Usage.:
    all = ['test_file1', 'test_file2', ... ]

Useful commands.:

Test Package Module:    python3 -m pytest [options] Test/test_module02.py
Test Package Class:     python3 -m pytest [options] Test/test_module02.py::TestClass01
Test Package Method:    python3 -m pytest [options] Test/test_module02.py::TestClass01::test_case02
Discovery:              python3 -m pytest [options]

To see fixtures:        python3 -m pytest [-s] 'test_path'
"""


# -------------------------------------------------------------------------------------------------------------------
all = ["test_module01", "test_module02", "test_module03", "test_module04", "test_module05"
       "test_module06", "test_module07", "test_module08", "test_module09", "test_module10"]
