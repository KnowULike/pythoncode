# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(01/12/2020)---------------------------------------------------------------------------------------------

Add a finalizer function to the fixture. (It allows to run a block of code after the test with a fixture has run.)
"""


# -------------------------------------------------------------------------------------------------------------------
import pytest

@pytest.fixture()
def fixture01(request):
    print("\nIn fixture...")

    def fin():
        print("\nFinalized...")

    request.addfinalizer(fin)

@pytest.mark.usefixtures('fixture01')
def test_case01():
    print("\nI'm the test_case01")