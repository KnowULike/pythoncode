# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

My first test module.

Now it is possible to run entire modules, classes or method tests from root directory. Just use:
    "path(divided by '.').'dir'.'module'.'class'.'method'"
Don't forget to add to __init__.py tests after they were created in module.

Use "python3 -m unittest discover [-v]" to discover and execute all the tests in the project directory
 and all its subdirectories. It's a vital component in a automated tests framework.
"""
# -------------------------------------------------------------------------------------------------------------------
all = ["test_module01", "test_module02", "test_module03", "test_module04",
       "test_module05", "test_module06", "test_module07", "test_module08"]