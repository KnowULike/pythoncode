# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

Skipping tests

unittest provides a mechanism for skipping tests, conditionally or unconditionally.
It uses the following decorators for implementing the skipping mechanism:

•	 unittest.skip(reason): Unconditionally skips the decorated test. reason should describe why the
 test is being skipped.

•	 unittest.skipIf(condition, reason): Skips the decorated test if condition is true


is very useful for running platform-specific test cases.

You can also skip entire test classes in a test module using the unittest.
skip(reason) decorator.
"""
# -------------------------------------------------------------------------------------------------------------------

import sys
import unittest

class TestClass13(unittest.TestCase):

    @unittest.skip("demonstrating unconditional skipping")
    def test_case01(self):
        self.fail("FATAL")

    @unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
    def test_case02(self):
        # Windows specific testing code
        pass

    @unittest.skipUnless(sys.platform.startswith("linux"), "requires Linux")
    def test_case03(self):
        # Linux specific testing code
        pass