# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

Listing All the Command-Line Options and Help
    > "python3 -m unittest -h"

Important Command-Line Options (Use just like -v)

    - Run [-q] for quiet mode in command line: it displays more information on failed tests only.
    - Run [-f]  for failsafe. It forcefully stops execution as soon as the first test case fails.
    - It's possible to mix options (ex.: -fv )
"""
# -------------------------------------------------------------------------------------------------------------------

import unittest

class TestClass08(unittest.TestCase):

    def test_case01(self):
        self.assertTrue("PYTHON".isupper())
        print("\nIn test_case1()")

    def test_case02(self):
        self.assertTrue("Python".isupper())
        print("\nIn test_case2()")

    def test_case03(self):
        self.assertTrue(True)
        print("\nIn test_case3()")