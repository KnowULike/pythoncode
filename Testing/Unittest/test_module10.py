# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

Failing a Test

Many times, you might want to have a method that explicitly fails a test when it’s called.
In unittest, the 'fail()' method is used for that purpose.

Note: When an exception is raised in a test case, the test case also fails but with a different message.
"""
# -------------------------------------------------------------------------------------------------------------------

import unittest
class TestClass12(unittest.TestCase):
    def test_case01(self):
        """This is a test method..."""
        print(self.id())
        self.fail()
        #raise Exception
