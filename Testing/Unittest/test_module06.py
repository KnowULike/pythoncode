# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

Write python(or python3) -m unittest 'file_name' in comand line to verify tests.
Use -v to see tests in detail.
"""
# -------------------------------------------------------------------------------------------------------------------
import unittest

class TestClass07(unittest.TestCase):
    def test_case01(self):
        self.assertTrue("PYTHON".isupper())
        print("\nIn test_case01()")

