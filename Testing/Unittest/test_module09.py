# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

Other useful methods.

The id() and shortDescription() methods are very useful for debugging.
    - id() returns the name of the method
    - shortDescription() returns the description of the method.
"""
# -------------------------------------------------------------------------------------------------------------------
import unittest

class TestClass11(unittest.TestCase):
    def test_case01(self):
        """This is a test method..."""
        print("\nIn test_case01()")
        print(self.id())
        print(self.shortDescription())