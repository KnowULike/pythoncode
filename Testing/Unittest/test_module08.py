# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

Importing 'test_me.py' file to be tested. Now it is possible to better organize code in a different directory.
It's a common practice to split modules of code for development and testing.

Note: Use 'tree' to see how directory is structured in command line.
"""
# -------------------------------------------------------------------------------------------------------------------
import unittest
import Testing.Unittest.test_me as test

class TestClass09(unittest.TestCase):

    def test_case01(self):
        self.assertEqual(test.add(2, 3), 5)
        print("\nIn test_case01()")

    def test_case02(self):
        self.assertEqual(test.mul(2, 3), 6)
        print("\nIn test_case02()")