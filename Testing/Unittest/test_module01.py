# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(29/11/2020)---------------------------------------------------------------------------------------------

Simple tests with unittest.
Assert types.
Runner in main() function.
command line runner: python 'file_name' -v
"""
# -------------------------------------------------------------------------------------------------------------------

import unittest

class TestClass01(unittest.TestCase):

    def test_case01(self):
        my_str = "ROCHA"
        my_int = 999
        self.assertTrue(isinstance(my_str, str))
        self.assertTrue(isinstance(my_int, int))

    def test_case02(self):
        my_pi = 3.14
        self.assertFalse(isinstance(my_pi, int))

if __name__ == '__main__':
    unittest.main()