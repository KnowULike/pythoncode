# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(Chicambi)-----------------------------------------------------------------------------------
Created on %(28/11/2020)---------------------------------------------------------------------------------------------

This is test_module01.
This is example of multiline docstring.
"""
# -------------------------------------------------------------------------------------------------------------------


class TestClass01:
    """This is TestClass01."""

    def test_case01(self):
        """This is test_case01()."""
        print("method")

pass

def test_function01():
    """This is test_function01()."""
    print("function")