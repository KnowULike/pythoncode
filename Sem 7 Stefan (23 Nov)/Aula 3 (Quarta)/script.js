window.onpageshow = function() {
    document.getElementById("pageTitle").style.color = "red";
    console.log("Challenge 1 Complete!");
    
    var elems = document.getElementsByTagName("p");

    // iterate using for...of loop
    for (var p of elems) {
        console.log(p.innerText);
        p.style.fontSize = "xx-large";
        if (p.className === "greenp"){
            p.style.color = "green";
        }
    }

    console.log("Challenge 2 & 3 Complete!");

}