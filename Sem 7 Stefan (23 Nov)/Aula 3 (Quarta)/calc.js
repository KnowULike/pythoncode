var value = 0;
var valueDisplay = document.getElementById("value-display");
var addBtn = document.getElementById("addBtn");
var subBtn = document.getElementById("subBtn");

addBtn.addEventListener("click", function () {
    value++;
    valueDisplay.innerText = value.toString();
});

subBtn.addEventListener("click", function () {
    value--;
    valueDisplay.innerText = value.toString();
});