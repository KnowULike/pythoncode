# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

# Dict
# Define Dictionaries
# Use [] for list
# Use ( , ) for Tuple
# Use { : } to define Dictionary
# Dictionary {ley : value}

d = {
    'brand': 'cherry',
    'model': 'arizo5',
    'color': 'white'
}

print(type(d))  # Dict
print(len(d))  # 3

# Question: Dictionaries are ordered or not?
# R:

# Add new <key:value> or change value

d['year'] = '2020'  # Add new key <key:value> to dict 'd'

# d[<key>] = <value>
print(d['model'])
d['color'] = 'Black'  # Change values

# .Get() - access the members in tuple/list we use [<index>]
print(d)  # {'brand': 'cherry', 'model': 'arizo5', 'color': 'Black', 'year': '2020'}

x = d.get('model')  # dict_name.get(<key>) => <value>
print(x)  # arizo5

x = d.get('cylinder')
print(x)  # None, 'cylinder' not found

x = d.get('cylinder', -1)  # if <key> not found returns -1
print(x)  # -1

# Keys(), Values() amd Items()
# Acess keys, values or both of them

print(list(d.keys()))  # ['brand', 'model', 'color', 'year']

print(list(d.values()))  # ['cherry', 'arizo5', 'Black', '2020']

print(list(d.items()))  # [('brand', 'cherry'), ('model', 'arizo5'), ('color', 'Black'), ('year', '2020')]

for k, v in d.items():  # Unpacking (slide 34 - ahmed 4th lesson)
    print(k, ":", v)

# Pop(<key>)
d.pop()  # Error, Pop() for dict expected at least 1 argument

d.pop('model')  # dict_name.pop(<key>)

print(d)  # {'brand': 'cherry', 'color': 'Black', 'year': '2020'}

# we cannot acess members by index on dict, thats why we can t use pop() without arguments

# popitem()
# remove the last item in dict
# return removed item in output

f = d.popitem()
print(d)  # {'brand': 'cherry', 'color': 'Black'}
print(f)  # ('year', '2020')
print(type(f))  # <class 'tuple'>

# clear() and del

d.clear()
print(d)  # {}
del d

# Example
# Make a dict of {<key : value>}, the value of each key is the number of key occurrences

# Input ['X', 'Y', 'X', 'Z', 'Y', 'X']
# Output {'X': 3, 'Y': 2, 'Z': 1}

a = ['X', 'Y', 'X', 'Z', 'Y', 'X']
d = {}

for i in a:
    if i not in d:
        d[i] = 1
    else:
        d[i] += 1
print(d)

# Solution 2 - Use get()
# Solution 3 - Use setdefault(,)

for i in range(len(a)):
    d[i] = d.setdefault(i, 0) + 1
print(d)

# Copy()
# make a copy with copy()

a = {}  # 'a' is an empty dict

b = a  # 'a' and 'b' are dependent

c = a.copy()  # 'a' and 'c' are independent

# Dependencies are like as Lists

# EXERCISE 1)
# make a dict of {<key:value>}
# value of each key is the number of key occurrences
# input 'abfadcaa'
# Output {'a': 4, 'b':2, 'f':1, 'd':1, 'c':1}

input = 'abfadcaa'

output = {}

for l in input:
    if l not in output:
        output[l] = 1
    else:
        output[l] += 1
print(output)

# Solution

# s = 'abfadcaa'
# d = {}
# for i in s:
#     s[i] = d.get(i, 0) + i
# print(d)

# EXERCISE 2)
# make a dict of  {<key : value>}
# value for eah key is the number of key occurrences
# input
# line = 'a dictionary is a datastructure.'
# Output
# {'a': 2, 'dictionary': 1, 'is': 1, 'datastruture': 1}
#     find repeats in a text line

line = 'a dictionary is a datastructure'
d = {}
w = line.split()
print(w)

for i in w:
    d[i] = d.get(i, 0) + 1

print(d)

# EXERCISE 2 v2.0...
# make a dict of  {<key : value>}
# value for eah key is the number of key occurrences
# input
# line = 'a dictionary is a datastructure
#         a set is a datastructure.'
# Output
# {'a': 2, 'dictionary': 1, 'is': 1, 'datastruture': 1}
#     find repeats in a text line

line = 'a dictionary is a datastructure\na set is a datastructure.'
d = {}
line = line.rstrip('.')
s = line.split('\n')
print(s)
w = ""
for i in s:
    w += i + ' '
print(w)
l = w.split()
print(l)

for i in l:
    d[i] = d.get(i, 0) + 1

print(d)  # It works!

# Another Solution

line = 'a dictionary is a datastructure\na set is a datastructure.'
d = {}

for l in line.split('\n'):
    l = l.split('.')[0]  # line split returns a list, so we need indexing
    w = l.split()

    for i in w:
        d[i] = d.get(i, 0) + 1

print(d)

# Solution
lines = 'a dictionary is a datastructure\na set is a datastructure.'

d = {}
for i in range(len(lines.split("\n"))):
    line = lines.split("\n")[i].split(".")[0]

    s = line.split()
    d[s].d.get(s, 0) + 1
print(d)

# EXERCISE 4)
#  Calculate sum of values in dict

d = {'a': 4, 'b': 2, 'f': 1, 'd': 1, 'c': 1}

print(sum(d.values()))  # Erro

# Sort Operator Module Itemgetter()
#  Sort in dict by keys/values
import operator

k = operator.itemgetter(1)  # sort by values
print(sorted(d.items(), keys=k))
# [('f', 1), ('d', 1), ('c', 1), ('b', 2), ('a', 4)]

k = operator.itemgetter(0)  # sort by keys
print(sorted(d.items(), keys=k))
# [('a', 4), ('b', 2), ('c', 1), ('d', 2), ('f', 1)]


# Sorted()
#  Sort values

num = {
    'ali': [12, 13, 8],
    'sara': [15, 7, 14],
    'taha': [5, 18, 13]
}

d = {k: sorted(v) for k, v in num.items()}

print(d)  # {'ali': [8, 12, 13], 'sara': [7, 14, 15], 'taha': [5, 13, 18]}

# Update() -> merge 2 dict
d1 = {'x': 3, 'y': 2, 'z': 1}
d2 = {'w': 8, 't': 7, 'z': 5}

d1.update(d2)

print(d1)  # {'x': 3, 'y': 2, 'z': 5, 'w': 8, 't': 7}

# with for
d1 = {'x': 3, 'y': 2, 'z': 1}
d2 = {'w': 8, 't': 7, 'z': 5}

d = {}
for i in (d1, d2):
    print(i)
    d.update(i)

print(d)  # {'x': 3, 'y': 2, 'z': 5, 'w': 8, 't': 7}

# merge dictionaries with {**d1, **d2}

d = {**d1, **d2}

print(d)  # {'x': 3, 'y': 2, 'z': 5, 'w': 8, 't': 7}

# EXERCISE 5)
#  Merge 2 dict together
#  For same keys, sum values

#  can´t use update method

# input:
# d1 = {'x': 3, 'y': 2, 'z': 1}
# d2 = {'w': 8, 't': 7, 'z': 5}

# Output:
# {'x': 3, 'y': 2, 'z': 6, 'w': 8, 't': 7}

d1 = {'x': 3, 'y': 2, 'z': 1}
d2 = {'w': 8, 't': 7, 'z': 5}

for i, j in d2.items():
    if i in d1:
        d1[i] += d2[i]
    else:
        d1.update({i: j})

print(d1)  # {'x': 3, 'y': 2, 'z': 6, 'w': 8, 't': 7}

# HOMEWORK)
# Create a 'person' Dictionary with this detais:

# print(len(person))  # 4

# print(person['phone']['home'])  # 01-4455
# print(person['phone']['mobile'])    # 918-123456

# print(person['children'])   # ['Olivia', 'Sophia']
# print(person['children'][0])    #Olivia

# print(person.pop('age'))    # 48

person = {'phone': {'home': "01-4455",
                    'mobile': "918-123456"},
          'children': ['Olivia',
                       'Sophia'],
          'age': 48,
          'other': 'Thing'}

print(len(person))  # 4
print(person['phone']['home'])  # 01-4455
print(person['phone']['mobile'])  # 918-123456
print(person['children'])  # ['Olivia', 'Sophia']
print(person['children'][0])  # Olivia
print(person.pop('age'))  # 48
