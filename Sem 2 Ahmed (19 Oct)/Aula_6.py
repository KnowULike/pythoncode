# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

# Standart Library comes with Python and contains a huge number of useful modules
# It is important to become familiar with Python Standart Library to solve problems quickly
# Some functions are written in other languages (ex: Written in C)

# Some of the most important modules in standart library:
# Time
# sys
# os
# math
# random
# Pickle
# urllib
# re
# cgi
# socket


# A module is a file consisting of Python code
# Modules can define functions, classes and variables.
# A Module can contain a runnable code

# import modules

import math  # Normal away

math.pi  # 3.141592653589793

import math as m  # New way, normally Numpy it is common practice to use np

pi = m.pi
math.pi

# Import Submodules

from os import getcwd

print(getcwd())  # C:\Users\franc\PycharmProjects\pythoncode

from os import getcwd as gc

print(gc())  # C:\Users\franc\PycharmProjects\pythoncode

# Note: We normally import just a part of a module for speed and memory purposes.

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = np.array([-20, -3, -2, -1, 0, 1, 2, 3, 4])
plt.boxplot(data)
plt.show()  # Pycharm IDE needs this to show plot results

#from mathplotlib import pyplot as plt

#Note: View plot in SciView option

#Avoid importing everything because it can lead to more than one function with the same name

# From 'A' import *
# From 'B' import *
# f(..)?

sum??
from numpy import *
sum??   #Overwrites the previus sum function

#Find functions attributes - dir()
dir     #Built in function
import math
dir(math)   #Show every function and attributes

s = 'a'
dir(s)  #Show everything aplicable to strings, it works for other types too

#Help and Documentation

help(len)   #Returns description and how to use it

len.__doc__ #Prints the documentation string

    #Example docString
def square(a):
    "Return the square of a"
    return a**2

square.__doc__


#Exercise - What there functions do?
#In math module:

math.fmod(9,4) #Returns the remainer (its 1), similar to %
math.gcd(30,4)  #Returns the greatest common diviser (its 2)
math.fabs(-4)   #Returns the absolute value (its 4)

#In random module:

random.randint(1, 5)    #Return random int between 1 to 5
random.choice([1, 5])   #Return random choice between only 1 or 5
a = [1, 2, 3, 4]
random.shuffle(a)   #Returns a randomize arrangement of 'a'
print(a)

import math

print(math.sqrt(4))  # 2.0
print(math.trunc(2.3))  # 2
print(math.floor(2.3))  # 2
print(math.ceil(2.3))  # 3
print(math.factorial(4))  # 24 , 4! = 4*3*2*1
print(math.log2(32))  # 5.0
print(math.log10(100))  # 2.0
print(math.e)  # 2.7
print(math.log(32))  # 3.46
print(math.sin(5))  # -0.9
print(math.fmod(9, 4))  # 1.0 , 9%4
print(math.gcd(30, 4))  # 2 , greatest common divisor
print(math.fabs(-4))  # 4.0 , float abs
print(abs(-4))  # 4
print(math.pow(2, 3))  # 8.0
print(pow(2, 3))  # 8
print(math.pi)  # 3.1415926…
print(f'{math.pi :.2f}')  # 3.14

#Some Module Example
import sys

print(sys.version)

print(sys.platform)

import platform

platform.release()

import datetime

now = datetime.datetime.now()   #Saves date in this moment on a var

print(now)

print(now.year) #Prints year in this moment (2020)

print(now.month)    #Prints month in this moment (10)

print(now.day)  #Prints day in this moment (20)

datetime.datetime.today()   #Prints today's date


#Exercise Ahmed
import datetime

now = datetime.datetime.now()

if datetime.datetime.today().minute % 2 != 0 : #Better to use "datetime.datetime..." then var "now" because isn´t static
    print("Odd minute")
else:
    print("Not an Odd minute")

# Use dir() to see datetime.datetime.today() operations in case they don,t show

#Exercise (for version)
from datetime import datetime as dt
m = dt.today().minute

for i in range(1,60,2):
    if i == m:
        print("Odd minute")
        break
    else:
        print("Not an Odd minute")
        break

#Exercise (list version)
from datetime import datetime as dt
m = dt.today().minute

l = [i for i in range(1,60, 2)]
for i in l:
    if i == m:
        print("Odd minute")
        break
    else:
        print("Not an Odd minute")
        break
