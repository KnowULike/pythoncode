# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
'''
1 - Print first row
2 - Print first column in single line
3 - Print main diameter 1, 5, 9
4 - print another diameter 3, 5, 7
5 - Calculate Sum of rows
6 - Calculate Sum of columns
'''

m = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

"""
1. m = [m[0], m[1], m[2]]
"""

print("first row is:", m[0])

"""
2.
1 => m[0][0]
4 => m[1][0]
7 => m[2][0]

i = 0,1,2
m[i][0]
"""

for i in [0, 1, 2]:
    print(m[i][0], end=' ')

"""
3.
1 => m[0][0]
5 => m[1][1]
9 => m[2][2]

i = [0, 1, 2]
[i][i]
"""
print("\n----Main diameter----")
for i in range(len(m)):
    print(m[i][i], end=' ')

"""
4.
3 => m[0][2]
5 => m[1][1]
7 => m[2][0]

i = [0, 1, 2]
j = [2, 1, 0]
[i][j]
"""
j = len(m) - 1
for i in range(len(m)):
        print(m[i][j], end=' ')
        j -= 1

"""
5.
"""
for i in [0, 1, 2]:
    print(sum(m[i]), end=' ')

"""
6.
"""
import numpy as np

m2 = np.array(m)
m2.T
for i in [0, 1, 2]:
    print(sum(m[i]), end=' ')

import copy

# Another way
m2 = copy.deepcopy(m)

for i in range(len(m)):
    for j in range(len(m)):
        m2[i][j] = m[j][i]

for i in [0, 1, 2]:
    print(sum(m2[i]), end=' ')