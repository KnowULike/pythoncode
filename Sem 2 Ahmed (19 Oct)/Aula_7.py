# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

# Define a List

# Use brackets [] or list() function

a = [5, 7, 12]  # Define a list, []
print(a[0])  # 5
print(a[1])  # 7
print(a[2])  # 12

print(type(a))  # <class> 'list'
print(len(a))  # 3

# Note: you can put different types in a list
b = [1.618, 'Python Course', 0, {'joe': 21}, [], (3, 6, 9)]

# Methods/Attributes of a list

a = [5, 7, 12]
# Simply use dir()
dir(a)
dir(list)

# Methods in List:
# index , count , insert , remove , pop ,
# reverse , sort , extend , append , clear,
# copy , ...

# What is the index?
my_list = ['p', 'r', 'o', 'b', 'e']
print(my_list[-1])  # e
print(my_list[-5])  # p
print(my_list[-0])  # p

# Find the index of a member in a list
a = [5, 7, 12]
print(a.index(7))  # 1

# Use index of a member to acess to it
print(a[1])  # 7

# List is mutable, you can change it
a[1] = 8  # [5, 8, 12]

# Index in strings
#  Compare a List with a string

s = "sara"  # String
print(s[1])  # a
# String is immutable, you can Not change the string characters by index
s[1] = "d"  # Error

# List is ordered
a = [1, 2]
b = [2, 1]
print(a == b)  # False

# Show the items in a List
friends = ['Hamed', 'Josi', 'Stefan']
for f in friends:
    print(f)

# You can use range and index
for i in range(3):
    print(friends[i])

# EXERCISE about limits
my_list = [1, 2, 23, 4, 'word']
for i in [0, 1, 2, 3]:      # Removed '4' from the list!
    print(my_list[i], my_list[i + 1])

# Continuation...
my_list = [1, 2, 23, 4, 'word']
for i in range(len(my_list) - 1):
    print(my_list[i], my_list[i + 1])

#if - break (1)
my_list = [1,2,23,4,'word']
for i in [0,1,2,3,4]:
    if i == len(my_list) - 1:
        break
    print(my_list[i], my_list[i+1])

#if - break (Continuation...)
my_list = [1,2,23,4,'word']
for i in range(len(my_list)):
    if i == len(my_list) - 1:
        break
    print(my_list[i], my_list[i+1])

# You can use different data types
# in a list
# [int, int, bool, str, float, list, …]

L = [3, 6, True, 'ali', 2.7, [5, 8]]

#Slicing
#list_name[start, stop, step]
#start is include
#stop in Not included
#step is +1 by default

a = [7, 5, 30, 2, 6, 25]

#list_name[Start : Stop] , step is +1
print(a[1:4])   #[5, 30, 2]

#list_name[ :Stop]
#':' means start from the first, step is +1
print(a[:3])    #[7, 5, 30]

#list_name[Start: ]
#':' means until the end, step is +1
print(a[3:])    #[2, 6, 25]

#Start > Stop, No step means 'Step += +1'
print(a[3:0])   #[]

#Start > Stop, step = -1,
#Remember that Stop is Not included
print(a[3:0:-1])    #[2, 30, 5]

#list_name[ : :-1]
#reverse list
print(a[ : :-1])    #[25, 6, 2, 30, 5, 7]

#Slicing with step
print(a[0:7:2]) # [7, 30, 6]
print(a[6:0:-2]) # [25, 2, 5]
print(a[50:0:-2]) # [25, 2, 5]
print(a[ :0:-2]) # [25, 2, 5]

a[3:5]=[14, 15]
print(a)    # [7, 5, 30, 14, 15, 25]

# Repeat and Concatenate lists with * and +

a = [4, 7]
b = a*2     # '*' repeat list 2 times
print(b)    # [4, 7, 4, 7]

a = [1, 2]
b = ['a', 'b', 'c']
c = a + b   # '+' concatinate 2 lists
print(c)    # [4, 7, 4, 7]

#Membership
#Check membership with 'in' and 'not in'

a = [7, 5, 30, 2, 6, 25]

print(14 in a)  #False
print(14 not in a)  #True

#Nested Lists
#How to access the values of nested lists

a = [3, [109, 27], 4, 15]

print(a[1]) #[109, 27]

print(a(1)) #Error

print(a[1][1])  #27

print(a[1, 1])  #Error

print(len(a))   #4

#Exercise 2
a = [7, 5, 30, 2, 6, 25]

listSum = 0
maximum = a[0]  #In the beginning, the first number is the biggest
maxIndex = 0
for n in a:
    listSum += n
    if n > maximum:
        maximum = n
        maxIndex = a.index(n)
print("sum =", listSum)
print("index =", maxIndex, "max =", maximum)

#Max, Min, Sum
a = [7, 5, 30, 2, 6, 25]

print(max(a))   #30
print(min(a))   #2
print(sum(a))   #75

#Calulate sum with 'for' loop
s = 0
for i in a:
    s += i  #s = s + 1
print(s)    #75

#Count & Insert
# count(), number of occurrences of a value
a = [1, 3, 6, 5, 3]

print(a.count(3))   #2

# insert(), insert object before index
a = [1, 2, 6, 5, 2]

a.insert(2, 13) #[index, value]
print(a)    #[1, 2, 13, 6, 5, 2]

#Remove & Pop

# remove() firt occurrence of value
a = [1, 2, 6, 5, 2]

a.remove(2) # .remove(value)
print(a)    #[1, 6, 5, 2]

a.remove(2)
print(a)    #[1, 6, 5]

#Note: if i want to remove all, i should put it in a loop

# pop(), remove and return item at index (default last)
x = [10, 15, 12, 8]
a = x.pop()
print(x)    #[10, 15, 12]
print(a)    #8

    #remove the obj by index from the list
a = ['a', 'b', 'c']
p = a.pop(1)    # .pop(index)
print(p)    # b
print(a)    #[a, c]

# del does not return the deleted value

a = [5, 9, 3]
del a[1]
b = del a[1] # Error
print(a)    # [5, 3]

#Del Slicing
#del() multi objs by slicing

a = [0, 1, 2, 3, 4, 5, 6]
del a[2:4]
print(a)    #[0, 1, 4, 5, 6]

#Reverse & Sort

a = [1, 2, 3]
print(a[ : :-1])    #[3, 2, 1]
a.reverse()
print(a)    #[3, 2, 1]

b = a.reverse() # b in none, you should USE SLICING
print(b)    #none
b = a[::-1] #correct way
print(b)    #[3, 2, 1]

a = [2, 4, 3, 5, 1]
a.sort()    #It only works with same types and common types
print(a)    # [1, 2, 3, 4, 5]

# extend()
x = [1, 2, 3]
x.extend(5) #Error
x.extend([5])
print(x)    #[1, 2, 3, 5]

#join the list 'x' to the end of list 'y'

x = [1, 2, 3]
y = [4, 5]
x.extend(y)
print(x)    #[1, 2, 3, 4, 5]
print(len(x))   #5
print(len(y))   #2

# append()
a = [1, 2, 3]
a.append(4) #[1, 2, 3, 4]
print(a)

#add list 'y' as one member to the list 'x'
x = [1, 2, 3]
y = [4, 5]
x.append(y)
print(x)    #[1, 2, 3, [4, 5]]
print(len(x))   #4
print(len(y))   #2

#You can use loops and append() to initialize a list
a = []
for i in range(4):
    a.append(i)
print(a)    #[0, 1, 2, 3]

#Clear & Copy

#Clear()
a = [0, 1, 2]
a.clear()
print(a)    #[]
print(len(a))   #0

#Copy()
a = [0, 1, 2]
b = a.copy()
print(b)    #[0, 1, 2]

#Why we shoul use copy()
a = [1, 2, 3]
b = a.copy()    #a, b are independent
c = a   #a, c are dependent to each other (IMPORTANT) -> pointers
d = a[:]    #c, d are independent (Except on numpy arrays)

#When we change c or a, both of them can be changed, but b is independent

a[1] = 22
c[0] = 11
d[2] = 33
print(a)    #[11, 22, 3]
print(b)    #[1, 2, 3]
print(c)    #[11, 22, 3]
print(d)    #[0, 1, 33]

#Use slicing only if u need a part of, else, use copy()

#Example
x = 2
y = x
y += 1
#The values are independent, because they are Integers
print(x)    #2
print(y)    #3

#'x' and 'y' both point to the same location in the memory
x = []
y = x
y.append(5)
print(x)    #[5]
print(y)    #[5]

#<M operation> for M in a

#Do the <M operation> for all members in a M is the representor of members in a

a = [i for i in range(4)]
print(a)    #[0, 1, 2, 3]

a = [i*2 for i in range(4)]
print(a)    #[0, 2, 4, 6]

a = [i*i for i in range(3, 6)]
print(a)    #[9, 16, 25]

a = [1, -2, 5, -56, 8]
b = [abs(i) for i in a]
print(b)    #[1, 2, 5, 56, 8]

import math
a = [round(math.pi, i) for i in range(1, 5)]
print(a)    #[3.1, 3.14, 3.142, 3.1416]

#Remove $ from all members in a list
a = ['$ali', 'sara$']
b = [i.strip('$') for i in a]
print(b)    #['ali', 'sara']

# [<M operation> for M in a if <M filter>]
# do the <M operation>
# for only filtered members in a
a = [11, 8, 14, 20, 2]
b = [ i for i in a if i < 10]
print(b)    # [8, 2]

#Note: .strip('') removes from the start or finish by creating an identic string
# ''.join('str') -> transforms lists in to a str

a = [1, 2]
b = [1, 4, 5]
c = []
for i in a:
    for j in b:
        if i != j:
            c.append((i, j))
print(c)    #[(1, 4), (1, 5), (2, 1), (2, 4), (2, 5)]

#NaN values
#Remove NaN from a list with for loop

a = [2.6, float('NaN'), 4.8, 6.9, float('NaN')]
b = []

import math

for i in a:
    if not math.isnan(i):
        b.append(i)

print(b)    #[2.6, 4.8, 6.9]

#Note: if you want to change the length of a list, dict or set in a loop and want to remove/add some objects
#you need to check the iterable variable in your loop.
#If you used some list/dict or set you need to make a copy.

# in 'list' -> elements
# in range(len()) -> index's

#Homework - Matrix Exercise



m = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

# 1 - Print first row
# 2 - Print first column in single line
# 3 - Print main diameter 1, 5, 9
# 4 - print another diameter 3, 5, 7
# 5 - Calculate Sum of rows
# 6 - Calculate Sum of columns


# 1. m = [m[0], m[1], m[2]]


print("first row is:", m[0])

# 2.
# 1 => m[0][0]
# 4 => m[1][0]
# 7 => m[2][0]
#
# i = 0,1,2
# m[i][0]

for i in [0, 1, 2]:
    print(m[i][0], end=' ')

# 3.
# 1 => m[0][0]
# 5 => m[1][1]
# 9 => m[2][2]
#
# i = [0, 1, 2]
# [i][i]

print("\n----Main diameter----")
for i in range(len(m)):
    print(m[i][i], end=' ')

# 4.
# 3 => m[0][2]
# 5 => m[1][1]
# 7 => m[2][0]
#
# i = [0, 1, 2]
# j = [2, 1, 0]
# [i][j]

j = len(m) - 1
for i in range(len(m)):
        print(m[i][j], end=' ')
        j -= 1


# 5.
for i in [0, 1, 2]:
    print(sum(m[i]), end=' ')


# 6.

import numpy as np

m2 = np.array(m)
m2.T
for i in [0, 1, 2]:
    print(sum(m[i]), end=' ')

import copy

# Another way
m2 = copy.deepcopy(m)

for i in range(len(m)):
    for j in range(len(m)):
        m2[i][j] = m[j][i]

for i in [0, 1, 2]:
    print(sum(m2[i]), end=' ')