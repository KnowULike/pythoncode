# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

# zip, zip()
a = (1, 2)
b = (3, 4)
x = zip(a, b)
print(list(x))  # [(1, 3),(2, 4)]
print(x[0])
print(type(x[0]))

# Map two list into zip
k = ['red', 'green']
v = ['FF0000', '008000']
z = zip(k, v)
d = dict(z)
print(d)  # {'red': 'FF0000', 'green': '008000'}

# EXERCISE 6)
# find the number of X occurrences in S
# Input
# s = "Python Course"
# x = ['o', 'r']
# Output
# {'o':2,'r':1}

#Solution
s = "Python Course"

output = {}

for l in s:
    if l != 'o' and l != 'r':
        continue
    else:
        output[l] = output.get(l, 0) + 1
print(output) #Correct solution

# setdault() whould work too, receives two parameters 1 to check other in case it dosen't exist (insert)

# EXERCISE 7)
# Find the bug in this code
#
# d = {'x': 3, 'y': 2, 'z': 1, 'y': 4, 'z': 2}
# r = {}
#
# for k, v in d.items():
#     if x not in r.keys():
#         r[k] = v
# print(r)

# ANSWER: dictionaries don't accept duplicates


# EXERCISE 8)
# Count the number of 's == True' in students list
# Output is 2
#
# students = {
#     ('id': 123, 'name': 'Sophia', 's':True),
#     ('id': 378, 'name': 'William', 's': True),
#     ('id': 934, 'name': 'Sara', 's':False)
#     }

#Solution
students = [
    {'id': 123, 'name': 'Sophia', 's': True},
    {'id': 378, 'name': 'William', 's': False},
    {'id': 934, 'name': 'Sara', 's': True}
]

count = 0

for i in students:
    count += i.get('s')

print('Output: ', count)

print(sum(d['s'] for d in students))  # Another way

# Nested dict
#   dict in dict

family = {
    'child1': {'name': 'James', 'age': 8},
    'child2': {'name': 'Emma', 'age': 20}
}

print(family)

d1 = {'name': 'James', 'age': 8}
d2 = {'name': 'Emma', 'age': 20}

family = {
    'child1': d1,
    'child2': d2
}

# Example - randomly choose between dictionary keys
d = {
    'F': 0,
    'B': 0
}

import random

for _ in range(17):  # Do it 17x
    d[random.choice(list(d.keys()))] += 1  # Whenever a key is choosen it increments
print(d)

# Dictionary Merge & Update Operators

# Merge (|) instead of (**d1, **d2)
# Update (|=) instead of dict.update

# See changes in new version of Python 3.9

# Homework
# Create a 'person' Dictionary with this detais:

person = {'phone': {'home': "01-4455",
                    'mobile': "918-123456"},
          'children': ['Olivia',
                       'Sophia'],
          'age': 48,
          'other': 'Thing'}

# print(len(person))  # 4

# print(person['phone']['home'])  # 01-4455
# print(person['phone']['mobile'])    # 918-123456

# print(person['children'])   # ['Olivia', 'Sophia']
# print(person['children'][0])    #Olivia

# print(person.pop('age'))    # 48

print(len(person))  # 4
print(person['phone']['home'])  # 01-4455
print(person['phone']['mobile'])    # 918-123456
print(person['children'])   # ['Olivia', 'Sophia']
print(person['children'][0])    #Olivia
print(person.pop('age'))    # 48

# Define Set

# use [] to define a List
# use ( , ) to define a Tuple
# use { : } to define a Dictionary
# use { , } to define a Set

# to define a empty list we use [] or list()
# to define a empty tuple we use () or tuple()
# to define a empty dict we use {} or dict()
# to define a empty set we use set()
f = {'apple', 'orange', 'banana'}
D = set()

# f[0]
# TypeError: 'set' object is not subscriptable

print(type(f))  # <class 'set'>
print(len(f))  # 3
print(f)  # {'orange', 'banana', 'apple'}

for i in f:
    print(i)  # prints elements like other types

m = set(('apple', 'orange', 'banana'))  # to convert to set

print(f == m)  # True

print('cherry' in f)  # False

# Operations to set
# add, update, remove()

f.add('cherry', 'pineapple')  # TypeError: add() takes exactly one argument (2 given)
f.add('cherry')
print(f)  # {'orange', 'banana', 'cherry', 'apple'}

f.update(['mango', 'grapes'])
print(f)  # {'mango', 'apple', 'orange', 'banana', 'grapes'}

f.remove('apple')
print(f)  # {'mango', 'orange', 'banana', 'grapes'}

# add()
# update()
f = {'a', 'b', 'c'}
f.add('d')
# f.add('e', 'f')
f.add(('e2', 'f2')) # {'a', 'd', 'b', ('e2', 'f2'), 'c'}
f.add(['e', 'f'])   # Error
# A = {'a', ['b', 'c']} # Error

f = {'a', 'b', 'c'}
f.update('g')   # { 'a', 'b', 'c', 'g'}
f.update('h', 'i') # {'a', 'b', 'c', 'g', 'h', 'i'}

f = {'a', 'b', 'c'}
f.update(['h', 'i']) # {'a', 'b', 'c', 'h', 'i'}

# Remove()
# Discard()
# Copy()
# pop()
# Clear()

vowels = {'a', 'e', 'i', 'o', 'u'}

# 'k' is not in vowels
vowels.remove('k')  # KeyError: 'k'

vowels.discard('k')  # if 'k' exist in vowels remove it, else no Error

v2 = vowels  # v2 and vowels are dependent

c = vowels.copy()  # c and vowels are independent

print(vowels)  # {'i', 'u', 'a', 'o', 'e'}

x = vowels.pop()  # Removes the last element and returns it

print(x)  # i
print(vowels)  # {'u', 'a', 'o', 'e'}
print(v2)  # {'u', 'a', 'o', 'e'}
print(c)  # {'i', 'o', 'e', 'u', 'a'}

vowels.clear()
print(vowels)  # set()
print(len(vowels))  # 0

del c  # deletes 'c'

# Union and Intersection in Python
x = {1, 2, 3}
y = {2, 3, 4}
print(x.union(y))  # {1, 2, 3, 4}
print(x | y)  # {1, 2, 3, 4}, | is OR

x = {1, 2, 3}
y = {2, 3, 4}
print(x.intersection(y))  # {2, 3}
print(x & y)  # {2, 3}, & is AND

x = {1, 2, 3}
y = {2, 3, 4}
x.update(y) # same as union
print(x)  # {1, 2, 3, 4, 5, 6}

# Difference, Difference_update, Symmetric_difference
A = {1, 2, 3, 4, 5}
B = {2, 4, 7}

print(A - B)  # {1, 3, 5}
print(B - A)  # {7}

r = A.difference(B)
print(r)  # {1, 3, 5}
print(A)  # {1, 2, 3, 4, 5}
print(B)  # {2, 4, 7}

X = {1, 2, 3}
Y = {2, 3, 4}
print(X.symmetric_difference(Y))  # {1, 4}
print(X ^ Y)  # {1, 4}
print(X.union(Y) - X.intersection(Y))  # {1, 4}
print(X.union(Y) - Y.intersection(X))  # {1, 4}

r = A.difference_update(B)
print(r)  # None
print(A)  # {1, 3, 5}
print(B)  # {2, 4, 7}

# SET - EXERCISE 1)
A = {1, 2, 3, 4, 6, 9, 10}
B = {1, 3, 4, 9, 13, 14, 15}
C = {1, 2, 3, 6, 9, 11, 12, 14, 15}

# Possible answers Part 1: {2, 6, 11, 12, 14, 15}
print(C - (A & B & C))
print(C - A.intersection(B).intersection(C))
print(C - C.intersection(A, B))
print(C.difference(A & B & C))

# Part 2 : {4}
print((A & B) - C)
print(A & B - C)

# Part 3 is {4, 10, 13}
print(A.union(B) - C)
print((A | B) - C)

#update function

s = 'Hamed'             # str
a = [13, 15]            # list
t = (7, 8)              # tuple
d = {'one':1, 'two':2}  # dict
x = {56, 98}            #set

x.update(s, a, t, d)

print(x)   # {'one', 98, 7, 8, 'm', 'H', 'e', 13, 15, 'd', 56, 'two', 'a'}

#isdijoint
# X and Y have intersect or not

X = {1, 2}
Y = {1, 2, 3}
print(X.isdisjoint(Y))  # False, there is intersection


X = {1, 2}
Y = {3, 7, 8}
print(X.isdisjoint(Y))  # True, there is no intersection

#issubset
# Subset
# We say that A is a subset of B since every elemnt of A is also in B

A = {1, 2, 4}
B = {1, 2, 3, 4, 5}
print(A.issubset(B))    # True
print(B.issubset(A))    # False

C = set()
print(A.issubset(C))    # False
print(C.issubset(A))    # True

# SET - EXERCISE 2)
#Wich characters of 'a', 'y', 'c', 'o', 'z' in 'Python Course'?

#Output: 'o', 'y'

#Solution (My own!)
s = 'Python Course'
characters = 'aycoz'

print(set(s) & set(characters)) # Works...

# HOMEWORK 1)
# Find match key: value in 2 dictionaries
#
# d1 = {'a': 1, 'b': 3, 'c': 2}
# d2 = {'a': 2, 'b': 3, 'c': 1}
#
# Try use set
#
# Output: {'b':3}

# Solution (My own)
s = set()
d1 = {'a': 1, 'b': 3, 'c': 2}
d2 = {'a': 2, 'b': 3, 'c': 1}
s = d1.items() & d2.items()
print(dict(s))  # Works...

# HOMEWORK 2)

while True:
    a = input("Insert a: ")
    b = input("Insert b: ")
    c = input("Insert c: ")
    d = input("Insert d: ")
    if a.isdigit() != True or b.isdigit() != True or c.isdigit() != True or d.isdigit() != True:
        print("Insert valid numbers.")
        continue
    elif a == b or c == d:
        print("Insert diferent numbers.")
        continue
    else:
        a = int(a)
        b = int(b)
        c = int(c)
        d = int(d)
        break
if a > b:
    a, b = b, a
elif c > d:
    c, d = d, c
else:
    pass
a_b = set(range(a, (b + 1)))
c_d = set(range(c, d))
if a in (a_b & c_d) and not b in (a_b & c_d):
    print("There is an intersection between a and d, not counting d.")
elif a_b == (a_b & c_d):
    print("a to b is a part of c to d.")
elif b in (a_b & c_d) and not a in (a_b & c_d):
    print("There is an intersection from c to b.")
elif c_d == (a_b & c_d):
    print("c to d is a part of a to b.")
else:
    print("There is no intersection between a to b and c to d.")
