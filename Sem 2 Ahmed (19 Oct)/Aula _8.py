# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

# define tuple

# use [ ] to define a list
# use ( ) to define a tuple
t = ('English', 'History', 'Mathematics')
print(t)
print(type(t))  # <class 'tuple'>
print(len(t))  # 3

# To define a tuple with single obj we should use "," after that obj

t1 = (3)  # t1 is integer
t2 = (3,)  # t2 is tuple
s1 = ('a')  # s1 is string
s2 = ('a',)  # s2 is tuple

# Access members Index Slicing
# Access the members in tuple we use [], same as list
t = ("Eng", "His", "Math")

print(t[0])  # Eng
print(t[1:3])  # ("Hist", "Math")
print(t.index("Eng"))  # 0
print("Eng" in t)  # True

# Access members
# Tuples are immutable, same as Strings

t = ("Eng", "His", "Math")

t[0] = 'Art'  # Error

for i in t:
    print(f'I like to read {i}')

# Sum, Min, Max, Count

t = (1, 9, 2)
print(sum(t))  # 12
print(max(t))  # 9
print(min(t))  # 1
print(t.count(9))  # 1

# + and * reversed

t = (1, 9, 2)  # (1, 9, 2, 1, 9, 2)
print(t * 2)  # (1, 9, 2, 1, 9, 2, 1, 9, 2)
print(t + t + t)  # (3, 6, 9)
print((3, 6) + (9,))  # (1, 2, 9, 6)
print((1, 2) + (9, 6))

# reversed()

print(tuple(reversed(t)))  # (2, 9, 1)

# Ordered

t1 = (1, 2)

t2 = (2, 1)

print(t1 == t2)  # False

# Append
# tuples are not changeable
# Here we overwrite t with new value!
t = (4, 6)
t = t + (9,)
print(t)  # (4, 6, 9)

# EXERCISE 1)
#  Exercise append

t = (4, 6)
l = list(t)
l.append(9)
t = tuple(l)
print(t)

# EXERCISE 2)
#  Exercise try to change the String

t = "$$Python$#Course $"
s = ""
for i in t:
    if (i.isalnum()):
        s += i
    if (i == '#'):
        s += ' '
print(s)

# Remove
# Remove a members from tuple only possible by converting to another data type!

t = (4, 7, 2, 9, 8)
x = list(t)
x.remove(2)
t = tuple(x)
print(t)  # (4, 7, 9, 8)

# Unpack
# Example 1
t = (4, 8)
a, b = t
print(a)  # 4
print(b)  # 8

# Example 2
car = ("blue", "auto", 7)
color, _, a = car
print(color)  # blue
print(_)  # auto
print(a)  # 7

# zip, zip(*)
a = (1, 2)
b = (3, 4)
c = zip(a, b)
x = list(c)
print(x)  # [(1, 3), (2, 4)]
print(x[0])  # (1, 3)
print(type(x[0]))  # <class 'tuple'>

# z can be tuple or list
z = ((1, 3), (2, 4))  # or z = [(1, 3), (2, 4)]
u = zip(*z)
print(list(u))  # [(1, 2), (3, 4)]

# Example 1
a = (1, 2, 'A')
b = (3, 4, 8)
c = zip(a, b)
x = list(c)
print(x)  # [(1, 3), (2, 4), ('A', 8)]
print(list(zip(*x)))  # [(1, 2, 'A'), (3, 4, 8)]

# minimum length between 'a' and 'range(2)' is 2
a = [11, 22, 33]
b = zip(a, range(2))
print(list(b))  # [(11, 0), (22, 1)]

# EXERCISE 3)
#  Exercise Try Zip with 3 input variables.
a = [1, 2, 'A']
b = ('Python', 161.8, 0, 5)
c = {10, 12, 14, 16, 18, 20}

# Output ==> print(list(zip(A, B, C)))
# [(1, 'Python', 10), (2, 161.8, 12), ('A', 0, 14)]
# Write program with the same input and output
# without using Zip() function?

d = list(b)
e = list(c)
aList = list()

length = min(len(a), len(b), len(c))
for i in range(0, length):
    x = (a[i], d[i], e[i])
    aList.append(x)
print(aList)

# isinstance()

# EXERCISE 4)
# How many tuples are in 'num' list
num = [8, 2, (9, 3), 4, (1, 6, 7), 34]
c = 0
for i in num:
    if type(i) == tuple:
        c += 1
print(c)  # 2

# Example
a = [(1, 2, 3), (4, 5, 6)]
b = [i[:-1] + (9,) for i in a]
print(b)  # [(1, 2, 9), (4, 5, 9)]

# HOMEWORK 1)

# Dont use another form of loop
# transform in a list

# Resolution
a = [(1, 2, 3), (4, 5, 6)]
c = a.copy()
for i in range(0, len(a)):
    c[i] = a[i][0], a[i][1], 9
print(c)

# HOMEWORK 2)
my_obj = enumerate((1, 3))
for i, j in my_obj:
    print("i is: ", i)
    print("j is: ", j)

# HOMEWORK 3)
inp = [(1, 3, 5), (2, 4), ('A', 8)]
otp = []  # Cria lista vazia
x, y, z = inp  # Unpack
for i in range(len(inp) - 1):  # Itera 2x e dá-nos o indice
    w = (x[i], y[i], z[i])  # var temporária para criar o tuple
    otp.append(w)  # add tuple a lista
print(otp)
