# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------
class Node: # Class node

    def __init__(self, data):   # Constructor
        self.data = data            # Data part of the node, used to store information and can be multiple variables
        self.next = None            # Pointer part of the note, used to link nodes

class CircularLinkedList:

    def __init__(self):     # Constructor
        self.head = None        # Empty pointer that will point to the first element. It can't get lost...

    def print_list(self):
        temp = self.head            # Create a temporary pointer that will iterate circular list.
        while temp:                 # while temp is different of None ( null in Python )
            print(temp.data)            # Print node data
            if temp.next == self.head:      # Condition to break the loop, as there is no 'None'
                break

    def append(self, data):
        if not self.head:           # if self.head is different from Null
            self.head = Node(data)      # self.head points to a new Node with 'data'
        else:
            newNode = Node(data)
            temp = self.head            # Create temporary pointer to iterate
            while temp != self.head:    # While temporary pointer dosen´t point to the first (this is a circular list)
                temp = temp.next            # Iterate temporary 1 element to front, until it reaches the first
            temp.next = newNode         # In last element put pointer pointing to new node
            newNode.next = self.head    # In new Node put pointer pointing to self.head (make it circular)

    def prepend(self, data):
        newNode = Node(data)
        newNode.next = self.head
        temp = self.head
        if not self.head:
            newNode = newNode.next
        else:
            while temp != self.head:
                temp = temp.next
            temp.next = newNode
        self.head = newNode
    #TODO
    def remove(self, key):
        if self.head:
            if self.head.data == key:
                trav = self.head
                while trav.next != self.head:
                    trav = trav.next
                if self.head == self.head.next:
                    self.head = None
                else:
                    trav.next = self.head.next
                    self.head = self.head.next
            else:
                trav = self.head
                prev = None
                while trav.next != self.head:
                    prev = trav
                    trav = trav.next
                    if trav.data == key:
                        prev.next = trav.next
                        trav = trav.next

    def __len__(self):
        cur = self.head
        count = 0
        while cur:
            count += 1
            cur = cur.next
            if cur == self.head:
                break
        return count

    def split_list(self):
        size = len(self)

        if size == 0:
            return None
        if size == 1:
            return self.head

        mid = size//2
        count = 0

        prev = None
        cur = self.head

        while cur and count < mid:
            count += 1
            prev = cur
            cur = cur.next
        prev.next = self.head

        split_cllist = CircularLinkedList()
        while cur.next != self.head:
            split_cllist.append(cur.data)
            cur = cur.next
        split_cllist.append(cur.data)

        self.print_list()
        print("\n")
        split_cllist.print_list()