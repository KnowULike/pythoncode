# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha ----------------------------------------------------------------------------------------------
Created on 11/01/2021 -----------------------------------------------------------------------------------------------
"""


# -------------------------------------------------------------------------------------------------------------------

# Logarithmic Scale

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 10, 10)
y = x
plt.plot(x, y, '-o')
plt.grid()
plt.show()

# SemilogY ----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 10, 10)
y = x
plt.plot(x, y, '-o')
plt.semilogy()
plt.grid()
plt.show()

# SemilogX ----------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 10, 10)
y = x
plt.plot(x, y, '-o')
plt.semilogx()
plt.grid()
plt.show()

# y & x scale ------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 10, 10)
y = x
plt.plot(x, y, '-o')
plt.yscale('log')
plt.xscale('log')
plt.grid()
plt.show()

#   (a) The streptococci bacteria population N at t (in months) is given by
# N0e^2t where N0 is the initial population. If the initial population was 100,
# how long does it take for the population to reach one million?
import matplotlib.pyplot as plt
import numpy as np
t = np.linspace(0, 10, 11)
y = 100 * np.exp(2 * t) - 1000000
plt.plot(t, y, "-o")
plt.axhline(y=10**6, color='r', linestyle='-')
# TODO lambda
plt.semilogy()
plt.grid()
plt.show()

# Consider the data

# | X |   2  |  30  |  70  | 100  | 150  |
# | Y | 4.24 | 16.4 | 25.1 | 30.0 | 36.7 |

import matplotlib.pyplot as plt
import numpy as np

x = [2, 30, 70, 100, 150]
y = [4.24, 16.4, 25.1, 30.0, 36.7]
plt.xscale('log')
plt.yscale('log')
plt.plot(x, y, '-o')
plt.grid()
plt.show()

logx = np.log10(x)
logy = np.log10(y)

print(np.poly1d(np.polyfit(logx, logy, 1)))
# TODO: Calcular equação An^x

plt.plot(logx, logy, 'o')
plt.grid()
plt.show()


# Exercises


# The data below obeys a power law. y = Ax^n.
# Obtain the equation and select the correct statement.

# | X | 5  |  15  |  30  | 50   | 95   |
# | Y | 10 |  90  |  360 | 1000 | 3610 |

# (a) n = 3
# (b) A = 3/2
# (c) n = 4
# (d) A = 1/2

import matplotlib.pyplot as plt
import numpy as np

x = [5, 15, 30, 50, 95]
y = [10, 90, 360, 1000, 3610]

plt.xscale('log')
plt.yscale('log')
plt.plot(x, y, '-o')
plt.grid()
plt.show()

logx = np.log10(x)
logy = np.log10(y)

print(np.poly1d(np.polyfit(logx, logy, 1)))
# TODO: Calcular equação An^x
print(np.log10(0.3979))  # -0.40022606085361173
print(10**0.3979)
# 2.5.e^-2

plt.plot(logx, logy, 'o')
plt.grid()
plt.show()

