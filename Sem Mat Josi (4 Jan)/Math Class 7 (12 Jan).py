# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(dd/mm/yyyy)---------------------------------------------------------------------------------------------
"""


# -------------------------------------------------------------------------------------------------------------------

# Standart Deviation using numpy
import numpy

dados = [10, 30, 50]
print(numpy.std(dados, ddof=1))

dados2 = [7.0, 8.0, 8.5]
print(numpy.std(dados2, ddof=1))


# Standart Deviation using statistics
import statistics

dados = [10, 30, 50]
print(statistics.stdev(dados))

dados2 = [7.0, 8.0, 8.5]
print(statistics.stdev(dados2))

# Quartile
# Mean, Median
import numpy

values = [3, 6, 7, 11, 13, 22, 30, 40, 44, 50, 52, 61, 68, 80, 94]
print(numpy.mean(values))
print(numpy.median(values))

# Systematic Sample

sysSample = []
sample = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
print(numpy.mean(sample))
step = 2

for i in range(0, len(sample), step):
    sysSample.append(sample[i])

sysMean = numpy.mean(sysSample)
print(sysSample)
print(sysMean)

# Cluster Sample

sysSample = []
sample = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
print(numpy.mean(sample))
step = 3

for i in range(0, len(sample), step):
    sysSample.append(sample[i])
    sysSample.append(sample[i+1])

sysMean = numpy.mean(sysSample)
print(sysSample)
print(sysMean)

# Stratified sample

import numpy
import random

def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [alist[i*length // wanted_parts: (i+1)*length // wanted_parts]
            for i in range(wanted_parts)]

stratified_list =[]

sample = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

print('mean of all values:', numpy.mean(sample))

A, B, C = split_list(sample, 3)
splited_list = [A, B, C]

for i in range(0, len(splited_list)):
    stratified_list.append(splited_list[i][random.randint(0, 4)])

systematic_mean = numpy.mean(stratified_list)

print(stratified_list)
print('stratified mean', systematic_mean)

# Statistics in Python (scipy)

from numpy import mean
from numpy import median
from numpy import var
from numpy import std
from numpy import percentile
from scipy.stats import mode
from scipy.stats import gmean
from scipy.stats import hmean

numbers = [1, 1, 2, 2, 3, 3, 4, 1, 2]
print(numbers)

Mean = mean(numbers)
Median = median(numbers)
Mode = int(mode(numbers)[0])  # index 1 gives frequency
print("Mean = ", Mean, "\nMedian = ", Median, "\nMode = ", Mode)

Geometric_Mean = gmean(numbers)
Harmonic_mean = hmean(numbers)
print("Geometric Mean = ", Geometric_Mean, "\nHarmonic Mean = ", Harmonic_mean)

Variance = var(numbers)
Standard_Deviation = std(numbers)
print("Variance = ", Variance, "\nStandard deviation = ", Standard_Deviation)

Q1 = percentile(numbers, 25)
Q2 = percentile(numbers, 50)
Q3 = percentile(numbers, 75)
print("Q1 = ", Q1, "\nQ2 = ", Q2, "\nQ3 = ", Q3)    # 1 1 1 2 2 2 3 3

# Output.: --------------------------------------------------------------------

# [1, 1, 2, 2, 3, 3, 4, 1, 2]
# Mean =  2.111111111111111
# Median =  2.0
# Mode =  1
# Geometric Mean =  1.8761425449123872
# Harmonic Mean =  1.6615384615384616
# Variance =  0.9876543209876544
# Standard deviation =  0.9938079899999066
# Q1 =  1.0
# Q2 =  2.0
# Q3 =  3.0

# -----------------------------------------------------------------------------

# Exercises

# 1. Ten observations Xi are given:

data = [4, 7, 2, 9, 12, 2, 20, 10, 5, 9]
sorted_data = sorted(data)
print(sorted_data)
# Determine the median, upper, and lower quartile and the inter-quartile range.

# Solution

from numpy import median
from numpy import quantile
import pandas as pd
from statistics import quantiles
df = pd.DataFrame(data)
print(quantile(data, .25))
print(quantiles(data, n=4, method="exclusive"))
print(quantiles(data, n=5, method="inclusive"))

print(df.quantile(0.25, interpolation='linear'))
print(df.quantile(0.25, interpolation='lower'))
print(df.quantile(0.25, interpolation='higher'))
print(df.quantile(0.25, interpolation='midpoint'))
print(df.quantile(0.25, interpolation='nearest'))
print(quantile(sor, .25))
print(quantile(sor, .75))



print(median(data))  # 8
Q1 = percentile(data, 25)
print(Q1)
Q3 = percentile(data, 75)
print(Q3)
print(percentile(data, 50))

# 2. Four observations Xi are given:

values = [2, 5, 10, 11]

# Determine the mean, empirical variance, and empirical standart deviation.

from numpy import mean
from numpy import var
from numpy import std

print(mean(values))
print(var(values))
print(std(values))

# 3. Find the median of the data in the Figure (see is slides 'Aula6')

import matplotlib.pyplot as plt


bars = [[0: 4], [1: 1], [2: 2], [3: 1], [4: 3], [5: 2], [6: 1], [7, 1]]
plt.plot(bars)


# 4. Find the standard deviation of the data in the (see is slides 'Aula6')
