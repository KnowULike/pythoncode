# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: %(António Rocha)--------------------------------------------------------------------------------------------
Created on %(05/01/2021)---------------------------------------------------------------------------------------------
"""


# -------------------------------------------------------------------------------------------------------------------

import math
import matplotlib.pyplot as plt
from pandas import np

# Exponencial

# Expoent- identifies the multiplification factor.
# Base - the number to be multiplied.

# 5^2 = ? -> 5 * 5 = 25 (Below in code!)

print(5**2)  # 25

# Logarithms

# 5^2 = ? -> 5 * 5 = 25
# log5(25) = 2 <- Exponent (Below in code!)

print(math.log(25, 5))  # Exponent = 2.0

# Radicals
#  (?)^2 = 25
#  Square root of 25 = 5 <- Base (Below in code!)

print(np.sqrt(25))  # Base = 5.0

# Exponentials - properties

# Zero power (x)^0 = 1
# Negative Expoents
# Power of a power
# Rational Exponents
# Quotient of powers
# Product of powers
# Power of a quotient
# Power of a product

# Exponencials - Plotting Exercises

# 1) ((3^2)/3) = 3
x = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
y = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]
plt.plot(x, y, colors='black')
plt.plot(3, 0, 'o', color='r')
plt.title('Exercise 1')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc='upper left')
plt.grid()
plt.show()

# 2) ((3.n.m^2)/3.n) = m^2
import matplotlib.pyplot as plt
import numpy as np

plt.hlines(y=0, xmin=-5, xmax=5, colors='black')
plt.vlines(x=0, ymin=-5, ymax=5, colors='black')
px = [-5, 5]
plt.plot(x, y, label='y=px**2')
plt.title('Exercise 2')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc='upper left')
plt.grid()
plt.show()

# 3) ((4.x^3.y^4)/3.x.y^3)
# 4) (x^3.y^4 * 2.x^2.y^3)^2
# 5) 5.x.(x^4.y^4)^4
# 6) ((3^4)/3)
# 7) ((x^2.y^4)/4.x.y)
# 8) ((x.y^3)/4.x.y)
# 9) (u^2.v^2 * 2.u^4)^3
# 10) ((3.v.u^5 * 2.v^3)/u.v^2 * 2.u^2.v)

# TODO: Log EXPLANATION

# Logarithms - Exercises

# Write each equation in its exponential form:
# 1) log7(X)
# 2) 3 = log10( X + 8 )
# 3) log5(125) = X

# Rewrite into logarithms

# 1) 2^4 = 16
# 2) 64^-(1/2) = 8
# 3) e^4 = 54.60

# TODO: TPC -> Exercicios antes dos radicais

# Radicals * 3



# Plots

#  Exponent
import matplotlib.pyplot as plt
import numpy as np
X = np.linspace(0, 8, 10)
formula = 2**X - 3
plt.plot(X, formula, "-m", label="y = (2 ** X) - 3")
plt.legend("Exponential Function")
plt.xlabel("X")
plt.ylabel("Y")
plt.legend(loc="upper left")
plt.grid()
plt.show()

#  Radical
import matplotlib.pyplot as plt
import numpy as np
X = np.linspace(0, 100, 10)
formula = np.sqrt(X)
plt.plot(X, formula, "-m", label="y = sqrt(x)")
plt.legend("Radical graph")
plt.xlabel("X")
plt.ylabel("Y")
plt.legend(loc="upper left")
plt.grid()
plt.show()

#  Logarithm
import matplotlib.pyplot as plt
import numpy as np
X = np.linspace(0, 100, 10)
formula = np.log(X)
plt.plot(X, formula, "-m", label="y = log(x)")
plt.legend("Log graph")
plt.xlabel("X")
plt.ylabel("Y")
plt.legend(loc="upper left")
plt.grid()
plt.show()

# Multiple (Exp) functions for comparison #

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 3, 10)
y1 = 2 ** x
y2 = 3 ** x
y3 = (2 ** x) + 4
y4 = (2 ** (x-1))

plt.plot(x, y1, 'm', label="2 ** x")
plt.plot(x, y2, 'b', label="3 ** x")
plt.plot(x, y3, 'r', label="(2 ** x) + 4")
plt.plot(x, y4, 'g', label="(2 ** (x - 1))")

plt.title("Exponential functions")
plt.xlabel("X")
plt.ylabel("Y")
plt.legend(loc="upper left")
plt.grid()
plt.show()

# Multiple "type of" functions for comparison

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 4, 5)
y1 = np.sqrt(x)
y2 = np.log(x)
y3 = 2 ** x

plt.plot(x, y1, '-m', label="y = sqrt(x)")
plt.plot(x, y2, '-b', label="y = log(x)")
plt.plot(x, y3, '-r', label="y = (2 ** x)")

plt.title("Comparison graph (exp, rad and log)")
plt.xlabel("X")
plt.ylabel("Y")
plt.legend(loc="upper left")
plt.grid()
plt.show()
