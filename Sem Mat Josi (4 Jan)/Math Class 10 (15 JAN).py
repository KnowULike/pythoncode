# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha ----------------------------------------------------------------------------------------------
Created on 15/01/2021   ---------------------------------------------------------------------------------------------
"""


# -------------------------------------------------------------------------------------------------------------------

from scipy.stats import uniform
import matplotlib.pyplot as plt
data = uniform.rvs(size=100000, loc=5, scale=10)
ax = plt.hist(data, bins=30)
ax.set(xlabel="interval")
plt.show()

x=np.linspace(-10, 10, 100)
y = x**2
plt.hist(y, 100, density=1)
plt.show()
plt.plot(x, y, "o")

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import uniform
x=np.linspace(0.001, 10, 100)
pdf = expon.pdf(x)
plt.plt(x, pdf, '-r', lw=2, alpha=0.6, label="expon pdf")
plt.xlabel("intervals")
plt.ylabel("Probabilitiy density")
plt.show()

x=np.linspace(-10, 10, 100)
y = x**3
plt.hist(y, 100, density=1)
plt.show()
plt.plot(x, y, "o")
plt.show()

n = 1000
x = linspace(0,6*np.pi,n)
sig1 = np.sin(x)
plt.hist(sig1, 100)
plt.show()
plt.plot(x, y, "o")
plt.show()

# Gaussian Distribution...

# LOC - (Mean) where the peak of the bell exists.
# SCALE - (Standart Deviation) how flat the graph distribution should be.
# Size - The shape of the returned array

from numpy import random
import matplotlib.pyplot as plt

x2 = random.normal(loc=0, scale=5, size=10000)
x = random.normal(loc=0, scale=1, size=10000)
plt.vlines(x=0, ymin=0, ymax=3000, color="k")
x1 = random.normal(loc=1, scale=2, size=10000)
plt.vlines(x=1, ymin=0, ymax=3000, color="r")
plt.hist(x2)
plt.hist(x1)
plt.hist(x)
plt.show()


from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt

data = np.random.normal(loc=0, size=1000)
mean, std = norm.fit(data)
plt.hist(data, bins=30, density=True)
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
y = norm.pdf(x, mean, std)
plt.plot()
plt.show()

# Central Limit Theorem

# General Idea: Regardless of the population distribution model, as the sample
# size increases, the sample mean tends to be normally distributed around the
# population mean, and its standart deviation shrinks as n increases.

# Certain conditions must be met to use the CLT.

# Independent Samples Test
# . "Randomization": Each sample should represent a random sample from the population, or at least follow the population distribution.

# Large Enough Sample Size
# . Sample size n should be large enough so that np >= 10 and nq >= 10

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import random as rd

def random_samples(population, sample_qty, sample_size):
    sample_means = []
    for i in range(sample_qty):
        sample = rd.sample(population, sample_size)
        sample_mean = np.array(sample).mean()
        sample_means.append(sample_mean)
    return sample_means

uniform = np.random.rand(100000)

print("mean =", np.mean(uniform))
plt.figure(figsize=(7, 4))
plt.title("Distribution", fontsize=15)
plt.hist(uniform, 100)

samples_from_normal = random_samples(list(uniform), 10, 10)
plt.figure()
plt.title("Dostribution Results", fontsize=15)
sns.distplot(samples_from_normal, color='g')
plt.show()
