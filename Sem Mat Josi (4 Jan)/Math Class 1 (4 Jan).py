# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha  ---------------------------------------------------------------------------------------------
Created on 04/01/2021   ---------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------

# Math Functions

# Math functions definition originates from equations that have at most one output for any input.
# Usually, 'Y' is the variable that we want to find and 'X' is the variable we have in order to evaluate 'Y'.

# y = f(x)

#   _x_         f(x)         _y_
#    1 ---------------------> a
#    2 ---------------------> b
#    3 ---------------------> c
#    4 ---------------------> d

# Plotting function's in Python ------------------------------------------------
# Linear Graph --------------------------------------------------------------

import matplotlib.pyplot as plt
import numpy as np
plt.title('Linear graphs ')     # Plot title
plt.xlabel('x')     # Plot X label
plt.ylabel('y', rotation=0)     # Plot Y label

# 1st Half of plot [-5 to 0]
x = np.linspace(-5, 0, 5)   # generate 5 evenly spaced numbers from -5 until 0.
y = x-3

# Middle point
x1 = 0
y1 = 1

# 2nd Half of plot [0 to 4]
x2 = np.linspace(0, 4, 5)
y2 = x+2

plt.plot(x, y, '-r', label='y=x+2')     # Plot half red
plt.plot(x1, y1, '-g', label='y=1')     # Plot middle point green
plt.plot(x2, y2, '-m', label='y=x-3')   # Plot 2nd half magenta
plt.legend(loc='upper left')    # Plot legend
plt.grid()  # Plot grid lines
plt.show()  # Show plot

# ------------------------------------------------------------------------------
# Increasing Functions ---------------------------------------------------------
# As 'x' moves right, 'y' gets bigger
# ------------------------------------------------------------------------------

import matplotlib.pyplot as plt
import numpy as np

plt.title("Increasing Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-5, 5, 11)
y = x
plt.plot(x, y, '-b')
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Decreasing Functions ---------------------------------------------------------
# As 'x' moves right, 'y' gets smaller
# ------------------------------------------------------------------------------

import matplotlib.pyplot as plt
import numpy as np

plt.title("Decreasing Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-5, 5, 11)
y = -x
plt.plot(x, y, '-b')
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Parallel linear Functions & Perpendicularity ---------------------------------
# A function is said to be parallel to another when the slope(m) is equal.
# On another side a function is perpendicular when m1 * m2 = -1
# ------------------------------------------------------------------------------

import matplotlib.pyplot as plt
import numpy as np

plt.title('Generic linear plot.')
plt.xlabel('x')
plt.ylabel('y', rotation=0)
x = [-5, 5]
m = [1, 2, 3, 5]
b = [-2, 3 / 4, 2, 1]
color = ['g', 'r', 'k', 'm']
for i in range(0, len(m)):
    y = m[i] * np.array(x) + b[i]
    plt.plot(x, y, label='y=%sx+%s' % (m[i], b[i]), color=color[i])

plt.axis('auto')
plt.xlim(x)
plt.ylim(x)
plt.grid()
axis = plt.gca()
plt.legend(prop={'size': 15})
plt.show()

# ------------------------------------------------------------------------------
# Constant Functions -----------------------------------------------------------
# Example 1. x = 6
# Example 2. f(x) = 3

import matplotlib.pyplot as plt

plt.title('Constant graphs')
plt.xlabel('x')
plt.ylabel('y', rotation=0)
plt.hlines(y=3, xmin=1, xmax=10, colors='r')
plt.vlines(x=6, ymin=1, ymax=10, colors='m')
plt.legend(loc='upper left')
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Functions Properties

# Addition       (f + g)(x) = f(x) + g(x)
# Subtraction    (f - g)(x) = f(x) + g(x)
# Multiplication (f . g)(x) = f(x) . g(x)
# Division       (f / g)(x) = f(x) / g(x)

# Exercises

# 1.
# f(n) = n - 5
# g(n) = 4n + 2
# Find (f + g)(-8)

# 2.
# g(a) = 3a - 2
# h(a) = 4a - 2
# Find (g + h)(-10)

# 3.
# g(x) = x**2 - 2
# h(x) = 2x + 5
# Find g(-6) + h(-6)

# 4.
# h(t) = t + 5
# g(t) = 3t - 5
# Find (h . g)(5)

# 5.
# h(n) = 2n - 1
# g(n) = 3n - 5
# Find h(0) / g(0)

# 6.
# g(t) = t - 3
# h(t) = -3t^3 + 6t
# Find g(1) + h(1)

# ------------------------------------------------------------------------------
# Continuous plynomial functions types -----------------------------------------

# Quadratic Function

import matplotlib.pyplot as plt
import numpy as np
plt.title("Quadratic Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-10, 10, 10)
y1 = x**2
plt.plot(x, y1, '-r')
plt.grid()
plt.show()

# Cubic Function

import matplotlib.pyplot as plt
import numpy as np

plt.title("Cubic Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-10, 10, 10)
y1 = -3 * x**3 + 6 * x
plt.plot(x, y1, '-b')
plt.grid()
plt.show()

# Quartic Graph

import matplotlib.pyplot as plt
import numpy as np

plt.title("Quartic Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-10, 10, 10)
y1 = -3 * x**4 + 6 * x + 2 * x
plt.plot(x, y1, '-b')
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Maximum and minimum values ---------------------------------------------------

# Quadratic Function

import matplotlib.pyplot as plt
import numpy as np

plt.title("Quadratic Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-10, 10, 10)
y1 = x**2
plt.plot(x, y1, '-m')
plt.plot(0, 0, 'o', color='r')  # Minimum value (0, 0)
plt.grid()
plt.show()

# Cubic Function

import matplotlib.pyplot as plt
import numpy as np

plt.title("Cubic Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-10, 10, 10)
y1 = -3 * x**3 + 6 * x
plt.plot(x, y1, '-b')
plt.plot(0, 0, 'o', color='r')  # Inflexion point (0, 0)
plt.grid()
plt.show()

# Quartic Graph

import matplotlib.pyplot as plt
import numpy as np

plt.title("Quartic Function")
plt.xlabel("x")
plt.ylabel("y", rotation=0)
x = np.linspace(-10, 10, 10)
y1 = -3 * x**4 + 6 * x + 2 * x
plt.plot(x, y1, '-g')
plt.plot(0, 0, 'o', color='r')  # Maximum value (0, 0)
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Discrete Function ------------------------------------------------------------

import matplotlib.pyplot as plt

plt.title('Discrete function')
x = [-3, -2, -1, 0, 1, 2, 3]
y = [8, 4, 2, 0, 2, 4, 8]
color = ['g', 'r', 'k', 'm', 'k', 'r', 'g']

for i in range(0, len(x)):
    plt.plot(x[i], y[i], 'o', color=color[i])   # Plot a point at each iteration

plt.legend(loc='upper left')
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Polynomial regression --------------------------------------------------------

import numpy
import matplotlib.pyplot as plt

x = [1, 2, 3, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 18, 19, 21, 22]
y = [100, 90, 80, 60, 60, 55, 60, 65, 70, 70, 75, 76, 78, 79, 90, 99, 99, 100]

model = numpy.poly1d(numpy.polyfit(x, y, 8))
line = numpy.linspace(1, 22, 100)

plt.scatter(x, y)
plt.plot(line, model(line))
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Exercises - Linear regression ------------------------------------------------

# The following example describes the expenditure (in dollars) on recreation per
# month by employees at a certain company, and their corresponding monthly
# incomes.

# ----------------------------------
# Expenditure ($) |     Income ($) |
# ----------------------------------
#      2400       |     41200      |
#      2650       |     50100      |
#      2350       |     52000      |
#      2950       |     66000      |
#      3100       |     44500      |
#      2500       |     37700      |
#      5106       |     73500      |
#      3100       |     37500      |
#      2900       |     56700      |
#      1750       |     35600      |
# ----------------------------------

# What is the slope, and the y-intercept ?
# Write the linear regression equation that represents the data in the table.

import numpy as np
import matplotlib.pyplot as plt

x = [2400, 2650, 2350, 4950, 3100, 2500, 5106, 3100, 2900, 1750]
y = [41200, 50100, 52000, 66000, 44500, 37700, 73500, 37500, 56700, 35600]

model = numpy.poly1d(numpy.polyfit(x, y, 1))
print(model)    # 9.774 x + 1.937e+04

# Answer:
# Linear regression equation: y = 9.774 x + 1.937e+04
# Slope = 9.774
# y-intersect = 1.937e+04

# Visualization:
line = np.linspace(-100, 5200, 75000)
plt.scatter(x, y, color="red")
plt.plot(line, model(line), "m")
plt.grid()
plt.show()

# ------------------------------------------------------------------------------
# Plotting discrete functions using LISTS --------------------------------------

import matplotlib.pyplot as plt

plt.title('Discrete function using lists')
plt.xlabel('x')
plt.ylabel('y', rotation=0)

x = [1, 2, 3, 4, 5]
y = [1, 2, 3, 4, 5]

plt.plot(x, y, 'or', label='y = x')
plt.plot(x, y, 'b', label='y = x')

plt.legend(loc='upper left')
plt.grid()
plt.show()

# Plotting discrete functions using TUPLES -------------------------------------

import matplotlib.pyplot as plt

plt.title('Discrete function using tuples')
plt.xlabel('x')
plt.ylabel('y', rotation=0)

x =(1, 2, 3, 4, 5)
y = (1, 2, 3, 4, 5)

plt.plot(x, y, 'or', label='y = x')
plt.plot(x, y, 'b', label='y = x')

plt.legend(loc='upper left')
plt.grid()
plt.show()

# Plotting discrete functions using DICTIONARIES -------------------------------

import matplotlib.pyplot as plt

plt.title('Discrete function using dictionaries')
plt.xlabel('x')
plt.ylabel('y', rotation=0)

x1 = {'x': [1, 2, 3, 4, 5], 'y': [1, 2, 3, 4, 5]}

plt.plot(x, y, 'or', label='y = x')
plt.plot(x, y, 'b', label='y = x')

plt.legend(loc='upper left')
plt.grid()
plt.show()
