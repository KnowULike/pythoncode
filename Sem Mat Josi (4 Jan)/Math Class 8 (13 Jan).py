# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha ----------------------------------------------------------------------------------------------
Created on 13/01/2021 -----------------------------------------------------------------------------------------------
"""

# -------------------------------------------------------------------------------------------------------------------

import numpy as np
import random
import matplotlib.pyplot as plt

simulation_number = 10000
club = 0
spade = 0
diamond = 0
heart = 0
percentages = []
for i in range(0, simulation_number):
    card_points = [1, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2]
    card_signs = ["HEART", "CLUB", "DIAMOND", "SPADE"]
    random_point = random.choice(card_points)
    random_sign = random.choice(card_signs)
    random_card = random_point, random_sign
    # print (random_card)
    ## Condition

    if (random_card[0] == 7 and random_card[1] == "CLUB"):
        club += 1
x = ["CLUB"]
percentages.append(np.round(club / simulation_number * 100, 2))

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
ax.bar(x, percentages)
print(sum(percentages), "%")

# Example:

# A bag contains fifteen balls only by their colours; Ten are blue and five are red.
# I reach into


import numpy as np
import random
import matplotlib.pyplot as plt

simulation_number = 10000
club = 0
spade = 0
diamond = 0
heart = 0
percentages = []
for i in range(0, simulation_number):
    card_points = [1, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2]
    card_signs = ["HEART", "CLUB", "DIAMOND", "SPADE"]
    random_point = random.choice(card_points)
    random_sign = random.choice(card_signs)
    random_card = random_point, random_sign
    # print (random_card)
    ## Condition

    if ((random_card[1] == "HEART") or (random_card[1] == "DIAMOND") or
            (random_card[0] == 1 and random_card[1] == "CLUB") or
            (random_card[0] == 1 and random_card[1] == "CLUB")):
        club += 1
x = ["CLUB"]
percentages.append(np.round(club / simulation_number * 100, 2))

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
ax.bar(x, percentages)
print(sum(percentages), "%")

# Joint Probability

import numpy as np
import random
import matplotlib.pyplot as plt

simulation_number = 10000
club = 0
spade = 0
diamond = 0
heart = 0
percentagesClub = []
percentagesSpade = []
for i in range(0, simulation_number):
    card_points = [1, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2]
    card_signs = ["HEART", "CLUB", "DIAMOND", "SPADE"]
    random_point = random.choice(card_points)
    random_sign = random.choice(card_signs)
    random_card = random_point, random_sign
    # print (random_card)
    ## Condition

    if (random_card[0] == 7 and random_card[1] == "CLUB"):
        club += 1
    if (random_card[0] == 7 and random_card[1] == "SPADE"):
        spade += 1

x = ["CLUB"]
y = ["SPADE"]
percentagesClub.append(np.round(club / simulation_number * 100, 2))
percentagesSpade.append(np.round(spade / simulation_number*100, 2))

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
ax.bar(x, percentagesClub)
ax.bar(y, percentagesSpade)
percentage = sum(percentagesSpade) + sum(percentagesSpade)
print(percentage, "%")

# 1. A fair coin is tossed, and a fair dice is thrown. write down sample spaces for

# (a) The toss of the coin;
print("Heads or Tails (solution = 2)")
# (b) The throw of a dice;
print("{1, 2, 3, 4, 5, 6}")
# (c) The combination of these experiments.
print("[{Heads:1}, {Heads:2}, {Heads:3}, {Heads:4}, {Heads:5}, {Heads:6}, {Tails:1},"
      " {Tails:2}, {Tails:3}, {Tails:4}, {Tails:5}, {Tails:6},]")

# 2. What is the chance of rolling a “4” with a dice?

# Sample Space (ss) = 1
# Possible Outcomes = 6

import matplotlib.pyplot as plt
import numpy as np
import random

labels = 'Other Throws', '4'
simulation_number = 10000
fours = 0
percentage = 0
for i in range(0, simulation_number):
    ss = [1, 2, 3, 4, 5, 6]
    throw = random.choice(ss)
    # Condition
    if (throw == 4):
        fours += 1

percentage = np.round(fours / simulation_number * 100, 2)
print(percentage, "%")


sizes = [100-percentage, percentage]
explode = (0, 0.1)
fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, labels=labels,autopct='%1.1f%%',
        shadow=True, startangle=45)
ax1.axis('equal')
plt.title("Percentages (Throw of a Dice)")
plt.show()

# 3. Find the probability of the sum of 2 dices been > 7 or odd ?

# Sample Space (ss) = 26
# Possible Outcomes = 36

import numpy as np
import random

simulation_number = 10000
desiredOutcome = 0
percentage = []
for i in range(0, simulation_number):
    diceThrow = [1, 2, 3, 4, 5, 6]
    firstThrow = random.choice(diceThrow)
    secondThrow = random.choice(diceThrow)
    throwResult = firstThrow + secondThrow
    # Condition
    if (throwResult > 7 or throwResult % 2 != 0):
        desiredOutcome += 1
percentage.append(np.round(desiredOutcome / simulation_number * 100, 2))

print(sum(percentage), "% (Probability of being bigger then 7 and an odd number)")
# Result: 75% aprox.

# 3. Find the probability of the sum of 2 dices been > 7 or Even ?
# Explain why it is different from the exercise 3.

# Sample Space (ss) = 26
# Possible Outcomes = 36

import numpy as np
import random

simulation_number = 10000
desiredOutcome = 0
percentage = []
for i in range(0, simulation_number):
    diceThrow = [1, 2, 3, 4, 5, 6]
    firstThrow = random.choice(diceThrow)
    secondThrow = random.choice(diceThrow)
    throwResult = firstThrow + secondThrow
    # Condition
    if (throwResult > 7 or throwResult % 2 == 0):
        desiredOutcome += 1
percentage.append(np.round(desiredOutcome / simulation_number * 100, 2))

print(sum(percentage), "% (Probability of being bigger then 7 and an even number)")
# Result: 66% aprox.

# Luis

import matplotlib.pyplot as plt
import numpy as np

dice = {1, 2, 3, 4, 5, 6}
posOutcomes = len(dice)**2
sampleSpace = [throw1 + throw2 for throw1 in dice for throw2 in dice]
# outcome_count = { outcome : possible_outcomes.count(outcome)/nr_possible_outcomes for outcome in possible_outcomes}
outcomes = {occurence: np.round(sampleSpace.count(occurence)/posOutcomes * 100, 2) for occurence in sampleSpace}
# print(probabilities)

# prob(>= 7)
desOutcomes1 = list(filter(lambda i: i >= 7, outcomes))
prob1 = sum([outcomes[i] for i in desOutcomes1])
print(f"prob(>= 7): {prob1}")

# prob(odd)
desOutcomes2 = list(filter(lambda i : i % 2, outcomes))
prob2 = sum([outcomes[i] for i in desOutcomes2])
print(f"prob(odd): {prob2}")

# prob(>= 7 and odd)
over_7_and_odd = list(filter(lambda x : x % 2, over_7))
p3 = sum([probs[nr] for nr in over_7_and_odd])
print(f"prob(>= 7 and odd): {p3}")

total_prob = p1 + p2 - p3
print(f"The probability of the sum of 2 dices being over 7 or odd is {total_prob:.2f}%!")

colors = []
for val in probs.keys(): # keys are the names of the boys
    if val > 7 or val % 2 == 0:
        colors.append('g')
    else:
        colors.append('black')

fig = plt.figure()
ax = fig.add_axes([0, 0, 1, 1])
plt.bar(range(len(probs)), list(probs.values()), align='center', color=colors)
plt.xticks(range(len(probs)), list(probs))
plt.title("Outcome probability (after rolling 2 dices)")
plt.show()