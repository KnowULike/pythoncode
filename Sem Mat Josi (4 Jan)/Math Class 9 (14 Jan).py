# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha --------------------------------------------------------------------------------------------
Created on 14/01/2020 ---------------------------------------------------------------------------------------------
"""


# -------------------------------------------------------------------------------------------------------------------
# Matrizes com np.array
# Multiplicação de matrizes
# Transpose
# Propriedades
# Determinantes
# Determinantes de 3's
# Inverter matrizes
# Multiplicar matrizes pela constante
# Equações

    # Cramer's Method (x e y são iguais a divisão dos determinantes)

import numpy as np
A = np.matrix("1 2;6 -3")
print(A)
A1 = np.matrix("3 2;8 -3")
print(A1)
A2 = np.matrix("1 3;6 8")
print(A2)
det1 = np.linalg.det(A)
print(det1)
det2 = np.linalg.det(A1)
print(det2)
det3 = np.linalg.det(A2)
print(det3)
print("x = ", det2/det1)
print("y = ", det3/det1)

 # Inversa

import numpy as np
import scipy as la
from scipy import linalg
A = np.array([[1, 2], [6, -3]])
B = np.array([3, 8])
XY = np.linalg.inv(A).dot(B)
print("x.y =", XY)
XY = la.linalg.solve(A, B)
print("x.y =", XY)

import matplotlib.pyplot as plt
from matplotlib import figure as fig
l = [], li = []
fig.ax = plt.subplot(3, 1)

m = np.array([[1, 3, -5, 2], [0, 4, -2, 1], [2, -1, 3, -1], [1, 1, 1, 1]])
print(m)
b = np.array([[0, 6, 5, 10]])
l.append(b)
x = np.linalg.solve(m, b)
print(x)
li.append(x)
fig.ax[0].axis("off")
fig.ax[0].inshow(m)
fig.ax[1].axis("off")
fig.ax[1].inshow(l)
fig.ax[2].inshow(li)
fig.ax[2].axis("off")

# Exercises
# 1.Find the inverses

A = np.array([])
B = np.array([])
C = np.array([])