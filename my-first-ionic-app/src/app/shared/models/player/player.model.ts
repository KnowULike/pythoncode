export class PlayerModel {
  public name?: string;
  public description?: string;
  public image?: string;
  public followers?: number;
  public trophies?: number;
  public url?: string;
  constructor(playerModel?: PlayerModel) {
  this.name = playerModel ? playerModel.name : null;
  this.description = playerModel ? playerModel.description : null;
  this.image = playerModel ? playerModel.image : null;
  this.followers = playerModel ? playerModel.followers : 0;
  this.trophies = playerModel ? playerModel.trophies : 0;
  this.url = playerModel ? playerModel.url : null;
  }
 }
