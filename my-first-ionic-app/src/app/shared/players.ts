import { PlayerModel } from "./models/player/player.model";

export const PLAYERS: Array<PlayerModel> = [
    new PlayerModel(
    {
    name: 'Bruno Fernandes',
    description: 'Arriving to Alvalade in 2017, he is considered one of the best portuguese players in Liga NOS, becoming the most valuable player to perform for The Lions.',
    followers: 2600,
    trophies: 3,
    image: '/assets/bf.jpg',
    url: 'https://www.youtube.com/embed/Y-jKSk6xUu8'
    }),
    new PlayerModel(
    {
    name: 'Luiz Phellype',
    description: 'Arriving to Alvalade in 2018 after 2 half seasons at portuguese club Paços Ferreira, he rapidly gained confidence from the club and started to be included in the initial 11.',
    followers: 1500,
    trophies: 3,
    image: '/assets/lp.jpg',
    url: 'https://www.youtube.com/embed/BDQTt5rYDzg'
    }),
    new PlayerModel(
    {
    name: 'Luciano Vietto',
    description: 'After some failed attempts in great clubs like Atletico Madrid and Sevilha, he was brought to The Lions in 2019, where he is starting to make is redemption.',
    followers: 2000,
    trophies: 1,
    image: '/assets/lv.jpg',
    url: 'https://www.youtube.com/embed/EloaqsS4nyQ'
    })
   ];