# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

# FICHA 3)
#  Exercício 1.
#   Qual o valor de cada expressão?

#         a. map(lambda x:x+1, range(1,4))
#         b. map(lambda x:x>0, [3,−5,−2,0])
#         c. filter(lambda x:x>5, range(1,7))
#         d. filter(lambda x:x%2==0, range(1,11))

#  Exercício 2.
#   Determine o valor de cada uma das expressões seguintes:

#         a. reduce(lambda y, z: y* 3+z, range(1,5))
#         b. reduce (lambda x, y: x** 2+y, range(2,6))

from functools import reduce

map(lambda x: x + 1, range(1, 4))  # range(2, 5)
map(lambda x: x > 0, [3, -5, -2, 0])  # True, False, False, False

filter(lambda x: x > 5, range(1, 7))  # 6
filter(lambda x: x % 2 == 0, range(1, 11))  # 2 4 6 8 10

reduce(lambda y, z: y * 3 + z, range(1, 5)) # 58
reduce(lambda x, y: x ** 2 + y, range(2, 6)) # 2814

# FICHA 4 - FICHEIROS E TRATAMENTO DE EXCEPÇÕES)
#  Exercício 1.
#   Suponha que executava os seguintes programas. Qual seria o resultado?
#    1.
#     out_file = open('outFile.txt', 'w')
#     for i in range(100):
#         out_file.write(str(i))
#    2.
#     in_file = open('inFile.txt', 'r')
#     indata = in_file.read()
#     in_file.close()
#     indata = in_file.read()
#     in_file.close()
#    3.
#     in_file = open("inFile.txt", 'r')
#     print(in_file.readline())
#     in_file = open("inFile.txt", 'r'))
#     print(in_file.readline())
#     in_file.close()

# R: No programa 1, é aberto um programa em modo de escrita e é escrito os números no range de (0-99).

#   No programa 2, é aberto um programa em modo de leitura e é lido o conteúdo para um variavel antes de fechar
#   o ficheiro. No final é feito uma chamda ao ficheiro e vai dar erro pois o ficheiro não se encontra aberto.

#   No programa 3, é aberto um ficheiro em modo de leitura e é lido o conteúdo duas vezes. Pelo que dá erro.

#  Exercício 2.
#   Escreva um programa que leia um ficheiro de texto linha a linha e escreva o seu conteúdo no ecrã.

with open("Authors.txt", 'r') as file_reader:
    for line in file_reader:
        print(line)

#  Exercício 3.
#   Modifique o programa da alínea anterior de forma a aparecer também o número de cada
#   linha.

with open("Authors.txt", 'r') as file_reader:
    l = 1
    for line in file_reader:
        print(l,')', line)
        l += 1

#  Exercício 4.
#   Dados referentes a observações são frequentemente guardados em ficheiros de texto. Por exemplo, as temperaturas
#   lidas a várias horas do dia, ao longo de vários dias, podem ser guardadas num ficheiro de números em vírgula
#   flutuante, onde cada linha contém os valores das várias temperaturas medidas num dia.

#         5.6 7.8 11.7 12.6 9.3 7.3
#         6.7 8.5 11.6 11.6 9.4 7.0
#         5.4 7.2 10.5 11.1 10.0 8.3

#   Utilizando a função media, escreva uma função médias que, dado o nome de um ficheiro de texto como o acima, imprima
#   as temperaturas médias diárias. Deverá imprimir um valor por linha e tantos valores quantas as linhas do ficheiro.
#   Sugestão: utilize o método string.split(s) para obter a lista de palavras existentes numa string. A função media
#   deve apanhar a exceções referentes à utilização do ficheiro. Para isso utilize um try: finally: ou um with, para se
#   assegurar que fecha o ficheiro. Exceções sobre a abertura do ficheiro deverão ser tratadas pelo chamador
#   (ver exercício seguinte).

def calculateAvg(ficheiro):
    """

    :param ficheiro:
    :return:
    """
    from statistics import mean as avg

    try:
        f = open(ficheiro, 'r')
        lines = f.readlines()
        for line in lines:
            # values = []
            # for value in line.split():
            #     values.append(float(value))
            values = list(map(float, line.split()))
            print(avg(values))
    except IOError:
        print("Erro ao abrir ficheiro!")
    finally:
        f.close()
def createFile(ficheiro):
    """

    :param ficheiro:
    :return:
    """
    try:
        f = open(ficheiro, "w")
        txt = "5.6 7.8 11.7 12.6 9.3 7.3\n6.7 8.5 11.6 11.6 9.4 7.0\n5.4 7.2 10.5 11.1 10.0 8.3"
        f.write(txt)
    except IOError:
        print("Erro ao criar ficheiro!")
    finally:
        f.close()

createFile("Authors.txt")
calculateAvg("Authors.txt")

#  Exercício 5.
#   Escreva uma função principal sem parâmetros que pede ao utilizador o nome dum ficheiro de temperaturas, e chama a
#   função medias passando o nome do ficheiro. Caso ocorra algum problema de acesso ao ficheiro, a função principal
#   deve escrever Erro de I/O ao ler o ficheiro.

def askFilename():
    """

    :return:
    """
    file = input("Insert the name of the file: ")
    file += ".txt"
    calculateAvg(file)

#  Exercício 6.
#   Informação referente a símbolos químicos pode ser guardada em ficheiros de texto. Neste caso estamos interessados
#   apenas no nome, número atómico e densidade (em g/dm3 ). Eis um exemplo:

#         Hélio 2 0.1786
#         Néon 10 0.9002
#         Argon 18 1.7818
#         Cripton 36 3.708
#         Xenon 54 5.851
#         Radônio 86 9.97

#   Escreva uma função linha_para_elemento que dada uma linha de um ficheiro de elementos (uma string), produz um
#   dicionário com chaves 'nome', 'atomico' e 'densidade'. O nome é do tipo string, o número atómico do tipo int e a
#   densidade do tipo float.

def linha_para_elemento(string):
    nome, atomico, densidade = string.split()
    d = {"nome": nome,
         "atomico": int(atomico),
         "densidade": float(densidade)}
    return d

#  Exercício 7.
#   Utilizando a função elemento_para_string, escreva uma função escrever_elementos que, dada uma lista de dicionários
#   representando elementos químicos e o nome de um ficheiro, escreve o conteúdo da lista no ficheiro.

def escrever_elementos(d, file):
    with open(file, 'w') as file_writer:
        for key in d:
            line = ""
            line += str(d.get(key).get("nome")) + " " + str(d.get(key).get("atomico")) + " " + \
                    str(d.get(key).get("densidade")) + '\n'
            file_writer.write(line)

d = {1: {'nome': 'Hélio', 'atomico': 2, 'densidade': 0.1786},
     2: {'nome': 'Néon', 'atomico': 10, 'densidade': 0.9002},
     3: {'nome': 'Argon', 'atomico': 18, 'densidade': 1.7818}}

escrever_elementos(d, "ex7.txt")
print(d)


#  Exercício 8.

def linha_para_atomo(string):
    atom, id, s, x, y, z = string.split()

    d = {'id': int(id),
         'símbolo': s,
         'x': float(x),
         'y': float(y),
         'z': float(z)}

    return d

# FICHA 4 - MANIPULAÇÃO DE CSV)
#  Exercício 1)
#   Comma Separated Values (CSV) é o formato mais comum de importação/exportação de dados para folhas de cálculo e para
#   bases de dados. A linguagem Python conta com um módulo para o efeito, CSV File Reading and Writing. O padrão mais
#   comum de utilização do CSV para leitura é o seguinte:

# import csv

# with open('ficheiro.csv', 'rU') as ficheiro_csv:
#     leitor = csv.reader(ficheiro_csv, delimiter=';')

#  No código acima leitor é um iterador. Pode ser usado num ciclo for, como por exemplo:

    # for linha in leitor:
      # <fazer algo com linha>

#   Alternativamente, podemos chamar o método leitor.next() para obter os sucessivos elementos no iterador. A exceção
#   StopIteration é levandada quando tentamos fazer um next() e não há mais elementos no iterador. A razão para usar
#   'rU' está relacionado com a função open(), e não propriamente com CSV. O parâmetro 'U' (de Universal), aceita linhas
#   com a convenção Unix ('\n') e Windows ('\r\n'). O delimitador ';' é importante pois o formato CVS para a língua
#   portuguesa utiliza o ponto-e-vírgula como separador, uma vez que a vírgula é utilizada como separador da parte
#   decimal dos números.
#
#   Escreva uma função csvLinhaParaGrafico que leia dum ficheiro CSV o gráfico de uma função e o
#   devolva. A função recebe o nome de um ficheiro. Assuma que o ficheiro é composto por duas linhas, a primeira
#   contendo os valores das abcissas, a segunda os valores das ordenadas. Não se esqueça de converter as strings lidas
#   do ficheiro em números em vírgula flutuante.

import csv

def csvLinhaParaGráfico(file):
    grafico = []
    with open(file, 'r') as csvfile:
        csv_reader = csv.reader(csvfile)
        line1 = csv_reader.__next__()
        X = [float(value) for value in  line1[0].split(',')]
        line2 = csv_reader.__next__()
        Y = [float(value) for value in line2[0].split(',')]
        for i in range(len(X)):
            grafico.append((X[i],Y[i]))
    return grafico

#  Exercício 2)
#   O padrão típico para escrever num ficheiro CSV é o seguinte.

# import csv
#
# with open('ficheiro.csv', 'wb') as ficheiro_csv:
#     escritor = csv.writer(ficheiro_csv, delimiter=';')
#     escritor.writerow([0, 1, 2, 3, 4, 5])
#     escritor.writerow([0, 2, 4, 6, 8, 10])

# Escreva uma função graficoParaCsvLinha que receba o nome de um ficheiro e um gráfico
# (um par de listas), e escreva o gráfico no ficheiro do seguinte modo: a primeira linha contém
# as abcissas, a segunda as ordenadas

import csv

def graficoParaCsvLinha(ficheiro, grafico):
    with open(ficheiro, 'wb') as csv_file:
        objWriter = csv.writer(csv_file)
        objWriter.writerow([0, 1, 2, 3, 4, 5])
        objWriter.writerow([0, 2, 4, 6, 8, 10])


if __name__ == '__main__':
    path = "dataset.csv"
    graph = [(0,0),(2,1),(4,3),(6,5),(8,7),(10,9),(12,11)]
    graficoParaCsvLinha(path, graph)