# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
#
#   Exercício 1.
# Escreva um programa que leia uma temperatura em graus Fahrenheit e que onverta para graus celsius

f = int(input("Introduza um numero:"))

c = float((f - 32) / 1.8)

print(f"{c}:.2")
# Exercício 2.
# Escreva um programa que peça o ano de nascimento a um utilizador e calcule que idade terá no final do ano.
from datetime import datetime as dt

current_year = dt.now().year
print(current_year)
year = int(input("Introduza data de nascimento: "))
print(current_year - year)

# Exercício 3
#  Escreva um programa que leia uma hora em horas, minutos, segudos e que traduza em segundos

h = input("Escreva uma hora: ")
h_components = h.split(':')
segundos = int(h_components[0]) * 3600 + int(h_components[1]) * 60 + int(h_components[2])
print(segundos)

# Exercício 4
#  Desenvolva a versão inversa do problema anterior: lê um número representando uma duração em segundos e imprime
#  o seu equivalente em horas, minutos e segundos. Por exemplo, 9999 segundos é equivalente a 2 horas, 46 minutos
#  e 39 segundos

t = int(input("Insira o número de segundos: "))
h = t//3600
m = (t-(h*3600))//60
s = t - (h*3600+m*60)
print(f'{h}:{m}:{s}')

# Exercício 5
# if x == 1
#     x = x + 1
#     if x == 1
#         x = x +1
#     else:
#         x = x + 1
# else:
#     x = x - 1
#
# a.  Qual o valor de x no final?
# b. Qual o valor de x para dar -1 no final
# c. Qual a parte redundante do código

# Answers:
#   a) 1
#   b) 0
#   c) 2nd If

# Exercício 6
# Escreva um programa em Python que receba um numero inteiro e que o representa em notação romana.

# Solution 1

map = tuple(zip(
    (1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
    ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I')
))


v = int(input("Introduza um numero: "))
n_romano = ""   # variable that will be shown in result
for decimal, letra_romana in map:   # get decimal values and corresponding letteres starting from the end
    count = v // decimal    # count the number of times a roman leter fits in the value
    n_romano += letra_romana * count   #   add letter * count times
    v -= decimal * count    # subtract letters to value before it reenters the for loop (check lower roman letters)
print(n_romano)    # Make a str with letters

# for _ in range(x) repeats x times ignoring the var

# Exercício 7
# Escreva um programa em Python um programa que leia um ano (>0) e escreva o século a que pertence

while(True):
    year = int(input("Insert a year: "))
    temp = year // 100
    rest = year % 100
    print(temp if rest == 0 else temp + 1)

# Exercício 8
# Escreva um programa em python que receba um ano, mês e dia em seperado, e um numero de dias 'x', e que devolva
# uma nova data 'x' dias mais tarde

# Solution 1 - Using import datetime

from datetime import datetime as dt
from datetime import timedelta as inc

entry = input("Insert a date (days months year): ")
day, month, year = map(int, entry.split(' '))
i = int(input("+ dias para inserir: "))

date = dt(year, month, day)
forecast = date1 + inc(days=i)

print("Forecast > ", forecast.day, forecast.month, forecast.year)

# Solution 2

ano = int(input("ano: "))
mes = int(input("mês: "))
dia = int(input("dia: "))
dias_add = int(input("que dia será daqui a este número de dias? "))

meses_dias = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12:31}

e_ano_bisexto = (mes == 2 and ano % 4 == 0 and not (ano % 400 == 0 and ano % 100 == 0))
dias_do_mes = meses_dias[mes] + e_ano_bisexto # you can add boolean expressions to numbers, as if True it will be interpreted as one and if False it will be interpreted as 0

while dia + dias_add > dias_do_mes:
    ano += (mes + 1) > 12 # é adicionado um se o proximo mês for menor que 12 (Dezembro)
    mes = (mes + 1) % 12 if mes + 1 > 12 else mes + 1 # se o mês for maior que doze incrementamos o mês o obtemos o resto da divisão (ex: mês = 13 então mês % 12 = 1)
    dias_add -= dias_do_mes - dia # removemos os restantes dias que faltam no mês dos dias adicionados
    dia = 0 # pomos o dia como 0 para adicionar-mos os restantes dias adicionais caso saia do loop ou no próximo loop sem ter um dia a mais
    dias_do_mes = meses_dias[mes] + e_ano_bisexto # atualizamos os dias do mês

dia += dias_add # adicionamos os restantes dias do mês

print(f"{ano}-{mes}-{dia}")

# Exercício 9
#  Indique os erros sintáticos no seguinte programa em Python:
#   x = 1
#   y = 1
#   while x = 1 and y < 5
#   y = y + 2

x = 1
y = 1
while x == 1 and y < 5: # in the operation, '=' and ':' were missing!
  y = y + 2


# Exercício 10
#  Este programa em Python tem como objectivo escrever a tabuada do número inteiro dado pelo utilizador.
#  Explique porque é que este programa não termina, corrija o erro e especifique qual o valor das variáveis 'n' e 'i'
#  no final da execução do programa corrigido:
#
# n = int(input("Escreve um número inteiro: "))
# print("Tabuada do", n, ":")
# i = 1
# while i <= 10:
#   print(n, "x", i, "=", n * i)
#   i + 1

n = int(input("Escreve um número inteiro: "))
print("Tabuada do", n, ":")
i = 1
while i <= 10:
    print(n, "x", i, "=", n * i)
    i += 1  # '=' was missing, which was enabling the incrementation of 'i' and counsing an infinite loop

# i = 10
# n = user input

# Exercício 11
#  Considere este programa em Python:
#
# dividendo = int(input("Dividendo: "))
# divisor = int(input("Divisor: "))
# resto = dividendo
# quociente = 0
# while resto >= divisor:
#   resto = resto - divisor
#   quociente = quociente + 1
#   print("O quociente é", quociente, "e o resto é", resto)

# a. O que faz este programa?

# R: Faz um loop que vai retirando o divisor ao dividendo até o resto ser zero e imprime todas as iterações.

# b. Quais são as duas operações aritméticas disponíveis em Python que implementam a
#  mesma funcionalidade?

# R: Using '//' to get the quotient and '%' to get the rest or just use divmod() to geth both.

# c. Altere o programa de forma a que, caso o dividendo seja menor que o divisor, o
#  utilizador seja alertado e lhe sejam pedidos novos valores.

# Possible solution
dividendo = -1
divisor = 0
while dividendo < divisor:
    dividendo = int(input("Dividendo: "))
    divisor = int(input("Divisor: "))
    resto = dividendo
    quociente = 0
    while resto >= divisor:
        resto = resto - divisor
        quociente = quociente + 1
        print("O quociente é", quociente, "e o resto é", resto)
    if dividendo < divisor:
        print("Erro: Dividendo menor que divisor!")

# d. Faça uma alteração adicional de forma a garantir também que tanto o dividendo
#  como o divisor são positivos.

# Possible solution
while True:
    dividendo = int(input("Dividendo: "))
    divisor = int(input("Divisor: "))
    resto = dividendo
    quociente = 0
    if dividendo < divisor or dividendo < 0 or divisor < 0:
        print("Erro!")
    else:
        while resto >= divisor:
            resto = resto - divisor
            quociente = quociente + 1
            print("O quociente é", quociente, "e o resto é", resto)
        break

# Exercício 12
#  O seguinte programa em Python escreve no ecrã os factoriais de todos os números inteiros entre 1 e n, em que 'n' é
#  dado pelo utilizador:
#
# n = int(input("Escreve um número inteiro: "))
# current_n = 1
# while current_n <= n:
#     i = current_n
# f = 1
# while i > 1:
#     f = f * i
# i = i - 1
# print("Factorial de " + str(current_n) + ": " + str(f))
# current_n = current_n + 1
#
#  Altere o programa de forma a que o programa não escreva os factoriais cujo valor é superior
#  a 1000. (Note que há formas mais ou menos eficientes de o fazer.)

# Possible solution
n = int(input("Escreve um número inteiro: "))
current_n = 1
while current_n <= n and current_n <= 6: # Add a limitation because x! above 6 are bigger then 1000
    i = current_n
    f = 1
    while i > 1:
        f = f * i
        i = i - 1
    print("Factorial de " + str(current_n) + ": " + str(f))
    current_n = current_n + 1

# Exercício 13
#  Escreva um programa em Python que peça ao utilizador um número decimal e escreva no ecrã a sua parte inteira
#  perguntando em seguida se o utilizador quer terminar a utilização do programa.

start = True
while start:
    n = float(input("Insert a decimal number: "))
    print(int(n))
    print("Are you finished? y\\n")
    answer = input()
    if answer == 'y':
        start = False

# Exercício 14
#  Escreva um programa em Python que peça ao utilizador um número inteiro k e uma palavra w e escreva no ecrã k linhas
#  em que cada linha i tem o número da linha i separado por um espaço da palavra w concatenada i vezes, com i no
#  intervalo de 1 a k.

# Possible solution
k = int(input("Numero: "))
w = str(input("Palavra: "))
k = int(input("Linhas: "))
for i in range(1, k+1):
    print(f'{i})', end=' ')
    print(w*i)

# Exercício 15
#  Escreva um programa em Python que peça ao utilizador um número inteiro k e escreva no ecrã o resultado do piatório
#  (produto) das potências de 3 com expoente de 0 a k.

# Possible solution
k = int(input("Numero: "))
for i in range(k+1):
    print(i**3)

# Exercício 16
#  Escreva um programa em Python que que peça ao utilizador um número inteiro k maior do que 2 e escreva no ecrã
#  quantos números perfeitos existem entre 2 e k (inclusive). Por exemplo, existem 4 números perfeitos entre 2 e
#  10000 (o 6, o 28, o 496 e o 8128).

# Possible solution
k = 1
while k < 2:
    k = int(input("Numero: "))
for qtd in range(2, k + 1):
    divisores = list()
    for numero_inteiro in range(1, qtd):
        if qtd % numero_inteiro == 0:
            divisores.append(numero_inteiro)
    if sum(divisores) == qtd:
        print(qtd)

# Exercício 17
#  Escreva um programa em Python que peça ao utilizador um número inteiro k maior do que 10 e escreva no ecrã quantas
#  capícuas existem entre 10 e k. Uma capícua é um número que se lê de igual forma da esquerda para a direita e da
#  direita para a esquerda. Por exemplo, entre 10 e 100 existem 9 capícuas

# Possible Solution
k = 1
while k < 10:
    k = int(input("Numero: "))
for capicua in range(10, k + 1):
    numero = str(capicua)
    if numero == numero[::-1]:
        print(int(capicua))