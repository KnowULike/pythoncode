# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
#
# Exercício 1.
#  Considere a seguinte função:

def imprimeDivisaoInteira(x, y):
    if y == 0:
        print("Divisao por zero")
    else:
        print(x // y)

# a. O que faz esta função?

# R: Imprime a divisão inteira (Ex.: 9//4 = 2)

# b. Qual o resultado da seguinte sequência de comandos?
#   imprimeDivisaoInteira(4, 2) => 2
#   imprimeDivisaoInteira(2, 4) => 0
#   imprimeDivisaoInteira(3, 0) => "Divisao por zero
#   help(imprimeDivisaoInteira) => Help on function...
#   imprimeDivisaoInteira() => Erro!

# c. Altere a definição da função de modo a adoptar a abordagem da programação por contratos (recorrendo a uma
# docstring) em vez da abordagem da programação defensiva.

def imprimeDivisaoInteira(x, y):
    """
    :param x:
    :param y:
    :return:
    """
    if y == 0:
        print("Divisao por zero")
    else:
        print(x // y)

# Exercício 2.
#  Considere a seguinte função:

# def potencia(a, b):
#     return a**b

# a. O que faz a seguinte sequência de comandos?
#   a = 2
#   b = 3
#   potencia(b, a)  => 9
#   potencia(a, b)  => 8
#   print(potencia(b, a)) => print(9)
#   print(potencia(a, b)) => print(8)
#   print(2, 0) => print(1)
#   print(2) => Erro!

# b. Escreva uma nova função 'potência p' que receba apenas um número inteiro 'a' e retorne 'a^a'.

def potencia(a):
    return a**a

# c. Escreva uma nova função 'potência' que possa ser chamada com dois ou com um argumento e, consoante o caso, execute
# 'a^0' ou 'a^a'

def potencia(a, b=0):
    return a**b

# Exercício 3.
#  Considere o seguinte programa:

#  a = 4

#  def printFuncao():
#      a = 17
#      print('Dentro da função: ', a)
#
#  printFuncao()
#  print('Fora da função: ', a)

# A. Qual é o resultado de executar este programa?

# > Dentro da função:  17
# > Fora da função:  4

# B. O que é uma variável Global? E uma variável Local?

# R: Global é a nível do modulo e local é ao nível da função,etc..

# Exercício 4.
#  Considere o seguinte código:

def somaDivisores(num):
    """
    Soma de divisores de um numero dado

    Requires:
        num seja int e num > 0

    Ensures:
        um int correspondente à soma dos divisores de num que sejam maiores que 1 e menores que num
    """

# a. Como cliente da função somaDivisores, o que deve satisfazer para a função cumprir o contrato?

# R: Introduzir um numero positivo inteiro chamando a função somaDivisores()

# b. E o que obtém da função se a chamar satisfazendo a sua pré-condição?

# R: A soma de divisores de um numero dado.

# Exercício 5.
#  Crie um programa que pergunte sucessivamente ao utilizador um número inteiro positivo e imprima a soma dos
#  seus divisores. A execução do programa deve terminar quando o utilizador introduzir um número negativo.
#  Nota: assuma que a função somaDivisores, apresentada no exercício anterior, se encontra definida.

def somaDivisores(n):
    soma = 0
    for i in range(1, n):
            if n % i == 0:
                soma += i
    return soma

while (True):
    number = int(input("Insert number: "))
    if number < 0:
        break
    print(somaDivisores(number))

# Exercício 6.
#  Considere o seguinte programa:

# DIA_ATUAL = 2
# MES_ATUAL = 11
# ANO_ATUAL = 2015
# print("Dados do Pai")
# anoPai = int(input("Introduza o ano de nascimento: "))
# mesPai = int(input("Introduza o mes de nascimento: "))
# diaPai = int(input("Introduza o dia de nascimento: "))
# print("Dados da Mae")
# anoMae = int(input("Introduza o ano de nascimento: "))
# mesMae = int(input("Introduza o mes de nascimento: "))
# diaMae = int(input("Introduza o dia de nascimento: "))
# if mesPai > MES_ATUAL or \
#         (mesPai == MES_ATUAL and diaPai > DIA_ATUAL):
#     print("Pai tem", ANO_ATUAL - anoPai - 1, "ano(s)")
# else:
#     print("Pai tem", ANO_ATUAL - anoPai, "ano(s)")
# if mesMae > MES_ATUAL or \
#         (mesMae == MES_ATUAL and diaMae > DIA_ATUAL):
#     print("Mae tem", ANO_ATUAL - anoMae - 1, "ano(s)")
# else:
#     print("Mae tem", ANO_ATUAL - anoMae, "ano(s)")

# Recorrendo a funções, simplifique o programa apresentado eliminando a repetição de código.

def pedeInformacao():
    year = int(input("Introduza o ano de nascimento: "))
    month = int(input("Introduza o mes de nascimento: "))
    day = int(input("Introduza o dia de nascimento: "))
    return day, month, year

def calculaIdade(name, day, month, year):
    DIA_ATUAL = 28   # Could import datetime
    MES_ATUAL = 10
    ANO_ATUAL = 2020
    if month > MES_ATUAL or (month == MES_ATUAL and day > DIA_ATUAL):
        print(name, "tem", ANO_ATUAL - year - 1, "ano(s)")
    else:
        print(name, "tem", ANO_ATUAL - year - 1, "ano(s)")

print("Dados do Pai")
diaPai, mesPai, anoPai = pedeInformacao()
print("Dados da Mae")
diaMae, mesMae, anoMae = pedeInformacao()
calculaIdade("Pai", diaPai, mesPai, anoPai)
calculaIdade("Mãe", diaMae, mesMae, anoMae)

# Exercício 7.
#  Escreva uma função que receba dois números inteiros e devolva o maior deles. Inclua o contrato da função.
#  Teste a função escrevendo um programa que receba dois números inteiros do utilizador e imprima o resultado da
#  chamada à função desenvolvida. Como teria de fazer para determinar o menor de dois números com uma segunda função
#  que tirasse partido de chamar a primeira?

def maior(a, b):
    """
    Função que recebe dois numeros e retorna o maior dos dois números

    :args
        a (int) : um número inteiro
        b (int) : outro número inteiro

    :return
        O maior dos dois números
    """
    return max(a, b)

def menor(a, b):
    """
    Função que recebe dois números e regressa o menor entre eles.

    :args
        a (int) : um número inteiro
        b (int) : outro número inteiro

    :return
        O menor dos dois números
    """
    return b if a == maior(a, b) else a

x = int(input("x: "))
y = int(input("y: "))
if maior(x, y):
    print(maior(x, y), x, "é o maior que", y)
else:
    print(maior(x, y), x, "não é o maior que", y)

# Exercício 8.
#  Escreva uma função (o contrato não pode ser esquecido) que elimine a casa das unidades de um número inteiro.
#  Por exemplo, retira(537) devolve 53. Se o argumento só tiver algarismo das unidades, a função deve devolver 0.
#  Teste a função escrevendo um programa que receba um número inteiro do utilizador e imprima o resultado de chamada
#  à função desenvolvida.

def cut(n):
    """
    Função que recebe um número e retira a casa das unidades, caso seja inferior que dez retorna zero.

    :args
        x (int) : um número inteiro
    :return
        O número dividido por 10
    """
    return x//10

x = int(input("insert a number: "))
print(cut(x))

# Exercício 9.
#  Escreva uma função que acrescente um 0 no final de um número inteiro. Por exemplo, aumenta(73) devolve 730 (se
#  esquecer o contrato a definição da função está incompleta). aumenta(0) deve devolver 0. Teste a função escrevendo
#  um programa que receba um número inteiro do utilizador e imprima o resultado da chamada à função desenvolvida.

def aumenta(n):
    """
    Função que recebe um número e multiplica por 10.

    :args
        x (int) : um número inteiro
    :return
        O número é multiplicado por 10
    """
    return n * 10

x = int(input("insert a number: "))
print(aumenta(x))


# Exercício 10.
#  Escreva uma função que receba um número inteiro e devolva a soma dos divisores próprios desse número.

def somaDivisores(num):
    """
    Soma dos divisores próprios de um numero dado.
    Requires:
        num seja int e num > 0
    Ensures:
        um int correspondente à soma dos divisores
        de num que sejam maiores que 1 e menores que num
    """

# Teste escrevendo um programa que receba um número inteiro do user e imprima a chamada da função desenvolvida.

# Exercício 11.
#  Escreva uma função que verifique se um dado número dado é primo. Relembre que um número é primo se é maior do
#  que 1 e não tem divisores próprios. Teste a função escrevendo um programa que receba um número inteiro do
#  utilizador e imprima o resultado da chamada à função desenvolvida.

def verificaPrimo(a):
    primo = True
    for i in range(2, a):
        if a % i == 0:
            primo = False
    return primo

v = int(input("value: "))
print(verificaPrimo(v))

# Exercício 12.
#  Usando a função do exercício anterior, escreva um programa que receba um número inteiro n maior do que 2 e escreva
#  no ecrã quantos números primos existem entre 2 e n (inclusive). Por exemplo, existem 1000 números primos entre
#  2 e 7919. Teste a função escrevendo um programa que receba um número inteiro do utilizador e imprima o resultado
#  de chamada à função desenvolvida. Explique, nesta situação, em que difere a programação defensiva da programação
#  por contratos.

while(True):
    n = int(input("Insert a number: "))
    if n <= 2:
        break
    [print(i) for i in range(2, n + 1) if verificaPrimo(i)]

