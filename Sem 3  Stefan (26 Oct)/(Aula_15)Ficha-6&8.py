# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------

# FICHA 6 - MÓDULOS/FICHEIROS
# EXERCÍCIO 1)
#  Considere que tem um módulo myStuff.py com o seguinte código:

# Definido em myStuff.py

def lime():
    print('I am a lime!')

banana = 'I am a banana'

def grapes(x):
    return 'I am a grape cluster with ' + str(x) + ' grapes!'

# a) Identifique os erros do seguinte excerto de código:

import myStuff  #*?

grapes()    # é necessário fazer import do grapes e passar um x ou fazer myStuff.grapes(x)
banana = 'I was a banana'
print(banana, myStuff.banana()) # myStuff.banana() não é um método! Logo n pode ser chamado assim
myStuff.lime() # regressa uma str para lado nenhum.

# b) Qual seria o resultado no ecrã do programa completamente corrigido?

# I am a grape cluster with 'x' grapes!
# I was a banana I am a banana
# I am a lime!

# c) Qual a diferença entre as seguintes instruções:
# import myStuff
# from myStuff import *
# from myStuff import grapes

# R: A primeira e a segunda linha fazem import de tudo no modulo e o último da função grapes()

# 2) Considere o seguinte excerto de código:

in_file = open('inFile.txt', 'r')
indata = in_file.read()
out_file = open('outFile.txt', 'w')
out_file.write(indata)
out_file.close()
in_file.close()

# a) Descreva o seu funcionamento.

# R: O programa abre um ficheiro em modo de leitura, extrai e abre um novo ficheiro em modo de escrita que por sua vez
# escreve o que extraiu.

# b) Seria possível reduzir o número de instruções mantendo o mesmo
# comportamento?

# R: Sim, usando um for loop com o open().

# EXERCÍCIO 3.
#  Suponha que executava os seguintes programas. Qual seria o resultado?
#     1.
#     out_file = open("outFile.txt", 'w')
#     for i in range(100):
#         out_file.write(str(i))
#     2.
#     in_file = open("inFile.txt", 'r')
#     indata = in_file.read()
#     in_file.close()
#     indata = in_file.read()
#     in_file.close()
#     3.
#     in_file = open("inFile.txt", 'r')
#     print(in_file.readline())
#     in_file = open("inFile.txt", 'r'))
#     print(in_file.readline())
#     in_file.close()

# R: No programa 1 é aberto o ficheiro 'outfile.txt' em modo de escrita e é criado um loop que imprime no ficheiro
#   os numeros no range de 0-99. O ficheiro não chega a ser fechado.
#   No programa 2 é aberto o ficheiro 'inFile.txt' em modo de leitura lê o conteudo para uma variável "indata" e de
#   seguida é fechado o ficheiro. Após fechar o ficheiro é feita uma tentativa de ler o ficheiro, mas sem sucesso.
#   No rpograma 3 são feitas 2 aberturas dp ficheiro "inFile.txt" e no final é fechado o ficheiro. O programa corre sem
#   dar erro. Neste caso dá erro pois têm um parenteses a mais.

# EXERCÍCIO 4.
#   Identifique o resultado de cada um dos seguintes excertos de código assumindo
#   que o conteúdo do ficheiro inFile.txt é:

    # kiwi
    # pera
    # maça
    # ananás

# 1.
in_file = open("inFile.txt", 'r', encoding='utf-8')
indata = in_file.read()
print(indata)

# Output:
#  kiwi
#  pera
#  maça
#  ananás

# a função '.read()' retorna o conteúdo completo numa string

# 2.
in_file = open("inFile.txt", 'r', encoding='utf-8')
indata = in_file.readline()
print(indata)

# Output:
#  kiwi

# a função '.readLine()' retorna o conteúdo de uma linha numa string

# 3.
in_file = open("inFile.txt", 'r', encoding='utf-8')
indata = in_file.readlines()
print(indata)

# Output:
#  ['kiwi\n', 'pera\n', 'maça\n', 'ananás']

# a função '.readLines()' retorna o conteúdo completo numa lista de Strings.

# 4.
in_file = open("inFile.txt", 'r', encoding='utf-8')
indata = list(in_file)
print(indata)

# Output:
#  ['kiwi\n', 'pera\n', 'maça\n', 'ananás']

# passando o objeto para 'list()' é o equivalente ao usar a função '.readLines()' que retorna o conteúdo completo numa
# lista de Strings.

# 5.
in_file = open("inFile.txt", 'r', encoding='utf-8')
a = in_file.readline()
print(a)
for i in range(5):
    a = in_file.readline()
    print(a)

# Output:
#  kiwi
#  pera
#  maça
#  ananás

# A função readline() lê linha a linha.

# 6.
in_file = open("inFile.txt", 'r', encoding='utf-8')
for i in in_file:
    print(i)
in_file.close()

# Output:
#  kiwi
#  pera
#  maça
#  ananás

# O loop for para cada linha do ficheiro imprime a linha (i).

# EXERCÍCIO 5.
#  Admitindo que o conteúdo do ficheiro inFile.txt é:

#       10
#       30
#       40
#       21

#  Considere o seguinte programa que tem como objectivo calcular a soma dos inteiros
#  contidos no ficheiro inFile.txt:

in_file = open("inFile.txt",'r')
sum = 2
for i in range(8):
    sum += in_file.readline()   # Erro ao somar inteiros com strings, a função readLine() retorna uma string.
out_file = open("outFile.txt", 'r')
out_file.write(i)

#  a) Identifique os erros no programa.

# TODO: Completar exercício

#  b) Apresente uma versão corrigida.

# TODO: Completar exercício

# FICHA 8 - CLASSES
# EXERCÍCIO 1.
#  Crie uma classe que modele uma pessoa:

# a. Atributos: nome, idade, peso e altura

# b. Métodos: Envelhecer, engordar, emagrecer, crescer. Obs: Por padrão, a cada ano que nossa pessoa envelhece, sendo
# a idade dela menor que 21 anos, ela deve crescer 0,5 cm.

class Pessoa:
    def __init__(self, nome, idade, peso, altura):
        self.nome = nome
        self.idade = idade
        self.peso = peso
        self.altura = altura

    def envelhecer(self):
        self.idade += 1

    def engordar(self, kgs = 1):
        self.peso += kgs

    def emagrecer(self, kgs = 1):
        self.peso -= kgs

    def crescer(self, centimetros = 2 ):
        if self.idade < 21:
            self.altura += 0.5
        else:
            self.altura += centimetros

# EXERCÍCIO 2.
# Desenvolva uma classe Macaco, que possua os atributos nome e bucho (estomago) e pelo menos os métodos comer(),
# verBucho() e digerir(). Faça um programa ou teste interativamente, criando pelo menos dois macacos, alimentando-os
# com pelo menos 3 alimentos diferentes e verificando o conteúdo do estomago a cada refeição. Experimente fazer com que
# um macaco coma o outro. É possível criar um macaco canibal?

class macaco:
    def __init__(self, nome = "macaco"):
        self.nome = nome
        self.bucho = []

    def comer(self, comida):
        self.bucho.append(comida)

    def verBucho(self):
        for i in self.bucho:
            print(i)

    def digerir(self, comida):
        self.bucho.remove(comida)

    def __str__(self):
        return self.nome

if __name__ == '__main__':
    m1 = macaco("Gusmão")
    m2 = macaco("Ricardo")
    m1.comer("banana")
    m1.comer(1)
    m1.comer(m2)
    m1.verBucho()