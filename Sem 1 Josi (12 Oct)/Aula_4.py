# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
import math

#Function to calculate the volume of a sphere
#   1. receive as input the radius of the sphere (float)
#   2. define constants to use in calculations
#   3. return the sphere formula rounded to the 3rd decimal house

def volume_sphere():
    radius = float(input("Please enter radius: "))
    const = 4/3
    power = 3
    return round(const*math.pi*(radius**power), 3)

#Simple function that receives an argument and returns what we bought

def food(vegetable):    # Def nameOfFunction( <parameter> ):  is how we define functions
    if vegetable == "tomato":
        print("I bought tomato")
    elif vegetable == "orange":
        print("I bought orange")
    else:
        print("I bought other vegetable")

#Simple functions to show how functions can call other functions

def morefood():
    eggs = "more_food local"
    print(eggs) # prints "more_food local"
    food() # calls food() function
    print(eggs) # prints "more_food local"

def food():
    eggs = "food local"
    print(eggs)

#Function that receives an arg 'n' and returns the odd and even numbers backwards.
#   1. Receive 'n' as argument
#   2. Check if n is valid (assuming it's an int)
#   3. Using math.mod() to check the module (%) and see whether it's odd or even

def evenOddReversal(n):
    if n <= 0:
        print("Invalid number!")
    for x in range(n, 0, -1):
        if math.fmod(x, 2): #   it's similar to use % 2
            print(f"Even number: {x}")
        else:
            print(f"Odd number: {x}")
    return

#Function to do the mean
#   1. calculate the sum of the list passed as argument (v)
#   2. calculate length of list
#   3. mean = sum / length
#   4. return mean rounded with 3 decimal houses

def mean(v):
    return round(sum(v)/len(v), 3)

if __name__ == '__main__':
    print(volume_sphere())
    food("Tomato")
    morefood()
    evenOddReversal(10)
    list = [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]
    print(mean(list))