# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python
# Author: António Francisco C. Rocha
# ---------------------------------------------------------------------------------------------------------------------
#
#     Operators.:
# Comparisons using print

print(4 == 5)               #False
print(6 > 7)                #False
print(15 < 100)             #True
print('hello' == 'hello')   #True
print('hello' == 'Hello')   #False
print('dog' != 'cat')       #True
print(True == True)         #True
print(True != False)        #True
print(42 == 42.0)           #True
print(42 == '42')           #False
print('apple' == 'Apple')   #False
print('apple' > 'Apple')
print('"A" in unicode is', ord('A'), ', "a" in unicode is', ord('a'))  # unicode

#     Intro to Loops.:
# What is the minimum age to drive in Portugal?

minimum_age = 18
age = 21
if age > minimum_age:                               #if (condition)
    print("Congrats, you can get your license")
else:                                          #every other case, note the ":" in the end
    print("Please, come back in {0} years".format(minimum_age - age))


# Getting Acess...

name = "Antonio"
password = "Covid19"
if name == 'Antonio':
    print(f'Hello {name}')
if password == 'swordfish':
    print('Access granted.')
else:
    print('Wrong password.')


# What is the minimum age to drive in Portugal? And the maximum age ?

minimum_age = 0
user_age = int(input("Insert your age:"))
if age < minimum_age:
    print("Please, come back in {0} years" .format(minimum_age-user_age))
elif age == 150:     #elif is abreviation of "Else if"
    print("Probably, you can not drive anymore. Please, make an appointment with your doctor.")
else:
    print("Congrats, you can get your license.")

#     Exercise 1.:
# Write a code that guess the number stored in a variable
#
#     Input: guess_number
#
#     Expected Output:
#         >Please, guess higher  # if the guess_number is smaller than answer
#         >Please, guess lower   # if the guess_number is higher than answer
#         >Congrats, you found the answer!!

num = 97
guess_number = int(input("Guess wich number am I thinking? R:"))
if num > guess_number:
    print("Please, guess higher")  # code case guess_number is bigger then answer
elif num < guess_number:
    print("Please, guess lower")  # code case guess_number is lower then answer
else:
    print("Congrats, you found the answer!!")  # code case guess_number is equal/other then answer

#     Operators extended version.:
# More Comparisons using print
# Careful with the paranthesis, the IDE runs with the error.

print((4 < 5) and (5 < 6))   # = True
print((4 < 5) and (9 < 6))    # = False
print((1 == 2) or (2 == 2))   # = True
print((2 + 2 == 4 and not 2 + 2 == 5) and 2 * 2 == 2 + 2) # = True

#     Exercise 2.:
#
# a.  Write a code to ask  How old are you ? Store the age in a variable and print it
#     Expected input: <Your age>
#     Expected output <Your age>

age = int(input())
print(age)

# b.  Improve your code to check if you have age to retire or not
#     Expected input: <Your age>
#     Expected output:
#         >Have a good day at work                        (if your age is in the range of 16 – 65)
#  	   	>You are too young to work, come back to school (if <your age> smaller than 16)
# 		>You have worked enough, Let’s Travel now       (if <your age> greater than 65)

workAge = int(input())
if workAge < 16:
    print("You are too young to work, come back to school")
elif workAge > 65:
    print("You have worked enough, Let’s Travel now")
else:
    print("Have a good day at work,come back to school")

# Doctest using conditionals

a = 3
b = 2
if a == 5 and b > 0:
    print('a is 5 and', b, 'is greater than zero.')
else:
    print('a is not 5 or', b, 'is not greater than zero.')

# Be careful using conditionals

day = "Saturday"
temperature = 30
raining = True
if day == "Saturday" and temperature > 20 and not raining:
    print("Go out")
else:
    print("Better finishing python programming exercises")

# While - Loop

n = 5
while n > 0:
    print(n)
    n -= 1
print('Boom!!')
print(n)

# An Infinite Loop

n = 5
while n > 0:        #var "n" never changes!
    print('Time')
    print('ticking ')
print('Stopped')

# Another Wrong Loop

n = 0
while n > 0:        #var "n" never enters the loop!
    print('Time')
    print('ticking ')
print('Stopped')

#     Breaking out of a Loop.:
# The break statement ends the current loop and jumps to the statement immediately
# following the loop.
# It is like a loop test that can happen anywhere in the body of the loop

while True:
    line = input('> ')
    if line == 'done':
        break
    print(line)
print('Done!')

#     Finishing an iteration with continue.:
# The continue statement ends the current iteration and jumps to the top
# of the loop and starts the next iteration.

while True:
    line = input('> ')
    if line[0] == '#':
        continue
    if line == 'done':
        break
    print(line)
print('Done!')

#     Finishing an Iteration with continue.:
# The continue statement ends the current iteration and jumps to the top of the loop
# and starts the next iteration.

while True:
    line = input('> ')
    if line[0] == '#':
        continue
    if line == 'done':
        break
    print(line)
print('Done!')
