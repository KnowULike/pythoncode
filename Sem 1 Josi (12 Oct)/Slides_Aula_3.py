# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python
# Author: António Francisco C. Rocha
# ---------------------------------------------------------------------------------------------------------------------
#
#        Aula 3

def rg():
    r = range(0,20,1) # range( init_value = 0 by default, final_value_excluded, size_of_jump = 1 by default)
    for i in r:
        print(i)

#     Exercise 1.:
# Write a code to print out the capital letters in a string:

def capLetters(input):
    output = ""
    for l in input:
        if l.isupper():
            output += l
    return output

if __name__ == '__main__':
    print(capLetters("The Best of made in Portugal - Hats, Soups, Shoes, Tiles & Ceramics, Cork"))

# TPC
#   Alterar o programa de forma a ter o seguint output
#
#     Before False
#     False 9
#     False 41
#     False 12
#     True 3
#     False 74      (Change 'True' here!)
#     False 15      (Change 'True' here!)
#     After True

found = False
flag = False
print("Before", found)
for value in [9, 41, 12, 3, 74, 15]:
    found = False               #Every cicle reboots
    if value == 3:
        found = True
        flag = True             #new flag for the last print
    print(found, value)
print("After", flag)
