# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

#   Exercise 1:
# What do the following expressions evaluate to?

# (5 > 4) and (3 == 5)
# not (5 > 4)
# (5 > 4) or (3 == 5)
# not ((5 > 4) or (3 == 5))
# (True and True) and (True == False)
# (not False) or (not True)

print((5 > 4) and (3 == 5))                     # T & F = False
print(not (5 > 4))                              # !T = False
print((5 > 4) or (3 == 5))                      # T | F = True
print(not ((5 > 4) or (3 == 5)))                # !(T | F) = !T = False
print((True and True) and (True == False))      # (T & T) & (T = F) = T & F = False
print((not False) or (not True))                # !F | !T = T | F = True


# Exercise 2:
# Identify the three blocks in this code:

spam = 0                    #Block 1 (1st, 2nd and 9th line)
if spam == 10:              #Block 2 (inside the first if )
      print('eggs')
      if spam > 5:          #Block 3 (inside the inner if and else)
           print('bacon')
      else:
          print('ham')      #End of Block 3
      print('spam')         #End of Block 2
print('spam')               #End of Block 1

#   Exercise 3:
#Write a code that reads n numbers and print how many of them are in the following ranges: [0; 25],
# [26; 50], [51; 75] and [76; 100].
# For example, for n = 10 and the following numbers {2; 61; -1; 0; 88; 55; 3; 121; 25; 75} ,

# your program should print:
# [0 ,25]: 4
# [26 ,50]: 0
# [51 ,75]: 3
# [76 ,100]: 1

n = int(input("Please, what size would u like the list to have: "))      #define list size
list = []
for i in range(n):
    num = int(input(f"insert number {i+1}: "))
    list.append(num)            #add numbers to the list
range1, range2, range3, range4 = 0, 0, 0, 0
for w in list:              # loop to verify ranges
    if w in range(0, 26):
        range1 += 1             # [0 - 25]
    elif w in range(26, 51):
        range2 += 1             # [26 - 50]
    elif w in range(51, 76):
        range3 += 1             # [51 - 75]
    elif w in range(76, 100):
        range4 += 1             # [76 - 100]
print(f"[0 - 25]: {range1}")
print(f"[26 - 50]: {range2}")
print(f"[51 - 75]: {range3}")
print(f"[76 - 100]: {range4}")

#   Exercise 4:

#       Exercise 5:
#   Write a code that reads a number (0-16) in the decimal base and prints that number in the binary base.

exit = 0
while exit != 1:
    try:
        dBNum = int(input("Insert a decimal based number: "))
        if dBNum in range(0, 17):
            print(bin(dBNum)[2:])   # Transforms the Decimal into the binary
            exit = 1
            break
        else:
            print("Invalid number, please try again!")
    except ValueError:
            print("Invalid input, please try again!")