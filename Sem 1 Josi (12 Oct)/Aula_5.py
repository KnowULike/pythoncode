# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# @Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------

# Variables Name - isidentifier()

print("a2".isidentifier())  # True
print("2a".isidentifier())  # False
print("_myvar".isidentifier())  # True
print("my_var".isidentifier())  # True
print("my-var".isidentifier())  # False
print("my var".isidentifier())  # False
print("my$".isidentifier())  # False
print("my#".isidentifier())  # False

# Multi assignment

a = 5
b = 1
print("Five plus one is {a+b}")  # returns a string
print(f"Five plus one is {a + b}")  # Returns 6

a = b = c = 5  # This statement assigns 5 to a, b and c
print(a, b, c)  # prints 5,5,5

x = 1
y = 2
y, x = x, y  # changes the values in x and y
print(x)  # 2
print(y)  # 1

#   Python data types
#   We can check datatypes with 'type()'

s = "Python Course"
print(type(s))  # str

i = 2
print(type(i))  # int

f = 2.5
print(type(f))  # float

c = 2 + 3j
print(type(c))  # Complex, with 2 as the real part and 3 the imaginary one

print(bool(5))  # True
print(bool(-2))  # True
print(bool("Hamed"))  # True
print(bool(0))  # False
print(bool(''))  # False
print(bool([]))  # False   (empty list)
print(bool({}))  # False (empty dictionary)
print(bool(()))  # False   (empty tuple)
b = True
c = 5 < 2
print(type(b))  # bool
print(type(c))  # bool

l = ["apples", "grapes", "oranges"]
print(type(l))  # List

t = ("apple", "banana", "cherry")
print(type(t))  # Tuple

d = {"id": "123", "name": "farshid"}
print(type(d))  # Dict

s = {"apple", "banana", "cherry"}
print(type(s))  # Set

# Receiving input from the console
# The output of 'input()' command is String

a = int(input("Enter a:"))
b = int(input("Enter b:"))
c = a + b
print(c)

n = 12.5
print("%i" % n)  # 12
print("%f" % n)  # 12.500000
print("%e" % n)  # 1.250000e+01

# Operators
# Addition
print(1 + 3)  # 4
# Subtraction
print(5 - 3)  # 2
# Multiplication
print(2 * 3)  # 6
# Float Division
print(3 / 2)  # 1.5
# Integer Division
print(3 // 2)  # 1
# Remainder
print(17 % 5)  # 2
# Exponencial
print(2 ** 3)  # 8
print(0 ** 0)  # 1  especial case
print(6 ** 0)  # 1

# Operators Precedence

print(8 - 2 * 3)  # 2
print(1 + 3 * 4 / 2)  # 7.0
print(16 / 2 ** 3)  # 2.0
print(2 ** 2 ** 3)  # 256, 2**3 is calculated first

# Augmented Assignment Operators

X = 4
X += 2  # x = x + 2
print(x)  # 6

y = 8
y //= 2  # y = y // 2
print(y)  # 4

# Comparison Operators

print(2 == 3)  # False
print(2 != 3)  # True
print(2 < 3)  # True

# Logical Operators

print(1 < 3 or 4 > 5)  # True
print(1 < 3 and 4 > 5)  # False
print(not 1 < 3)    # Faslse

# Short-circuit Examples

print(1 >= 2 and (5 / 0) > 2)  # False, Python checks the first condition and sees it is false and ignores the rest
print(3 >= 2 and (5 / 0) > 2)  # Error

# Membership Operators

x = [1, 2, 3, 4, 5]
print(3 in x)  # True
print(24 not in x)  # True

# Bitwise Operators

a = 13
print(bin(a))  # 1101
b = 14
print(bin(b))  # 1110
c = a | b
print(bin(c))  # 1111
c = a & b
print(bin(c))  # 1100
c = a ^ b      # Xor
print(bin(c))  # 0011

a = 13  # Shift to left
print(a << b)  # 26
a = 20  ## Shifft to right
print(a >> b)  # 10
a = 18  # Shift 2 bitsnto the right
print(a >> 2)  # 4

# Operations on strings

s1 = "Python"
s2 = "Course"
s3 = s1 + s2
print(s3)  # Python Course

s = "Sara"
print(2 * s)  # SaraSara

# Conditional statements & Expression
#  Control Statements
import math
n = -16
if n < 0:
    n = abs(n)
print(math.sqrt(n))

a = 5
if True:
    a = 6
print(a) # 6

#if-else example:
a = 20
if a % 2 == 0:
    print('even')   # even
else:
    print('odd')

x = 3
y = 2
if x == 1 or y == 1:    # if 1 in(x, y)
    print('ok')
else:
    print('no') # no

names = ['sara', 'taha', 'farshid']
if 'ali' in names:
    print('found')
else:
    print('not found')  # not found

a = 2
b = 5
if a < b:
    m = a
else:
    m = b

# Conditional expression
m = a if a < b else b

my_list = ["a", "e", "i", "o", "u"]
if "o" in my_list:
    s = "yes"
else:
    s = "no"

s = "yes" if ("o" in my_list) else "no"  # Shorter version

x = 2
y = 6

z = 1 + (x if x > y else y + 2)  # z = 1 + (y+2)

print(z)  # 9

grade = 12
s = 'fail' if grade < 10 else 'pass'
print(s)    # pass

# Nested if statements

score = 75

if score <= 90:
    l = "A"
else:
    if score >= 80:
        l = "B"
    else:
        if score >= 70:
            l = "C"
        else:
            l = "D"

print(l)  # C

# If - elif - else

score = 75

if score <= 90:
    l = "A"
elif score >= 80:
    l = "B"
elif score >= 70:
    l = "C"
else:
    l = "D"

print(l)  # C

# Loops

# for, range examples

# range(start, end, step) from start till end - 1 with step = +2

for j in range(5, 10, 2):
    print(j, end=' ')  # 5 7 9

s = 'Python'
for ch in s:
    print(ch)

for _ in range(3):
    print('hello')

# range(end) start from 0 till end - 1 with step = +1

for i in range(4):
    print(i, end=' ')  # 0 1 2 3

# range(start, end) from start till end - 1 with step = +1

for i in range(3, 8):
    print(i, end=' ')  # 3 4 5 6 7

#For range example
#Count the number characters in word

word = "python"
c = 0
for i in word:
    c += 1
print(c)    #6

# Step = -3

for i in range(9, 2, -3):
    print(i, end=' ')  # 9 6 3

#Count how many 'a' are in word

word = 'alireza'
c = 0
for i in word:
    if i == 'a':
        c+=1
print(c)    # 2

#Vowels in name

name = 'farshid'
v = 'aeiou'
c = 0
for ch in name:
    if ch in v:
        print(ch)  # a i
        c += 1
print(c)  # 2

name = 'farshid'
v = 'aeiou'
a = [ch for ch in name if ch in v]
print(a)    # ['a', 'i']

#Nested For
for i in range(1, 4):
    for j in range (2, 4):
        print(j)
    print()

#Break Continue Examples

for i in range(5):
    if i == 3:
        break
    else:
        print(i)    #0 1 2

for i in range(5):
    if i == 3:
        continue
    else:
        print(i)    #0 1 2 4

#While Example

i = 1
while i <= 3:
    print(i)    # 1 2 3
    i += 1

n = 7
while n >= 3:
    print(n, end=' ')
    n -= 1

#while break example

s = "abcdef"
i = 0
while True:
    if s[i] == 'd':
        break
    print(s[i]) #a b c
    i += 1


n = 8
while n > 2:
    n -= 1
    if n == 5:
        break
    print(n)    #7 6

#while - continue

n = 8
while n > 2:
    n -= 1
    if n == 5:
        continue
    print(n)    #7 6 5 4 3 2

# PEP 8
#  It is not standart of PEP8
n = 1
while n <= 3: print(n); n += 1

#It is standart PEP8
n = 1
while n <= 3:
    print(n)
    n += 1

#GAME
#Guess the number between '0' to '9'

import random
n = random.randrange(0, 10)
f = "no"

print(n)

while f == "no":
    a = int(input("Game: guess number between 0 to 9: "))
    if a < n:
        print("Increase")
    elif a > n:
        print("decrease")
    else:
        print("Correct, you won")
        f = "yes"
print("Game finished!")
