# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python
# Author: António Francisco C. Rocha
# ---------------------------------------------------------------------------------------------------------------------
#
#
#     Exercise 1.:
#
# Try the following mathematical calculations and guess what is happening: ((3 / 2)), ((3 // 2)), ((3 % 2)), ((3**2)).
#
import math

print((3/2))    #Division passing to float (1.5)
print((3//2))   #Division with round() (1)
print((3 % 2))  #Rest (1)
print((3**2))   #Square (9)

#     Exercise 2.:
#
# Calculate the average of the following sequences of numbers: (2,4) | (4,8,9) | (12,14/6,15)

print((2+4)/2)
print((4+8+9)/3)
print(f"{((12+(14/6)+15)/3):.2f}")

#     Exercise 3.:
#
# The volume of a sphere is given by ( 4/3 * pi * radius^3 ). Calculate the volume of a sphere of radius 5.

radius = float(input("Please, Enter radius: "))
const = 4 / 3
power = 3
volume = const * math.pi * pow(radius, power)
print(f"{volume:.3f} units")

#     Exercise 4.:
#
# Use the module operator (%) to check which of the following numbers is even or odd: (1,5,20,60/7).

print(1%2)          #odd (1)
print(5%2)          #odd (1)
print(20%2)         #even (0)
print(f"{(60/7)%2:.2f}")     #odd (0,5..)