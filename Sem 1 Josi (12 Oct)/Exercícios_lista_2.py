# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python
# Author: António Francisco C. Rocha
# ---------------------------------------------------------------------------------------------------------------------

#     Exercise 1.:
# Write a code that reads two numbers and then a character that represents a
# arithmetic operator (‘+’, ‘-’, ‘*’ or ‘/’). Your program should then print the result
# operator applied to the two numbers given.

x = int(input("Insert number 1:"))
y = int(input("Insert number 2:"))
op = str(input("Insert operator:"))
if op == "+":
    print(x + y)
elif op == "-":
    print(x - y)
elif op == "/":
    print(x / y)
elif op == "*":
    print(x * y)
else:
    print("Operator unknown")

#     Exercise 2.:
# Consider the following menu:
#
#     1 - Pão Alentejano
#     2 - Bolo Lêvedo [dos Açores]
#     3 - Bolo do Caco [da Ilha da Madeira]
#     4 - Broa
#     5 – I want to leave
#
# Your program should:
# •	print the menu;
# •	read a number from 1 to 5;
# •	and print the option menu corresponding to the number read.
# This must be repeated until the user selects the option 5.

while True:
    print("1 - Pão Alentejano")
    print("2 - Bolo Lêvedo [dos Açores]")
    print("3 - Bolo do Caco [da Ilha da Madeira]")
    print("4 - Broa")
    print("5 - I want to leave")
    op = int(input(">"))
    if op == 1 or op == 2 or op == 3 or op == 4:
        print(op)
        print()
    elif op == 5:
        break
    else:
        print("Invalid input! Please, try again")

#     Exercise 3.:
# What will be printed by the program below? Assume that the value of D in the ord(<letter>) of the first letter of
# your name. Example: ord(A) = 65
#
# x = 5 + D
#  y = 0
#  while True :
#  y = (x % 2) + 10 * y
#  x = x // 2
#  print (’x =’, x, ’y =’, y)
#  if x == 0:
#  break
#
#  while y != 0:
#  x = y % 100
#  y = y // 10
#  print (’x =’, x, ’y =’, y)

x = 5 + ord("A")
y = 0
while True :
    y = (x % 2) + 10 * y
    x = x // 2
    print ("x =", x, "y =", y)
    if x == 0:
        break

while y != 0:
    x = y % 100
    y = y // 10
    print ("x =", x, "y =", y)

#     Exercise 4.:
# Write a program that reads a positive integer n and prints n lines with the following
# format (example for n = 5 => 5 spaces)
#
#     Output:
#         >1
#         >  2
#         >   3
#         >    4
#         >     5

string = ""
num = int(input("Insert a positive number:"))
x = 1
while x < num+1:
    y = 1
    while y < x:
        string += " "
        y += 1
    string += f"{x}\n"
    x += 1
print(string)