# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python
# Author: António Francisco C. Rocha
# ---------------------------------------------------------------------------------------------------------------------

#     Exercise 1.:
# Answer what type of object(variable) should be used to store each of the following
# information:
#     a.	A person's age.
#     b. 	The area of your yard in square meters.
#     c. 	The amount of money in your bank account
#     d.	The name of your favorite songs.

age = int
yard = float
cash = float
song = str

#     Exercise 2.:
# Write a Python program that accepts a word from the user and reverse it.

word = input("Insert a word:")
i = 0
while i < len(word):
    i = i + 1
    print(word[-i])

#     Exercise 3.:
# Initialize the string “abc” on a variable named “s”:
#
# a.	Use a function to get the length of the string.
# b.	Write the necessary sequence of operations to transform the string “abc” in “aaabbbccc”
#  Suggestion: Use string concatenation and string indexes.

s = "abc"
print(s[0] * len(s) + s[1] * len(s) + s[2] * len(s))

#     Exercise 5.:
# Starting from the string “aaa bbb ccc”, what sequences of operations do you need
# to arrive at the following strings? You can use the “replace” function.
#
# 1. “AAA BBB CCC”
# 2. “AAA bbb CCC

str = "aaa bbb ccc"
print(str.upper())                          # (1) AAA BBB CCC
print(str.upper().replace("BBB", "bbb"))    # (2) AAA bbb CCC

#     Exercise 6.:
# Consider the code snippet below:
#     a = 10
#     b = a
#     c = 9
#     d = c
#     c = c + 1
# After executing this code snippet, what will be the value stored in each variable?

a = 10
b = a
c = 9
d = c
c = c + 1

print(a)    #10
print(b)    #10
print(c)    #10
print(d)    #9

#     Exercise 7.:
# Write a code that reads two integer values in the variables 'x' and 'y' and change
# the content of the variables. For example, assuming that x = 2 and y = 10 were the
# values read, your program should make x = 10 and y = 2. Redo this problem using
# only 'x' and 'y' as variables.

x = int(input("insert x:"))
y = int(input("insert y:"))
aux = x
x = y
y = aux
print(f"x = {x}, y = {y}\n")

x = int(input("insert x:"))
y = int(input("insert y:"))
x, y = y, x                 #Multi-Assignment
print(f"x = {x}, y = {y}")

#     Exercise 8.:
# Consider a program that should classify a number as odd or even and, in addition,
# classify it as less than 100 or greater than or equal to 100. The solution below
# does this classification correctly?
#
#     print (" Enter a number  :")
#     a = int ( input ())
#     if a % 2 == 0 and a < 100:
#           print ("the number is even and smaller than 100")
#     else :
#           if a >= 100:
#                     print ("The number is even and equal or higher than 100")
#     if a % 2 != 0 and a < 100:
#             print ("The number is odd and smaller than 100")
#     else :
#     if a >= 100:
#                print ("The number is odd and equal or higher than 100")

print(" Enter a number  :")
a = int(input())    #Better identation
if a % 2 == 0 and a < 100:
    print("the number is even and smaller than 100")
elif a % 2 == 0 and a >= 100:   #The loop in the second conditional allowed for odd numbers >= 100 to print like even.
    print("The number is even and equal or higher than 100")
elif a % 2 != 0 and a < 100:    #The loop in the forth conditional allowed for even numbers >= 100 to print like odd.
    print("The number is odd and smaller than 100")
else:
    print("The number is odd and equal or higher than 100")

