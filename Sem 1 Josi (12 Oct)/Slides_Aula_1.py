import sys

# Ex1
# 1.A
print('Welcome to python course')
print("Welcome to python course")
# 1.B
print(8 + 5 * 13)
# 1.C
print("Python is fun", 31)
# 1.D
print("Hello" + "António")
print("Hello"''"António")
print("Hello" + "\tAntónio\n You splitting this line several\ntimes\n")
print("Hello, António")
greeting = "Hello"
name = "António"
print(greeting + name)
print(greeting + " " + name)
print(type(name))
age = 1
print(name, age)
print("oi" * 5)  # String Computation
# 1.E
print("%s is %d years old" % (name, age))
print("{} is {} years old".format(name, age))
print(f"{name} is {age} years old")  # Remember f from format function
# Ex2
# Ex3
print("Please, Enter your name:")
student_name = input()
print(student_name)
# Ex4
print("Enter three strings:")
strings = input()
a, b, c = strings.split(" ")
print(f"{a}\n{b}\n{c}")
# Best Way
str1, str2, str3 = input("Enter three strings:").split(" ")
print(str1, str2, str3)
# Ex5
totalMoney = 1000
quantity = 3
price = 450
print(f"I have {totalMoney} dollars so I can buy {quantity} football for {price:.2f} dollars.")
# Ex5 Another Way
quantity = 3
totalMoney = 1000
price = 450
statement1 = "I have {1} dollars so I can buy {0} football for {2:.2f} dollars."
print(statement1.format(quantity, totalMoney, price))
# Ex6
# msg =  012345678901234
# letters = "abcdefghijklmnopqrstuvwxyz"
msg = "Lisbon is in Portugal"  # 23 letters
print(msg[1])  # second letter, index starts at 0
print(msg[0:6])  # Lisbon
print(msg[3:5])  # 4th and 5th letter
print(msg[0:9])  # from the beginning to 9th position
print(msg[:9])  # same as the previus line
print(msg[10:14])  # from 10th to 14th letter
print(msg[10:])  # From 10th position till the end
print(msg[:6])  # Lisbon (first half)
print(msg[6:])  # is in Portugal (second half)
print(msg[:6] + msg[6:])  # Join both halfs of the string
print(msg[:])  # all string
# Ex7
print(msg[-21:-15])  # Lisbon (the 'minus' operator reverses the starting point)
print(msg[-8:-5])  # PORTUGAL without TUGAL (Por)
print(msg[-14:-12])  # is
print(msg[-8:])  # FROM 13TH position until the end (Portugal)
print(msg[-11:-8])  # in
print(msg[-23:])  # all the string because it has 21 letters
print(msg[-5:-1])  # The last 5 letters without the final one (tuga)
print(msg[7:])  # from the 7th to the end
print(msg[:])  # All the string
# Ex8
a = 12
b = 3
print(a + b)  # 15
print(a - b)  # 9
print(a * b)  # 36
print(a / b)  # 4.0
print(a // b)  # 4 integer division, rounded down towards minus infinity
print(a % b)  # 0 modulo: the remainder after integer division
# Ex9
num1 = int(input("Enter number 1:"))
num2 = int(input("Enter number 2:"))
print(f"{num1} + {num2} =", num1 + num2)
# Ex10
extended_num = input("Enter a number with this format: : xxxx----xxxx----xxxx---")
print(extended_num[-7:-3])
# Other way
credit_str = "xxxx----xxxx----5555---"
new_credit_str = credit_str[credit_str.index('5'):credit_str.index('5') + len('5555')]
print(new_credit_str)
# Useful Information
print("\nInteger value information: ", sys.int_info)
print("\nMaximum size of an integer: ", sys.maxsize)