# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------

# Arimethic Mean
# Geomtric Mean
# Another one Mean

# Mean Median Mode

# First Sort the values then find the median!


# Mode - Most frequent value in dataset

# Example

import statistics
from scipy import stats
import numpy as np

a = [4, 36, 45, 50, 75]
b = [1, 2, 2, 3, 4, 7, 9]
c = [6, 3, 9, 6, 5, 9, 9, 3, 1]

print(statistics.mean(a))   # 42
print(np.mean(b))   # 4.0
print(np.mean(c))   # 5.666666666666667

print(statistics.mode(a))   # 4
print(stats.mode(b))    # ModeResult(mode=array([2]), count=array([2]))
print(stats.mode(c))    # ModeResult(mode=array([9]), count=array([3]))

print(statistics.median(a)) # 45
print(statistics.median(b)) # 3
print(np.median(c)) # 6.0

c = [6, 3, 9, 6, 6, 5, 9, 9, 3, 1]
sorted(c)   # [1, 3, 3, 5, 6, 6, 6, 9, 9, 9]

# Variance & Standart Deviation

# Sampling
"""
Why take a sample?

Mostly because it is easier and cheaper.

Imagine youu want to know what the whole country thinks ...
you can´t ask millions of people, so instead you ask maybe
1,000 people.

To find out information about the population (Such as mean and standart deviation)

"""


"""
Population standart deviation example

- Calculate the average
- Calculate "Somatório"
"""

"""
Sample standart deviation example
"""

import statistics
import numpy as np

a = [9, 2, 5, 4, 12, 7, 8, 11, 9, 3, 7, 4, 12, 5, 4, 10, 9, 6, 9, 4]
b = [9, 2, 5, 4, 12, 7]

# TODO: diff between pop and sample

"""
Quartile
"""

import matplotlib.pyplot as plt

x = [0, 0.5, 2]
y = [0, 1, 4]
plt.plot(x, y, 'go--')  # equivalent to plt.plot(x, y, color='green', marker='o', linestyle='dashed')
# [<matplotlib.lines.Line2D at 0x21c83668a60>]
plt.show()  # visualize graph

x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]

plt.plot(x, y, 'r--p', label='y=2x')
plt.legend
plt.title('test')
plt.xlabel('X')
plt.ylabel('Y')
# Text(0, 0.5, 'Y') # put semicolon for formatiing
plt.show()

x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]
plt.plot(x1, y1, 'b:D', label='y=x^2');
plt.legend;
plt.show()

plt.plot(x, y, 'r--p', label='y=2x')
plt.plot(x1, y1, 'b:D', label='y=x^2');
plt.show()

import matplotlib.pyplot as plt

x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]

x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]

plt.subplot(211)
plt.plot(x, y, 'r--p', label='y=2x')
plt.legend
plt.subplot(212)
plt.plot(x1, y1, 'b:D', label='y=x^2');
plt.legend
plt.show()
plt.savefig(path)

import numpy as np
import matplotlib.pyplot as plt

x = np.arange(14)
x = np.sin(x/2)
plt.setp(x, y + 2, label='pre')
# TODO: plot
plt.setp(x, y + 1, label='mid')
# TODO: plot
plt.setp(x, y, label='post')
# TODO: plot
plt.grid(axis='x', color='0.95')
plt.legend(title='Parameter where:')
plt.show()

# plot, subplot, suptitle, set_xlabel, set_ylabel

import numpy as np
import  matplotlib.pyplot as plt

x1 = np.linspace(0.0, 5.0) # generate numbers
x2 = np.linspace(0.0, 2.0)


y1 = np.cos(2 * np.pi * x1) * np.exp(x1)
y2 = np.cos(2 * np.pi * x2)

# subplots not subplot
fig, (ax1, ax2) = plt.subplots(2, 1) # Unpack , OOP way
# fig.subtitle("A Tale of 2 subplots")

ax1.plot(x1, y1, 'o-')
ax1.set_ylabel('Damped Oscillation')

ax2.plot(x2, y2, '.-')
ax2.set_xlabel('Time(s)')
ax2.set_ylabel('Undamped')

plt.show()



# State-Based vs OOP

# PLOT, SUBPLOT, SET(TITLE, XLABEL, YLABEL)
# GRID
# LEGEND
# SAVEFIG

import numpy as np
import  matplotlib.pyplot as plt

x = np.arange(0-0, 2.0, 0.01)
y = 1 + np.sin(2 * np.pi * x)

fig, ax = plt.subplot()
ax.plot(x, y, label='sin(\phi$)')

# TODO: fINISH
ax.set()
ax.grid()
ax.legend()
ax.savefig(path)
plt.show()

# Boxplot
"""
Boxplot are a standardized way of displaying the distribution of data based on a five number summary

Minimum > = (Q1 - 1.5 * IQR)
First quartile (Q1)
Median (Q2)
Third Quartile (Q3)
Maximum < = (Q3 + 1.5 * IQR)

1. Tell you the values of your outliers
2. Identify if data is symmetrical
3. Determine how tightly data is grouped
4. See your data is skewed
"""

"""
Boxplot on a Normal Distribution

By removing outliers, we have access to 99.3% of data on a normal distribution

"""

import numpy as np
import matplotlib.pyplot as plt

data = np.array([-10, -5, -2, -1, 0, 1, 2, 3, 4])

q1 = np.quantile(data, .25)
print(q1)

q2 = np.quantile(data, .05)
print(q2)

q3 = np.quantile(data, .75)
print(q3)

iqr = q3 - q1
print(iqr)

lv = q1 - 1.5 * iqr
print(lv)

hv = q3 + 1.5 * iqr
print(hv)

plt.boxplot(data);

plt.show()

plt.boxplot(data);
plt.grid()

plt.show()

# another boxplot example

import numpy as np
import matplotlib.pyplot as plt

class1 = np.array([60, 70, 80, 83, 85, 87, 88, 89, 90, 92, 94, 95, 97, 100, 110])
class2 = np.array([130, 143, 150,158, 160, 170, 175, 182, 185, 188, 190, 200, 210, 280, 300])

plt.boxplot([class1,class2], patch_artist=True)
plt.show()

# Bar

import numpy as np
import matplotlib.pyplot as plt

names = ['A', 'B', 'C', 'D', 'E']
values = [5, 10, 8, 3, 2]

plt.bar(names, values, color='green');
plt.xlabel('names');
plt.ylabel('Nº of students');
plt.show()

# TODO: Scatter plot

# Histogram

import numpy as np
import matplotlib.pyplot as plt

data = np.array([3, 3, 5, 6, 7, 7, 8, 9, 9, 10, 10, 10, 11, 12 ,12, 14, 15, 16, 17 ,18, 19, 19, 20])

plt.hist(data, bins=4, edgecolor='black');  # bins not bin
plt.xlabel("Value");
plt.ylabel("Frequency");
plt.show()


import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

x = 100 + 15 * np.random.random(1000)


# TODO: histogram
n, bins, _ = plt(...)

# Pie

import matplotlib.pyplot as plt

l = ["Python", "Java", "C++"]
s = [200, 150, 100]
c = ["green", "red", "yellow"]

plt.pie(s, labels=l, colors=c)
plt.show()

plt.pie(s, labels=l, colors=c, autopct='%1.2f%%')
plt.show()

plt.pie(s, labels=l, colors=c, explode=(0.1, 0, 0), shadow=True, autopct='%1.2f%%')
plt.show()

# Pandas/Box

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame(np.random.rand(10, 5), columns=['A', 'B', 'C', 'D', 'E'])
print(df)

df.plot.box();
plt.show()

df.plot.box(color={"boxes": "green", "whiskers": "red", "medians": "blue", "caps": "grey", })
plt.show()

df.plot.box(vert=False, positions=[1, 4, 5, 6, 8])
plt.show()

# Pandas Bar & Barh

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame(np.random.rand(10, 4), columns=['A', 'B', 'C', 'D'])
print(df)

df.plot.bar()
plt.show()

df.plot.bar(stacked=True)
plt.show()

df.plot.barh(stacked=True)
plt.show()

# Pandas Scatter

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame(np.random.rand(10, 4), columns=['A', 'B', 'C', 'D'])
print(df.head())

df.plot.scatter(x='A', y='B', color='red', label='group 1');
plt.show()

df.plot.scatter(x='C', y='D', color='blue', label='group 2');
plt.show()

k = df.plot.scatter(x='A', y='B', color='red', label='group 1')
df.plot.scatter(x='C', y='D', color='blue', label='group 2', ax=k)
plt.show()

# Pands Plot( kind= ... )


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame(np.random.rand(10, 3), columns=['A', 'B', 'C'])
df['C'] *= 200
df.sort_values(by='A', inplace=True)  # inplace to update dataframe
df.head(7)

df.plot(kind='line', x='A', y='B', color='green')
plt.show()

# Pandas Area

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame(np.random.rand(10, 3), columns=['A', 'B', 'C'])
df['C'] *= 200
df.sort_values(by='A', inplace=True)  # inplace to update dataframe
df.head(7)

df.plot(kind='area')
plt.show()

# Pandas Scatter Style

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# TODO: cheat codes and finish

plt.style.use('ggplot')


# Pandas Series Pie

import pandas as pd
import numpy as np

s = pd.Series(3 * np.random.rand(3), index=['a', 'b', 'c'], name= 'series')
print(s)

s.plot.pie()
plt.show()

# TODO: Finish

# Pandas FataFrames Pie

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame(3 * np.random.rand(10, 4), index=['a', 'b', 'c', 'd'], columns=['x', 'y'])
print(df)
#TODO
# df.plot.pie(subplot=, ...)

plt.show()

# Mashgrid
"""
The numpy,meshgrid function is used to create a rectangular grid out of two given one-dimensional arrays representing
 the Cartesian indexing or Matrix indexing. 
"""

import numpy as np

x = np.array([1, 2, 3, 4])
y = np.array([7, 8])

a, b = np.meshgrid(x, y)
print(a)
# [[1 2 3 4]
#  [1 2 3 4]]

print(b)
# [[7 7 7 7]
#  [8 8 8 8]]

print(a.shape)  # (2, 4)

# Countour

# What is the Contour? (TOPOGRAPHIC MAPS)

# 3D Plot

import numpy as np
import matplotlib.pyplot as plt
from apl.toolkit.matplot3d import Axes3D

x = np.arange(-10, 10, 0.5)
y = np.arange(-10, 10, 0.5)
x, y = np.meshgrid(x, y)
z = x ** 2 + y **2

fig = plt.figure(figsize=(5, 5))    # Create an empty 3D figure
ax = fig.gca(projection='3d')

s = ax.plot_surface(x, y, z, cmap=plt.cm.rainbow)   # Plot 3D surface

cset = ax.contour(x, y, z, zdir= 'z', offset=0, cmap=plt.cm.rainbow)    # Contour

fig.

# TODO: Finish

# Contour & Contourf


import numpy as np
import matplotlib.pyplot as plt

x = np.arange(1, 11)
y = x.reshape(-1, 1)
h = x*y

# TODO: Finish

# 3d ratating iris_PCA

import pandas as pd
import matplotlib.pyplot as plt
from sklearn import datasets

fig, ax = plt.subplots()
iris = datasets.load_iris()
df = pd.DataFrame(iris.data, columns=[])

# Rotate graph