# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------
import math


def squareArea(side_lenght):
    return round((side_lenght ** 2), 3)


def squarePerimeter(side_lenght):
    return round((side_lenght * 4), 3)


def circleArea(radius):
    return round((math.pi * (radius**2)), 3)


def circlePerimeter(radius):
    return round(((2 * math.pi) * radius), 3)


def sphereVolume(radius):
    return round(((4 / 3) * math.pi * (radius ** 3)), 3)


def figuresDetails(x):
    print("circle area:     ", circleArea(x))
    print("circle perimeter:", circlePerimeter(x))
    print("square area:     ", squareArea(x))
    print("square perimeter:", squarePerimeter(x))
