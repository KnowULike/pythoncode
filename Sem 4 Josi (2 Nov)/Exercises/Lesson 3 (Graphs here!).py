# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------


# Exercise slide 12
import numpy as np
dtype = [('name', 'U10'), ('grande', float), ('age', int)]  # Changed 'S10' to 'U10' to remove b'parameter' in print
values = [('Joseanne', 5, 31), ('Hamed', 5, 32), ('Stefan', 5, 24)]
sorted_data = np.array(values, dtype=dtype)
print(np.sort(sorted_data, order='age'))    # [('Stefan', 5., 24) ('Joseanne', 5., 31) ('Hamed', 5., 32)]

# Exercise 1.
#  In the Sudoku game we have a 9 x 9 matrix divided into 3 x 3 filled squares previously with some numbers
#  between 1 and 9 (see the example).
#  A solution for an instance of the game is to fill all empty positions with numbers between 1 and 9 respecting the
#  following rules:
#
#    (a) There cannot be repeated numbers in the same square, that is, each number between 1 and 9 must appear
#    exactly once in each square.
#    (b) There can be no repeated numbers on any line of the matrix.
#    (c) There can be no repeated numbers in any column of the matrix.
#
#  Write a function that takes a 9 x 9 matrix as a parameter, that represents a proposed solution for a Sudoku, and
#  test if the matrix is a valid solution for the game, returning True if true and False otherwise.
#  The following function must be implemented: def isSolution(mat):

import numpy as np

def verifyLine(matrix_line):
    """
    Goes through a list and checks for duplicates.

    :parameter a list
    :return: True in case there is a duplicate

    example.:

    [1,2,3,4,5,2,7,1,9]
        ...sorts...
    [1,1,2,2,3,4,5,7,9]
     ...Transforms...
    [F,True,F,T,F,...,False]

    if any value is True it's a duplicate
    """
    mtx = sorted(list(matrix_line))
    mtx = list(map(lambda x: np.logical_and(x > 0, mtx[x] == mtx[x - 1]), range(len(mtx))))
    if True in mtx:
        return True
    return False

def each_matrix(size, iterable):
    """ Chunks the iterable into (Size x Size) elements at a time, each yielded as a list.

    Example:
      for subMatrix in each_matrix(3, bigger_matrix 9x9 ):
          print(subMatrix)

      # output:
      all submatrix's with 3x3
    """
    r1 = [0, 1, 2]
    r2 = [0, 1, 2]
    current_slice = []
    i, j = 0, 0

    while (i <= 8 and j < 8):
        for i in r1:
            for j in r2:
                current_slice.append(iterable[i, j])
                if len(current_slice)/3 >= size:
                    yield current_slice
                    if i == r1[2]:
                        r2 = [x + 3 for x in r2]

                    if i == 8 and j == 8:
                        i = 7
                    elif i == r1[2] and j == 8:
                        r1 = [x + 3 for x in r1]
                        r2 = [0, 1, 2]
                        j = 0
                    current_slice = []

def isSolution(mat):
    """
    APPROACH:

    Check if each row of the board matrix stores only unique values from the range [1, 9] or not.
    Check if each column of the board matrix stores unique values from the range [1, 9] or not.
    Check if all possible 3 × 3 submatrices of the board matrix stores unique values from the range [1, 9] or not.

    STEPS:

    Traverse the given matrix.
    Check if the above conditions are satisfied or not.
    If any of the above conditions is not satisfied, then return 'False'.
    Otherwise, return 'True'.
    """
    try:
        # Check for numbers bigger or below range 1-9
        if np.where(np.asarray(list(map(lambda x: np.logical_or(x < 1, x > 9), mat))) == True):
           return False
    except:
        print("Invalid input on the board!")

        # Check for duplicates horizontally
        for line in mat:
            if verifyLine(line):
                return False

        # Check for duplicates in 3x3 submatrix's
        for subMatrix in each_matrix(3, mat):   # Generator to get submatrix's in a list horizontally
            if verifyLine(subMatrix):
                return False

        # Transpose to verify lines horizontally
        mat2 = np.copy(mat).transpose()

        # check for duplicates vertically
        for line in mat2:
            if verifyLine(line):
                return False

        return True

# Exercise 2.
#  For the code below:

# j = 1
#
# def main():
#     a = 9
#
#     if a % 2 == 0:
#         a = 2
#     else:
#         a = 3
#
#     print(fun1(2, 4))
#
#     for i in range(3):
#         for j in range(3):
#             print(fun1(a, i + j))
#
#
# def fun1(a, b):
#     p = 1
#     for i in range(b):
#         p = p * a
#     return p + j
#
# main()

#   (a) Determine the local and global variables for this program. For each variable identify which function it
#   belongs to.

# R.:
#    - J is a global variable.
#    - There are 2x 'a' both in main() and fun1() but are local
#    - 'i' belongs to the for's where it was created, are local
#    - 'p' is local and belongs to fun1()

#   (b) Show what will be printed on the computer screen when this program is run.

# OUTPUT:
# 17
# 2
# 4
# 10
# 4
# 10
# 28
# 10
# 28
# 82

# Exercise 3.
#  Write a program that reads a sequence of whole numbers and saves them in a list. Then, the program must read another
#  integer A and the program must find two different position numbers in the list whose multiplication is C and print
#  them out. If there are no such numbers, the program must print a corresponding message.
#
#       List = [1 2 3 7 11 13]
#
#       Input: A = 77
#       Output: 7 and 11
#       Input: A = 50
#       Output:  values not found


import numpy as np

try:
    ip = input("Please, insert a sequence of whole numbers: ") # Input
    pdt = int(input("A = "))   # Product to look for
    seq = list(map(int, ip.split(" ")))    # Sequence normalized to 'int'
    mtx = np.where(np.outer(seq, seq) == pdt)   # matrix with all possible products
    print("Values not found!") if mtx[0].size == 0 else print(mtx[0][0], "and", mtx[0][1])
except:
    print("Invalid input!")

# Challenge 1.
#  Como desafio, gostaria que vocês apagasse do repositório atual de vocês a pasta "Area" depois usasse o git revert ou
#  git reset para recuperá-la (gostaria de uma explicação da diferença dos dois comandos - coloquem a explicação em
#  um arquivo e adicionem na pasta de exercícios da aula no repositório 5 linhas no máximo).

# -> Search for Challenge(2).txt in repo.

# Challenge 2.
#  Além disso, criar um novo branch no bitbucket para adicionar o módulo de vocês e verificar possibilidade de mover
#  o módulo para o branch novo (sem deletar, apenas mover as pastas).

# -> Search for dataset.csv in master branch

# Challenge 3.
#  Também, está no anexo um arquivo csv com os dados do PISA de matemática de Portugal (avaliação global da educação)
#  Gostaria de ver um gráfico de médias obtidas por ano de portugal (PRT),  um outro de 5 outros os países(livre
#  escolha) comparados com Portugal e um terceiro gráfico de barras com os dados em relação ao desempenho de meninos e
#  as meninas usando o matplotlib   https://data.oecd.org/pisa/mathematics-performance-pisa.htm#indicator-chart


# Challenge 3.1
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
path = "C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\DP_LIVE_04112020161547500.csv"

data = pd.read_csv(path)
data["Value"] = np.float64(data["Value"])

datasetPT = data.loc[data.LOCATION == 'PRT', ["LOCATION", "TIME", "Value"]]

plt.plot(datasetPT.groupby(["TIME"]).mean(), label="Portugal")

plt.legend()
plt.title("Challenge 3.1")
plt.xlabel("Years")
plt.ylabel("Mean of values")
plt.show()

# Challenge 3.2
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
path = "C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\DP_LIVE_04112020161547500.csv"

data = pd.read_csv(path)
data["Value"] = np.float64(data["Value"])

datasetEU = data.loc[data.LOCATION.isin(['PRT', 'ESP', 'FRA', 'ITA', 'GBR', 'DEU']), ["LOCATION", "TIME", "Value"]]

# print(datasetEU)
# print(datasetEU.groupby(["LOCATION", "TIME"]).mean())
datasetEU.groupby(["LOCATION", "TIME"]).mean()

d1 = datasetEU.loc[datasetEU.LOCATION =='PRT']
d2 = datasetEU.loc[datasetEU.LOCATION =='ESP']
d3 = datasetEU.loc[datasetEU.LOCATION =='FRA']
d4 = datasetEU.loc[datasetEU.LOCATION =='ITA']
d5 = datasetEU.loc[datasetEU.LOCATION =='GBR']
d6 = datasetEU.loc[datasetEU.LOCATION =='DEU']

plt.plot(d1.groupby(["TIME"]).mean(), label="PRT")
plt.plot(d2.groupby(["TIME"]).mean(), label="ESP")
plt.plot(d3.groupby(["TIME"]).mean(), label="FRA")
plt.plot(d4.groupby(["TIME"]).mean(), label="ITA")
plt.plot(d5.groupby(["TIME"]).mean(), label="GBR")
plt.plot(d6.groupby(["TIME"]).mean(), label="DEU")

plt.legend()
plt.title("Challenge 3.2")
plt.xlabel("Years")
plt.ylabel("Mean of values")
plt.show()

# Challenge 3.3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
path = "C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\DP_LIVE_04112020161547500.csv"

data = pd.read_csv(path)
data["Value"] = np.float64(data["Value"])

datasetPT = data.loc[data.LOCATION == 'PRT', ["LOCATION", "SUBJECT", "Value"]]
# print(datasetPT)
# print(datasetPT.groupby(["SUBJECT"]).mean())

sorted_data = datasetPT.groupby(["SUBJECT"]).mean()

# l2 = [sorted_data.SUBJECT["BOY"], sorted_data.SUBJECT["GIRL"]]
# print(l2)

plt.bar(["Boy", "Girl"], sorted_data.Value, width=0.8, align="center", color="orange")

plt.title("Challenge 3.3")
plt.xlabel("Gender")
plt.ylabel("Mean of values")
plt.show()
