# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------

# Exercise 1

#  - In the module that we created,  provide operations with circles, namely, calculation of the perimeter and area
#  given the radius. Add the sphere volume function code in your module.

#  - Provide operations with squares, namely, calculation of the perimeter and area given the length of the side.

#  Provide the possibility to ask the user for a value and writes on the screen the perimeter and the area of the circle
#  and square based in the radius and side measurement.

import sys
sys.path.append("C:\\Users\\franc\PycharmProjects\pythoncode\Semana 4 (Nov 2)\Area")
import GeometryCalculations as geo

if __name__ == '__main__':
    print(geo.squareArea(3))    # 9
    print(geo.squarePerimeter(3))   # 12
    print(geo.circleArea(3))    # 28.274
    print(geo.circlePerimeter(3))   # 18.85

    geo.figuresDetails(3)
    # circle area:      28.274
    # circle perimeter: 18.85
    # square area:      9
    # square perimeter: 12

    try:
        x = float(input("Please insert a number to get geometric figures with radius/lenght x:"))
        geo.figuresDetails(x)
    except:
        print("Invalid number.")