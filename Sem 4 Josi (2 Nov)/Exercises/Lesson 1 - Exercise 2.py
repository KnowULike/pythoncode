# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------

# Exercise 2
#  Write a function that takes as a parameter a list of integers. The function must return a tuple with two integer
#  values f1 and f2, where f1 is the list element with lowest frequency (lowest number of occurrences in the list) and
#  f2 is the element with the highest frequency. Tip: use a dictionary to compute the frequencies of the list elements.
#  The following function must be implemented:
#       def frequencies (v)

# Input =  [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9]

# Output = ([f1], [f2])     f1 = [5,7]     f2=[1]

def frequencies (v):
    d = {}
    for i in v:
        d[i] = d.get(1, 0) + 1
    f1 = max(d.keys(), key=(lambda k: d[k]))
    f2 = min(d.keys(), key=(lambda k: d[k]))
    output = (f1, f2)
    return output

if __name__ == '__main__':

    values = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9]
    print(frequencies(values))
