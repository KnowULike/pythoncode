# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------
import sys
sys.path.append("C:\\Users\\franc\PycharmProjects\pythoncode\Semana 4 (Nov 2)\Area")
import GeometryCalculations as geo

geo.squareArea(2)   # 4

import numpy as np

print(np.__version__)   # version 1.19.2
print(np.version)   # <module 'numpy.version' from 'C:\\Users\\franc\\AppData\\Local\\Packages
# \\PythonSoftwareFoundation.Python.3.8_qbz5n2kfra8p0\\LocalCache\\local-packages\\Python38\\site-packages\\numpy\\version.py'>

print(dir(np))  # Displays all methods available to manipulate numpy
print(dir(np.array))    # Displays all methods available to display numpy.array

students = np.array([[[1, 3, 5], [1, 1, 1]],[[4.5, 4, 5], [4.3, 4.4, 4.6]]])

print(students)
# [[[1.  3.  5. ]
#   [1.  1.  1. ]]
#
#  [[4.5 4.  5. ]
#   [4.3 4.4 4.6]]]

print(students.ndim, students.dtype)    # 3 float64

print(students.shape)   # (2, 2, 3), cuidado que é diferente dependendo da estrutura
print(students.nbytes)  # 96
print(students.ndim)    # 3
print(students.dtype)   # float64
print(students.size)    # 12
print(students.data)    # memory location address
print(students.itemsize)    # 8

x1 = np.array([[-1, 3]])
x2 = np.array([[1, 2]])
x3 = x1+x2
x4 = x1*x2
x5 = x1-x2
x6 = x1/x2
print(x1)   # [[-1  3]]
print(x2)   # [[1 2]]
print(x3)   # [[0 5]]
print(x4)   # [[-1  6]]
print(x5)   # [[-2  1]]
print(x6)   # [[-1.   1.5]]

# Manipulating Data types - https://numpy.org/devdocs/user/basics.types.html

y = np.power(10, 4, dtype=np.int8)
print(y)    # 16 overflow int8 size
y = np.power(10, 4, dtype=np.int32)
print(y)    # 10 000 correct value

# Array broadcasting - https://numpy.org/doc/stable/user/basics.broadcasting.html#:~:
# text=The%20term%20broadcasting%20describes%20how,that%20they%20have%20compatible%20shapes.

# Reshape Vs Resize
print(np.arange(10))    # Creates a vector -> [0 1 2 3 4 5 6 7 8 9]
print(np.arange(10).reshape(2, 5))  # Reshapes vetor to 2 vectors with lenght 5
                                        # [[0 1 2 3 4]
                                        #  [5 6 7 8 9]]

print(np.resize(np.arange(10), (2, 5))) # Same as reshape
                                            # [[0 1 2 3 4]
                                            #  [5 6 7 8 9]]

print(np.resize(np.arange(10), (2, 7))) # This time it completes the vectors
                                            # [[0 1 2 3 4 5 6]
                                            #  [7 8 9 0 1 2 3]]

# Newaxis & Copy

# Newaxis: adds a column in the array
# Copy: copy the array to other array

d = np.arange(2, 5)
print(np.arange(2, 5))    # vetor [2 3 4]
print(d.shape)  # (3,)

print(d[:, np.newaxis])
# [[2]
#  [3]
#  [4]]

print(d[:, np.newaxis].shape)   # (3, 1)

x = np.array([1, 4, 3])
y = x
z = np.copy(x)
x[1] = 2
print(x)    # [1 2 3]
print(y)    # [1 2 3]
print(z)    # [1 4 3]

# Sorting: organizing a array using a specific parameter

dtype = [('name', 'U10'), ('grande', float), ('age', int)]  # Changed 'S10' to 'U10' to remove b'parameter' in print
values = [('Joseanne', 5, 31), ('Hamed', 5, 32), ('Stefan', 5, 24)]
sorted_data = np.array(values, dtype=dtype)
print(np.sort(sorted_data, order='age'))    # [('Stefan', 5., 24) ('Joseanne', 5., 31) ('Hamed', 5., 32)]

# Creating Vectors and Matrices

print(np.zeros(3))  # [0. 0. 0.]
print(np.zeros((3, 4))) # [[0. 0. 0. 0.]
                        #  [0. 0. 0. 0.]
                        #  [0. 0. 0. 0.]]
# (Careful a tuple to work!)

b = np.zeros((10, 6), dtype=int)
print(b)
# [[0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]
#  [0 0 0 0 0 0]]

print(b.ndim, b.dtype, b.size)  # 2 int32 60

b = np.zeros(10)
b[8] = 1
print(b)    # [0. 0. 0. 0. 0. 0. 0. 0. 1. 0.]

# Slicing arrays is almost the same as slicing lists, except you can specify multiple dimensions

b = np.arange(10, 50)
print(b)    # [10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44
            #  45 46 47 48 49]
print(b.ndim, b.dtype, b.size)  # 1 int32 40

b = np.arange(50)   # Creates array from 0 to 49
b = b[::-1] # Invert array
b = b.reshape(5, 10)    # Reshapes array to multiple arrays (5x10)

print(b)
# [[49 48 47 46 45 44 43 42 41 40]
#  [39 38 37 36 35 34 33 32 31 30]
#  [29 28 27 26 25 24 23 22 21 20]
#  [19 18 17 16 15 14 13 12 11 10]
#  [ 9  8  7  6  5  4  3  2  1  0]]

print(b[4, 1])  # 8
print(b[3:])
# [[19 18 17 16 15 14 13 12 11 10]
#  [ 9  8  7  6  5  4  3  2  1  0]]

print(b[0, 2:]) # [47 46 45 44 43 42 41 40]
print(b[:1, 1:])    # [[48 47 46 45 44 43 42 41 40]]

x[0, 0]  # top-left element
x[0, -1]  # first row, last column element
x[0, :]  # all first row
x[:, 0]  # all first column

# Notes:
    # Zero-indexing
    # Multi-dimensional indices are comma-separated (ex: a tuple)
    # Writing to a slice overwrites the original array


# Concatenation, vstack, hstack
#   concatenate - join a sequence of arrays along an existing axis
#   vstack - Stack arrays in sequence vertically (row wise)
#   hstack - Stack arrays in sequence horizontally (column wise)

b = np.arange(50)
b = b.reshape(5, 10)
c = np.ones((2, 10))

print(c)
# [[1. 1. 1. 1. 1. 1. 1. 1. 1. 1.]
#  [1. 1. 1. 1. 1. 1. 1. 1. 1. 1.]]

print(np.concatenate((b, c)))
# [[ 0.  1.  2.  3.  4.  5.  6.  7.  8.  9.]
#  [10. 11. 12. 13. 14. 15. 16. 17. 18. 19.]
#  [20. 21. 22. 23. 24. 25. 26. 27. 28. 29.]
#  [30. 31. 32. 33. 34. 35. 36. 37. 38. 39.]
#  [40. 41. 42. 43. 44. 45. 46. 47. 48. 49.]
#  [ 1.  1.  1.  1.  1.  1.  1.  1.  1.  1.]
#  [ 1.  1.  1.  1.  1.  1.  1.  1.  1.  1.]]

a = np.arange(2, 6)
b = np.array([[3, 5], [4, 6]])

print(np.vstack(a))
# [[2]
#  [3]
#  [4]
#  [5]]

print(np.hstack(b)) # [3 5 4 6]

# Ones, zeros_like, ones_like, eye, empty, full

#  empty_like - Return an empty array with shape and type of input
#  ones - Return a new array setting values to one.
#  zeros - Return a new array setting values to zero.
#  full - Return a new array of given shape filed with value.

d = np.ones((1, 3)) # Careful with tuple!
x = np.zeros_like(d)
x1 = np.ones_like(x)

print(d)    # [[1. 1. 1.]]
print(x)    # [[0. 0. 0.]]
print(x1)   # [[1. 1. 1.]]

b = np.eye(3)   # Matriz Unitária

print(b)
# [[1. 0. 0.]
#  [0. 1. 0.]
#  [0. 0. 1.]]

print(np.empty((1, 2)))  # [[1.  1.5]], Careful with tuple!

print(np.full((2, 2), np.inf))
# [[inf inf]
#  [inf inf]]

# Astype

b = np.arange(50)
c = b.astype(np.int16)

print(b.astype(np.int16))
# [ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
#  24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47
#  48 49]

print(b.ndim, b.dtype, b.size)  # 1 int32 50
print(c.ndim, c.dtype, c.size)  # 1 int16 50

# Random Numbers

b = np.random.random((10, 10))  # Careful with tuple!
bmin, bmax = b.min(), b.max()

print(bmin, bmax)   # 0.0018531267644404936 0.9795643409080874

b = np.random.random((3, 3, 3))

print(b)
# [[[0.22036608 0.93985397 0.67835612]
#   [0.97845602 0.78449588 0.13083374]
#   [0.50208521 0.22619922 0.38950174]]
#  [[0.11032955 0.10787122 0.6336593 ]
#   [0.31379507 0.34850459 0.15481013]
#   [0.22177824 0.40301263 0.95560241]]
#  [[0.11509651 0.88526549 0.24513743]
#   [0.90032034 0.3066515  0.034766  ]
#   [0.79757732 0.18819263 0.7159135 ]]]

# Usefull funcionality

yesterday = np.datetime64('today') - np.timedelta64(1)
today = np.datetime64('today')
tomorrow = np.datetime64('today') + np.timedelta64(1)

print(yesterday, today, tomorrow)   # 2020-11-03 2020-11-04 2020-11-05

b = np.arange('2019', '2021', dtype='datetime64[D]')

print(b.size, b.ndim)   # 731 1


import matplotlib.pyplot as plt

values = np.linspace(-(2*np.pi),2*np.pi, 90)
cos_value = np.cos(values)
sin_value = np.sin(values)

plt.plot(cos_value, color='blue', marker='*')
plt.plot(sin_value, color='red')
plt.show()  # Show plot on SciView for Pycharm!

from PIL import Image

my_path = "imagine a path"
im = Image.open(my_path)
plt.imshow(im)

im2 = open(my_path)
im_p2 = np.array(im2.convert('P'))  # Changes pixel color!
plt.imshow(im2)


im3 = open(my_path)
im_p3 = np.array(im3.convert('L'))  # Same as above but different color!
plt.imshow(im3)