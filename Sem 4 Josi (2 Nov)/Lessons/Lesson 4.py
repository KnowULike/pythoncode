# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------

import numpy as np
import pandas as pd

data = pd.Series([0.25, 0.5, 0.75, 1.0])

data.values

data[2] # 0.75

data = pd.Series([0.25, 0.5, 0.75, 1.0], index=['a', 'b', 'c', 'd'])

data['b']   # 0.5

data = pd.Series([0.25, 0.5, 0.75, 1.0], index=[2, 5, 3, 7])

data[2] # 0.25


pop_d = {'California':13424,
         'Texas':25143}

population = pd.Series(pop_d)
population

population['California']

states = pd.DataFrame({'population': population})
states