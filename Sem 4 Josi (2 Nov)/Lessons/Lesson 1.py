# -*- coding: utf-8 -*-----------------------------------------------------------------------------------------------
"""
@author: António Rocha %(username)-----------------------------------------------------------------------------------
Created on %(date)---------------------------------------------------------------------------------------------------
"""
# -------------------------------------------------------------------------------------------------------------------

# List 2 - Exercise 1
# Expected Exercise well commented code example

firstNum = int(input("Tell me the first number:"))
# read second number
secondNum = int(input("Tell me the second number:"))
# boolean to control the loop
validOperation = False
# make operation using the variables with the values that were given
while not validOperation:
    # read operator
    operator = input("Tell me which operator, out of the    following:\n '+', '-', '*' or '/'\n")
    if operator == "+":
        result = firstNum + secondNum
        validOperation = True
        break
    elif operator == "-":
        result = firstNum - secondNum
        validOperation = True
        break
    elif operator == "*":
        result = firstNum * secondNum
        validOperation = True
        break
    elif operator == "/":
        result = firstNum / secondNum
        validOperation = True
        break
    else:
        print("Value needs ot be one of the following operators    instead:\n '+', '-', '*' or '/' ")
        continue

# print result
print("The result of your operation is:", result, "\n")

# List 2 - Exercise 1
# Best Solution

n1 = input("\nEnter first number: ")
n2 = input("\nEnter second number: ")
o = input("\nEnter the operator: ")

equation = n1+o+n2
print(f"{n1} {o} {n2}")

res = eval(equation)    # Zero breaks this code!
print("\nRES:", res)


# Exercise 3
print("\nExemplo 3: ")

r1, r2, r3, r4 = 0, 0, 0, 0

for i in [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]:
    if i in range(0, 26):
        r1 = r1 + 1
    if i in range(26, 51):
        r2 = r2 + 1
    if i in range(51, 76):
        r3 = r3 + 1
    if i in range(76, 101):
        r4 = r4 + 1

print("\n[0 ,25]: ", r1)    # [0 ,25]:  4
print("[26,50]: ", r2)  # [26,50]:  0
print("[51,75]: ", r3)  # [51,75]:  3
print("[76,100]:", r4)  # [76,100]: 1

# Guess game (Solution)
guess_number = 5
user_number = True
while user_number:
    user_number = int(input("Enter a anumber: "))
    if user_number < guess_number:
        print("Please, guess higher")
    elif user_number == guess_number:
        print("Congrats, you found the answer")
        break
    else:
        print("Please, guess smaller.")

# REDO - Be careful using condicional
# Solution in comments!
day = 'Saturday'
temperature = 30
raining = True

if day == 'Saturday' and temperature > 20 or not raining:   # The use of 'not' keyword is wrong! Can induce in error.
    print('Go Out')
else:
    print("Better finishing python programming exercises")

# Revisions


# Importing modules
# Finding functions attributes
# Documentation
# List:  <identification> =  [<value1>, <value2>]
#          Accepts different variable types
#          Index to access and/or find members
#          Cannot change strings in lists using indexes
#          Ordered
#          usually for loop is used to manipulate lists
#          It is possible to make slicing
#          it is possible to concatenate lists with * and +
#          in and in not indicates membership
#          it is possible to include a list inside other list
#          Lists have methods such as .pop(), insert(),
#          count(), append()
#          Filtering

import sqlite3  # Import sqlite3 module
from sqlite3 import Error   # import Error from sqlite3 module
from numpy import * # import everything from numpy, not recommended
import numpy as np  # import numpy but by calling 'np'

b = [4, 6, 7]
print(np.add(b, 5)) # Using numpy.add  5 to b ( output: [ 9 11 12] )

print(dir(np))  # Checking numpy info

L = [3, True, 'Ali', 2.7, [5, 8]]   # A list can contain multiple types inside at the same time

a = [3, [109, 27], 4, 15]

# print(a(1)) gives a TypeError: 'list' object is not callable
print(a[1]) # We acess a list by index with []  ( output: [109, 27] )

a = [7, 5, 30, 2, 6, 25]
print(a[1:4])   # its accessing to all numbers from the 2nd to the 4th index ( output: [5, 30, 2] )

a = [1, 3, 6, 5, 3]
print(a.count(3))   # .count('value'), counts the number of occurences of value ( output: 2 )

a = []
for i in range(4):  # range from 0-3
    a.append(i) # adds 'i' to list 'a'
print(a)    # [0, 1, 2, 3]

# Tuple Revisions

# Tuple:  <identification> =  (<value1>, <value2>)

#         Index to access and/or find members
#         Cannot change strings in tuples using indexes
#         it is possible to concatenate lists with * and +
#         Ordered

# it is possible to use python build in functions to manipulate tuples
# Lists have methods such as .count(), .index(),

# Removing member is only possible by converting to another type
# It is possible to save tuple member in other variables - unpacking
# A look in zip().

t = ("English", "History", "Mathematics")
print(t[1]) # History
print(t.index("English"))   # 0

t = (1, 9, 3, 9)
print(t.count(9))   # 2
print(max(t))   # 9
print(t + t)    # (1, 9, 3, 9, 1, 9, 3, 9)

print((1, 2) == (2, 1)) # False

a = (1, 2)
b = (3, 4)
c = zip(a, b)
x = list(c)
print(x)    # [(1, 3), (2, 4)]
print(x[0]) # (1, 3)
z = ((1, 3), (2, 4))
u = zip(*z)
print(list(u))  # [(1, 2), (3, 4)]

t = (4, 7, 2, 9, 8)
x = list(t)
x.remove(2)
t = tuple(x)
print(t)    # (4, 7, 9, 8)

# Dictionary Revisions

# Dictionary:  <identification> =  {<value1>: <value2>,
#                                   <value3>: <value4> }

# Dictionaries have methods such as .get(), pop(), .popitem(), .copy(), .update()

# It is possible to use python build in functions to manipulate dictionaries sum().
# Sorting using operator module

d = {"brand": "cherry", "model": "arizo5", "color": "white"}
d["color"] = "black"
print(d)    # {'brand': 'cherry', 'model': 'arizo5', 'color': 'black'}

x = d.get("model")
print(x)    # arizo5

print(list(d.keys()))   # ['brand', 'model', 'color']

print(list(d.values())) # ['cherry', 'arizo5', 'black']

d.pop("model")
print(d)    # {'brand': 'cherry', 'color': 'black'}

k = ["red", "green"]
v = ["#FF0000", "008000"]
z = zip(k, v)
d = dict(z)
print(d)    # {'red': '#FF0000', 'green': '008000'}

num = {"ali": [12, 13, 8], "sara": [15, 7, 14], "taha": [5, 18, 13]}
d = {k: sorted(v) for k, v in num.items()}
print(d)    # {'ali': [8, 12, 13], 'sara': [7, 14, 15], 'taha': [5, 13, 18]}

# Sets  Revisions

# Set :  <identification> =  {<value1>, <value2>,<value3>}

#       Add values in the dictionary
#       Lists have methods such as .add(), update(), .remove(), .copy(), .update()
#       joint theory
#       Subset

f = {"apple", "orange", "banana"}
f.add("cherry")
print(f)    # {'cherry', 'orange', 'banana', 'apple'}

f.update(["mango", "grapes"])
print(f)    # {'cherry', 'orange', 'apple', 'mango', 'grapes', 'banana'}

f.remove("apple")
print(f)    # {'cherry', 'orange', 'mango', 'grapes', 'banana'}

X = {1, 2, 3}
Y = {2, 3, 4}
# union
print(X.union(Y))   # {1, 2, 3, 4}
print(X | Y)    # {1, 2, 3, 4}

# intersection
print(X.intersection(Y))    # {2, 3}
print(X & Y)    # {2, 3}

# Subset
A = {1, 2, 4}
B = {1, 2, 3, 4, 5}
print(A.issubset(B))    # True
print(B.issubset(A))    # False


# Files Revisions

# Files:

#          Open files  open(), .close()

# open(<path to the file>, <Mode>)
# With open takes care of closing files

# manipulating files

#   .read(), .readline() .readlines()
#          readline() – Reads a single line of the file and return a string
#          readlines() – reads the entire file and return a list of strings
#          read () – reads the entire file and return a string

epopeia = open("C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\Epopeia.txt", "r", encoding='utf-8')

for line in epopeia:
    print(line)
    if "tempestade" in line.lower():
        print(line)

# Output: Epopeia de Camões (Toda)

epopeia.close()

with open("C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\Epopeia.txt", "r",
          encoding='utf-8') as epopeia:
    for line in epopeia:
        if "tempestade" in line.lower():
            print(line)  # Despois de procelosa tempestade,
print(line)  # Despois que o Rei Fernando faleceu.


# Printing Epopeia line by line in one string
file_path = "C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\Epopeia.txt"
encoding_type = "utf-8"
with open(file_path, "r", encoding=encoding_type) as epopeia:
    line = epopeia.readline()
    while line:
        print(line, end='')
        line = epopeia.readline()


# Printing Epopeia in a list of strings after reading the whole file
file_path = "C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\Epopeia.txt"
encoding_type = "utf-8"
with open(file_path, "r", encoding=encoding_type) as epopeia:
    poem = epopeia.readlines()
    print(type(poem))   # <class 'list'>
    print(poem)
    for lines in poem[::-1]:    # Invert
        print(line, end='')  # ['\n', '    Canto I\n', '\n', '    As armas e os ..]

# Printing Epopeia in a string after reading the whole file
file_path = "C:\\Users\\franc\\OneDrive\\Ambiente de Trabalho\\Trabalho\\Josi\\Epopeia.txt"
encoding_type = "utf-8"
with open(file_path, "r", encoding=encoding_type) as epopeia:
    txt = epopeia.read()
    print(type(txt))    # <class 'str'>
    print(txt)
    for line in txt[::-1]:  # Invert
        print(line, end='') # .uecelaf odnanreF ieR o euq siopseD ...
    print(type(txt))


# Json
# Files:
#     Json files - JavaScript Object Notion file - easy to understand by human and  machines

#JSON validator
# Data is separated by commas
# Curly Brackets hold objects.

#          Json files – import json packet

# open (<path to the file >)  json.load(<object>),
# json.dumps(<pythonObject>, <formatting attribute>) – convert python objects in json objects
# json.dump((<pythonObject>, <fileObject>, <formatting attribute>) ) – write json files

import json

path = "/Semana 4 (Nov 2)\\Lessons\\teste.json"
with open(path) as json_file:
    jsonObject = json.load(json_file)
    name = jsonObject["data"][0]["name"]
    address = jsonObject["data"][0]["address"]["city"]
    name2 = jsonObject["data"][1]["name"]
    address2 = jsonObject["data"][1]["address"]["city"]

    print(jsonObject, '\n')
    # {'data': [{'id': '12341', 'name': 'Joseanne Viana',
    #            'address': {'city': 'Lisboa', 'state': 'Lisboa', 'postal_code': '1000549'}, 'position': 'PhD Student'},
    #           {'id': '45678', 'name': 'Alfredo Batman',
    #            'address': {'city': 'Porto', 'state': 'Porto', 'postal_code': '2745114'},
    #            'position': "Master's Student"}]}

    print(name)  # Joseanne Viana
    print(address)  # Lisboa

    print(name2)    # Alfredo Batman
    print(address2) # Porto

    a = json.dumps(jsonObject, indent=4)
    print('\n', a)
    #  {
    #     "data": [
    #         {
    #             "id": "12341",
    #             "name": "Joseanne Viana",
    #             "address": {
    #                 "city": "Lisboa",
    #                 "state": "Lisboa",
    #                 "postal_code": "1000549"
    #             },
    #             "position": "PhD Student"
    #         },
    #         {
    #             "id": "45678",
    #             "name": "Alfredo Batman",
    #             "address": {
    #                 "city": "Porto",
    #                 "state": "Porto",
    #                 "postal_code": "2745114"
    #             },
    #             "position": "Master's Student"
    #         }
    #     ]
    # }

# Excel

import pandas as pd

a = "C:\\Users\\franc\OneDrive\Ambiente de Trabalho\Trabalho\Josi\Employee_data.xlsx"
all_data = pd.read_excel(a, index_col=1)

email = all_data["email"].head()
companyName = all_data["company_name"]
xl = pd.ExcelFile(a)
df = xl.parse("b")

print(df)   # Data frame fromm employee_data

# Try, catch, else, finally
# Accessing code from other libraries outside of python, text files, databases

import json

file_path = "/Semana 4 (Nov 2)\\Lessons\\teste.json"

try:
    with open(file_path) as json_file:  # path is not correct
        json_object = json.load(json_file)
except:
    print("File not found")
else:
    name = json_object["data"][0]["name"]
    address = json_object["data"][1]["address"]["city"]
    print(json_object)
    print(name)
    print(address)

finally:
    print("Something happened")

# Path is not correct
    # File not found
    # Something happened

# Path is correct
    # {'data': [{'id': '12341', 'name': 'Joseanne Viana',
    #            'address': {'city': 'Lisboa', 'state': 'Lisboa', 'postal_code': '1000549'}, 'position': 'PhD Student'},
    #           {'id': '45678', 'name': 'Alfredo Batman',
    #            'address': {'city': 'Porto', 'state': 'Porto', 'postal_code': '2745114'}, 'position': "Master's Student"}]}
    # Joseanne Viana
    # Porto
    # Something happened

# Lambda expressions Revision
#
# Lambda function also called anonymous function
# Can receive any number of arguments, but can only have one expression
#
#   lambda <arguments>:<expression>

def power_tree(x):
    return (x ** 3)


print(power_tree(10))  # 1000

a = lambda x: x ** 3
print(a(10))  # 1000


def larger_num(num1, num2):
    if num1 > num2:
        return num1
    else:
        return num2


print(larger_num(5, 7))  # 7

larger_num = lambda num1, num2: num1 if num1 > num2 else num2
print(larger_num(5, 7))  # 7

# Maps Revisions

# Perform an operation and return it value in a list

txt = "LISBON IS IN PORTUGAL"

def char_lowercase():
    char_lowercase = [char.lower() for char in txt]
    return char_lowercase

# using map
def map_lowercase():
    map_lowercase = list(map(str.lower, txt))
    return map_lowercase

def comp_words():
    words_lowercase = [word.lower() for word in txt.split(" ")]
    return words_lowercase

def map_words():
    map_w = list(map(str.lower, txt.split(" ")))
    return map_w

print(char_lowercase())
# ['l', 'i', 's', 'b', 'o', 'n', ' ', 'i', 's', ' ', 'i', 'n', ' ', 'p', 'o', 'r', 't', 'u', 'g', 'a', 'l']
print(map_lowercase())
# ['l', 'i', 's', 'b', 'o', 'n', ' ', 'i', 's', ' ', 'i', 'n', ' ', 'p', 'o', 'r', 't', 'u', 'g', 'a', 'l']

print(comp_words()) # ['lisbon', 'is', 'in', 'portugal']
print(map_words())  # ['lisbon', 'is', 'in', 'portugal']

# Filter Revisions

# Perform an operation and return it. Value in a list

vegetables = [
    ["Beets", "Couliflower", "brocculi"],
    ["Beets", "Carrot", "Couliflower"],
    ["Beets", "Carrot"],
    ["Beets", "brocculi", "Carrot"]
]

def not_broccoli(food_list: list):
    return "broccoli" not in food_list

broccoli_less_list = list(filter(not_broccoli, vegetables))
print(broccoli_less_list)
# [['Beets', 'Couliflower', 'brocculi'],
#  ['Beets', 'Carrot', 'Couliflower'],
#  ['Beets', 'Carrot'],
#  ['Beets', 'brocculi', 'Carrot']]

# Classes Revision

# Object oriented programming
# Aims to combine data and processes related to that data (objects)

# The __init__() method is called a constructor method
# The increase() method increments the value of the attribute val by 1.
# The show() method displays the value of the attribute val

class josi:
    a = [2, 4, 6]
    b = [7, 8]
    c = [10, 11, 21]

    def __init__(self, val):
        self.val = val
        print(val)

    def increase(self):
        self.val += 1

    def show(self):
        print(self.val)

x = josi(3) # 3
c = x.increase()
d = x.show()    # 4
print(type(x.a))    # <class 'list'>
print(x.a.__add__(x.b)) # [2, 4, 6, 7, 8]
print(x.a.__eq__(x.b))  # False
print(x.a.__ne__(x.c))  # True

# Creating a new module and importing is in next lessons