import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlay, faPause, faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { useRef, useState } from "react";

const Player = ({
    audioRef,
    currentSong,
    setCurrentSong,
    songs,
    setsongs,
    isPlaying,
    setIsPlaying}) => {

    const animateTrackRef = useRef(null);
    const [songInfo, setSongInfo] = useState({
        current: 0,
        duration: 0,
    }); // 1. Criar um estado
    const animationPercentage = Math.round((songInfo.current/songInfo.duration) * 100);
    

    // 2.1 Adicionar handlers nas setinhas
    // 2.2. parar música atual que estiver a tocar
    // 2.3 Obter o índice da música atual
    // 3.4 Obter i indice da música seguinte ou anterior

    // Handlers
    const onAudioPlayHandler = (Event) => {
        if (isPlaying) {
            audioRef.current.pause();
            setIsPlaying(false);
        } else {
            audioRef.current.play();
            setIsPlaying(true);
        };
    };

    const onTimeUpdateHandler = (Event) => {    // 2.Criar um event handler
        const current= Event.target.currentTime;
        const duration= Event.target.duration;
        /* animateTrackRef.current.style = {
            transform: `translateX(${Math.round((current/duration)*100)}%)`,
        }; */
        setSongInfo({
            current: current,
            duration: duration
        });
    }

    const onTimeChangeHandler = (Event) =>  {
        audioRef.current.currentTime = Event.target.value;
        setSongInfo({...songInfo, current: Event.target.value});
    }

    const skipTrackHandler = async(forwards) => {
        const songsLength = songs.length;
        const currentSongIndex = songs.findIndex((song) => {
            return (song.id === currentSong.id);
        });
        let nextIndex;
        if (forwards) {
            nextIndex = (currentSongIndex + 1) % songsLength;
        } else {
            nextIndex = (songsLength + (currentSongIndex - 1)) % songsLength;
        }
        await setCurrentSong(songs[nextIndex]);
        notifyActiveLibraryHandler(songs[nextIndex]);
        setIsPlaying(true);
        audioRef.current.play();
    }

    const onEndedHandler = async(Event) => {
        // 1. Buscar próx. música
        const songsCount = songs.length;
        const currentSongIndex = songs.findIndex((song) => {
            return (song.id === currentSong.id);
        });
        const nextIndex = (currentSongIndex + 1) % songsCount;

        // 2. Tocar próx. música
        await setCurrentSong(songs[nextIndex]);
        notifyActiveLibraryHandler(songs[nextIndex]);  // Tem de se fazer next index porcausa do await
        setIsPlaying(true);
        audioRef.current.play();
    }

    const notifyActiveLibraryHandler = (nextElement) => {
        const newSongs = songs.map((song) => {
            if (song.id === nextElement.id) {
                return {
                    ...song,
                    active: true
                };
            } else {
                return {
                    ...song,
                    active: false
                };
            }
        });
        setsongs(newSongs);
    }

    // Aux functions
    const getTime = (time) => {
        return(
            Math.floor(time/60) + ":" + ("0" + Math.floor(time % 60)).slice(-2)
        );
    }



    return (
        <div className="player">
        <div className="time-control">
            <p>{getTime(songInfo.current)}</p>   
            <div className="track" style={{
                backgroundImage: `linear-gradient(to right, ${currentSong.color[0]}, ${currentSong.color[1]})`
            }}>
                <input type="range" min={0} max={songInfo.duration} value={songInfo.current} onChange={onTimeChangeHandler}/>
                <div className="animate-track" ref={animateTrackRef} style={{transform: `translateX(${animationPercentage}%)`}}>
                <div className="animate-track-thumb"></div>
                </div>
            </div>
            <p>{getTime(songInfo.duration)}</p>  
        </div>
        <div className="play-control">
            <FontAwesomeIcon icon={faAngleLeft} size="2x" onClick={() => skipTrackHandler(false)}/>
            <FontAwesomeIcon icon={isPlaying ? faPause : faPlay} size="2x" onClick={onAudioPlayHandler}/>
            <FontAwesomeIcon icon={faAngleRight} size= "2x" onClick={() => skipTrackHandler(true)}/>
            <audio src={currentSong.audio} ref= {audioRef} onTimeUpdate={onTimeUpdateHandler} 
            onLoadedMetadata={onTimeUpdateHandler} onEnded={onEndedHandler}/>
        </div>
    </div>
    );
}

export default Player