import LibrarySong from "./LibrarySong";

const Library = ({
    songs,
    setSongs,
    isLibraryOpen,
    setCurrentSong,
    currentSong,
    isPlaying,
    setIsPlaying,
    audioRef
    }) => {
    return (
        <div className={`library ${isLibraryOpen ? "open": " "}`}>
            <h2>Library</h2>
            <div className="Library-Songs">
                {   
                    songs.map((song) => {
                        return <LibrarySong 
                                    key={song.id}
                                    song={song} 
                                    songs={songs} 
                                    setSongs={setSongs}
                                    currentSong={currentSong}
                                    setCurrentSong={setCurrentSong}
                                    isPlaying={isPlaying}
                                    setIsPlaying={setIsPlaying}
                                    audioRef={audioRef}
                                />
                            })
                }
            </div>
        </div>
    );
}

export default Library;